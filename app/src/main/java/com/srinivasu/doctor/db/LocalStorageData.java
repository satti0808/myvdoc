package com.srinivasu.doctor.db;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.pddstudio.preferences.encrypted.EncryptedPreferences;
import com.quickblox.users.model.QBUser;
import com.srinivasu.doctor.utils.Consts;

public class LocalStorageData {
    private EncryptedPreferences sharedPreferences;
    private EncryptedPreferences.EncryptedEditor editor;
    private Gson gson;

    public LocalStorageData(Context context) {
        sharedPreferences = new EncryptedPreferences.Builder(context).withEncryptionPassword("@@CEMSAPP##").build();
        editor = sharedPreferences.edit();
        gson = new Gson();
    }

    public void storeQBUser(QBUser qbUser){
        if(qbUser!= null){
            qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
            editor.putString("qbuser", new Gson().toJson(qbUser));
            editor.commit();
        }
    }

    public QBUser getQBUser(){
        String user_details_json = sharedPreferences.getString("qbuser", "");
        return gson.fromJson(user_details_json, QBUser.class);
    }

    public String QBuserId(){
        return sharedPreferences.getString("qb_id", null);
    }

    public void clearQBUser(){
        editor.putString("qbuser", "");
        editor.putString("qb_id","");
        editor.commit();
    }

    public void storeUserDetails(String role, String id, String username, String token, String email, String number, String age, String gender){
        clearUserDetails();
        editor.putString("roleID", role);
        editor.putString("user_id", id);
        editor.putString("userName", username);
        if(!TextUtils.isEmpty(age)){
            editor.putString("age", age);
        }
        if(!TextUtils.isEmpty(gender)){
            editor.putString("gender", gender);
        }

        editor.putString("token",token);
        if(!TextUtils.isEmpty(email)){
            editor.putString("email", email);
        }
        if(!TextUtils.isEmpty("number")){
            editor.putString("number", number);
        }
        editor.commit();
    }

    public void storePatientsDoctor(String doctorId){
        clearDoctorDetails();
        editor.putString("pat_doc_id", doctorId);
        editor.commit();

    }

    public String getRoleId(){
        return sharedPreferences.getString("roleID", null);
    }
    public String getID(){
        return sharedPreferences.getString("user_id", null);
    }
    public String getUserName(){
        return sharedPreferences.getString("userName", null);
    }
    public String getToken(){
        return sharedPreferences.getString("token", null);
    }

    public String getEmail(){
        return sharedPreferences.getString("email", "");
    }
    public String getNumber(){
        return sharedPreferences.getString("number", "");
    }
    public String getAge(){
        return sharedPreferences.getString("age", "");
    }
    public String getGender(){
        return sharedPreferences.getString("gender", "");
    }

    public String getPatientsDotcotrId(){
        return sharedPreferences.getString("pat_doc_id", null);
    }


    public void clearUserDetails(){
        editor.putString("roleID","");
        editor.putString("user_id","");
        editor.putString("userName","");
        editor.putString("token","");
        editor.putString("email", "");
        editor.putString("number", "");
        editor.commit();
    }

    private void clearDoctorDetails() {

        editor.putString("pat_doc_id","");
        editor.commit();
    }
}
