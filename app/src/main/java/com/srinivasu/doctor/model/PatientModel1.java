package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class PatientModel1 {
    @SerializedName("ID")
    private String ID;

    @SerializedName("PatientName")
    private String PatientName;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }
}
