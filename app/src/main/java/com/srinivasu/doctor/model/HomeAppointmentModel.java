package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class HomeAppointmentModel {
    @SerializedName("appointment_ID")
    private String appointment_ID;

    @SerializedName("patient_ID")
    private String patient_ID;

    @SerializedName("patient_name")
    private String patient_name;

    @SerializedName("appointment_time")
    private String appointment_time;

    @SerializedName("appointment_date")
    private String appointment_date;

    @SerializedName("appointment_type")
    private String appointment_type;

    @SerializedName("phone")
    private String phone;

    @SerializedName("duration")
    private String duration;

    @SerializedName("img_url")
    private String img_url;

    @SerializedName("vedioid")
    private String vedioid;

    public String getAppointment_ID() {
        return appointment_ID;
    }

    public void setAppointment_ID(String appointment_ID) {
        this.appointment_ID = appointment_ID;
    }

    public String getPatient_ID() {
        return patient_ID;
    }

    public void setPatient_ID(String patient_ID) {
        this.patient_ID = patient_ID;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getAppointment_type() {
        return appointment_type;
    }

    public void setAppointment_type(String appointment_type) {
        this.appointment_type = appointment_type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getVedioid() {
        return vedioid;
    }

    public void setVedioid(String vedioid) {
        this.vedioid = vedioid;
    }
}
