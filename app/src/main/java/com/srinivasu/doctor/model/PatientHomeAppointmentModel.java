package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class PatientHomeAppointmentModel {

    @SerializedName("appointment_ID")
    @Expose
    private String appointment_ID;

    @SerializedName("doctor_ID")
    @Expose
    private String doctor_ID;

    @SerializedName("doctor_name")
    @Expose
    private String doctor_name;

    @SerializedName("appointment_date")
    @Expose
    private String appointment_date;

    @SerializedName("appointment_time")
    @Expose
    private String appointment_time;

    @SerializedName("appointment_type")
    @Expose
    private String appointment_type;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("duration")
    @Expose
    private String duration;

    public PatientHomeAppointmentModel() {
    }

    public String getAppointment_ID() {
        return appointment_ID;
    }

    public void setAppointment_ID(String appointment_ID) {
        this.appointment_ID = appointment_ID;
    }

    public String getDoctor_ID() {
        return doctor_ID;
    }

    public void setDoctor_ID(String doctor_ID) {
        this.doctor_ID = doctor_ID;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getAppointment_type() {
        return appointment_type;
    }

    public void setAppointment_type(String appointment_type) {
        this.appointment_type = appointment_type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
