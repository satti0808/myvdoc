package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class MedicineMasterModel {

    private int ID;
    private String Name;
    private String DrugName;
    private String Price;
    private String ExpDate;
    private String MfgName;
    private String Route;
    private String Frequency;
    private String Strength;
    private String Disp;

    public MedicineMasterModel() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDrugName() {
        return DrugName;
    }

    public void setDrugName(String drugName) {
        DrugName = drugName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getExpDate() {
        return ExpDate;
    }

    public void setExpDate(String expDate) {
        ExpDate = expDate;
    }

    public String getMfgName() {
        return MfgName;
    }

    public void setMfgName(String mfgName) {
        MfgName = mfgName;
    }

    public String getRoute() {
        return Route;
    }

    public void setRoute(String route) {
        Route = route;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getStrength() {
        return Strength;
    }

    public void setStrength(String strength) {
        Strength = strength;
    }

    public String getDisp() {
        return Disp;
    }

    public void setDisp(String disp) {
        Disp = disp;
    }
}
