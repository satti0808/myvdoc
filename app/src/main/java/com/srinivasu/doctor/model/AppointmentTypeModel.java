package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class AppointmentTypeModel {
    @SerializedName("ID")
    private String ID;
    @SerializedName("Appointmenttype")
    private String Appointmenttype;
    @SerializedName("Amount")
    private String Amount;
    @SerializedName("Status")
    private String Status;
    @SerializedName("Duration")
    private String Duration;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAppointmenttype() {
        return Appointmenttype;
    }

    public void setAppointmenttype(String appointmenttype) {
        Appointmenttype = appointmenttype;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    @Override
    public String toString() {
        return this.Appointmenttype;            // What to display in the Spinner list.
    }
}
