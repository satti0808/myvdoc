package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class ImageModel {
    @SerializedName("URL")
    private String URL;

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
