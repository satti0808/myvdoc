package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class PrescriptionModel {

    private int ID;
    private int TblMedicineMaster_ID;
    private String Quantity;
    private String Amount;
    private String appointmentID;
    private MedicineMasterModel medicineMaster;

    public PrescriptionModel() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getTblMedicineMaster_ID() {
        return TblMedicineMaster_ID;
    }

    public void setTblMedicineMaster_ID(int tblMedicineMaster_ID) {
        TblMedicineMaster_ID = tblMedicineMaster_ID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public MedicineMasterModel getMedicineMaster() {
        return medicineMaster;
    }

    public void setMedicineMaster(MedicineMasterModel medicineMaster) {
        this.medicineMaster = medicineMaster;
    }
}
