package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttachmentsListModel {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("data")
    @Expose
    private AttachmentsModel data1;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public AttachmentsModel getData1() {
        return data1;
    }

    public void setData1(AttachmentsModel data1) {
        this.data1 = data1;
    }
}
