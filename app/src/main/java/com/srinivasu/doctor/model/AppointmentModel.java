package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentModel {
    @SerializedName("DoctorID")
    private String DoctorID;

    @SerializedName("date")
    private String date;

    @SerializedName("appointments")
    private List<AppointmentDetailsModel> appointments=null;

    public String getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(String doctorID) {
        DoctorID = doctorID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<AppointmentDetailsModel> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<AppointmentDetailsModel> appointments) {
        this.appointments = appointments;
    }
}
