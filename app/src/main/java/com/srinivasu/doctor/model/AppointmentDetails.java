package com.srinivasu.doctor.model;

import org.parceler.Parcel;

@Parcel
public class AppointmentDetails {

    private String appointment_ID;
    private float appointment_amount;
    private String appointment_date;
    private String appointment_time;
    private String appointment_type;
    private int doctor_ID;
    private String doctor_name;
    private String duration;
    private String phone;
    private String vedioid;
    private float rating;
    private String feedback;

    public AppointmentDetails() {
    }

    public String getAppointmentID() {
        return appointment_ID;
    }

    public void setAppointmentID(String appointment_ID) {
        this.appointment_ID = appointment_ID;
    }

    public float getAppointmentAmount() {
        return appointment_amount;
    }

    public void setAppointmentAmount(float appointment_amount) {
        this.appointment_amount = appointment_amount;
    }

    public String getAppointmentDate() {
        return appointment_date;
    }

    public void setAppointmentDate(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointmentTime() {
        return appointment_time;
    }

    public void setAppointmentTime(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getAppointmenType() {
        return appointment_type;
    }

    public void setAppointmentType(String appointment_type) {
        this.appointment_type = appointment_type;
    }

    public int getDoctorID() {
        return doctor_ID;
    }

    public void setDoctorID(int doctor_ID) {
        this.doctor_ID = doctor_ID;
    }

    public String getDoctorName() {
        return doctor_name;
    }

    public void setDoctorName(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVideoID() {
        return vedioid;
    }

    public void setVideoID(String vedioid) {
        this.vedioid = vedioid;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
