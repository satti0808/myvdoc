package com.srinivasu.doctor.model;

import java.util.List;

public class SingleDaySlotItem {

    String day;
    String id;
    boolean isDoctorAvailable;
    List<TimeSlots> slots;

    public SingleDaySlotItem(String day, String id, boolean isDoctorAvailable, List<TimeSlots> slots) {
        this.day = day;
        this.id = id;
        this.isDoctorAvailable = isDoctorAvailable;
        this.slots = slots;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDoctorAvailable() {
        return isDoctorAvailable;
    }

    public void setDoctorAvailable(boolean doctorAvailable) {
        isDoctorAvailable = doctorAvailable;
    }

    public List<TimeSlots> getSlots() {
        return slots;
    }

    public void setSlots(List<TimeSlots> slots) {
        this.slots = slots;
    }

    public static class TimeSlots {
        String startTime;
        String endTime;

        public TimeSlots(String startTime, String endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }
    }


}
