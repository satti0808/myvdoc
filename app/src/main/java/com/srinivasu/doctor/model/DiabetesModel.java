package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class DiabetesModel {
    @SerializedName("ID")
    private String id;
    @SerializedName("Date")
    private String Date;

    @SerializedName("Time")
    private String Time;

    @SerializedName("PerLunch")
    private String PerLunch;

    @SerializedName("PostLunch")
    private String PostLunch;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getPerLunch() {
        return PerLunch;
    }

    public void setPerLunch(String perLunch) {
        PerLunch = perLunch;
    }

    public String getPostLunch() {
        return PostLunch;
    }

    public void setPostLunch(String postLunch) {
        PostLunch = postLunch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
