package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestModel {
    @SerializedName("ID")
    private String ID;

    @SerializedName("TestName")
    private String TestName;

    @SerializedName("TestType")
    private String TestType;

    @SerializedName("skills_required")
    @Expose
    private List<TestSkillModel> skills;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String testName) {
        TestName = testName;
    }

    public String getTestType() {
        return TestType;
    }

    public void setTestType(String testType) {
        TestType = testType;
    }

    public List<TestSkillModel> getSkills() {
        return skills;
    }

    public void setSkills(List<TestSkillModel> skills) {
        this.skills = skills;
    }
}
