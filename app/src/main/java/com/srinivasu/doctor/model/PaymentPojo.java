package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class PaymentPojo {

    @SerializedName("name")
    private String name;

    @SerializedName("date")
    private String date;

    @SerializedName("amount")
    private String amount;


    public PaymentPojo(String name, String date,String amount){
        this.setName(name);
        this.setDate(date);
        this.setAmount(amount);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}