package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LeavesModel {
    @SerializedName("ID")
    private String ID;

    @SerializedName("StartTime")
    private String StartTime;

    @SerializedName("EndTime")
    private String EndTime;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getStartTime() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(StartTime);
            SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }

    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(EndTime);
            SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }
}
