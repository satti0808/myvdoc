package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientAppointmentDataModel {
    @SerializedName("status")
    private String status;

    @SerializedName("startTime")
    private String startTime;

    @SerializedName("endTime")
    private String endTime;

    @SerializedName("patient")
    private PatientModel appointments=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(startTime);
            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public PatientModel getAppointments() {
        return appointments;
    }

    public void setAppointments(PatientModel appointments) {
        this.appointments = appointments;
    }
}
