package com.srinivasu.doctor.model;

public class DoctorAvailableSlot {

    private String slotID;
    private String slotTime;
    private int isAvailable;
    private int isSlotExpired;

    public String getSlotID() {
        return slotID;
    }

    public void setSlotID(String slotID) {
        this.slotID = slotID;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }

    public int getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(int isAvailable) {
        this.isAvailable = isAvailable;
    }

    public int getIsSlotExpired() {
        return isSlotExpired;
    }

    public void setIsSlotExpired(int isSlotExpired) {
        this.isSlotExpired = isSlotExpired;
    }
}
