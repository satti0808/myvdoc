package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class PatientPojo {

    @SerializedName("name")
    private String name;

    @SerializedName("age")
    private String age;


    public PatientPojo(String name, String age){
        this.setName(name);
        this.setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}