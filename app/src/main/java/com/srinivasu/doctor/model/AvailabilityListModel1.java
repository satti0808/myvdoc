package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AvailabilityListModel1 {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("data")
    @Expose
    private List<AvailabilityModel> data1;

    @SerializedName("leaves")
    @Expose
    private List<LeavesModel> leaves;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<AvailabilityModel> getData1() {
        return data1;
    }

    public void setData1(List<AvailabilityModel> data1) {
        this.data1 = data1;
    }

    public List<LeavesModel> getLeaves() {
        return leaves;
    }

    public void setLeaves(List<LeavesModel> leaves) {
        this.leaves = leaves;
    }
}
