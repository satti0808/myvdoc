package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class TestSkillModel {
    @SerializedName("id")
    private String id;

    @SerializedName("skill")
    private String skill;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}
