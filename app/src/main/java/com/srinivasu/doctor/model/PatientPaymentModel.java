package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientPaymentModel {
    @SerializedName("appoitmentID")
    private String appoitmentID;

    @SerializedName("DoctorName")
    private String DoctorName;

    @SerializedName("doctorID")
    private String doctorID;

    @SerializedName("date")
    private String mDate;

    @SerializedName("Amount")
    private String Amount;

    public String getAppoitmentID() {
        return appoitmentID;
    }

    public void setAppoitmentID(String appoitmentID) {
        this.appoitmentID = appoitmentID;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getDate() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(mDate);
            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }

    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
