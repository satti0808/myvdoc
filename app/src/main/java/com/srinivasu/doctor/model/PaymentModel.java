package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentModel {

    @SerializedName("appoitmentID")
    private String appoitmentID;

    @SerializedName("PatientName")
    private String PatientName;

    @SerializedName("patientID")
    private String patientID;

    @SerializedName("date")
    private String mdate;

    @SerializedName("Amount")
    private String Amount;

    public String getAppoitmentID() {
        return appoitmentID;
    }

    public void setAppoitmentID(String appoitmentID) {
        this.appoitmentID = appoitmentID;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getDate() {

        /*String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String outputPattern = "dd/MM/yyyy";
        LocalDateTime inputDate = null;
        String outputDate = null;
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern(inputPattern, Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(outputPattern, Locale.ENGLISH);
        inputDate = LocalDateTime.parse(date, inputFormatter);
        outputDate = outputFormatter.format(inputDate);
        return outputDate;*/
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(mdate);
            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }
    }

    public void setDate(String date) {
        this.mdate = date;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
