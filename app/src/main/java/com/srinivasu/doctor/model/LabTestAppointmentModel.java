package com.srinivasu.doctor.model;

import org.parceler.Parcel;

@Parcel
public class LabTestAppointmentModel {

    private String ID;
    private String AppointmentID;
    private String TestID;
    private LabTestDetails testmaster;

    public LabTestAppointmentModel() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAppointmentID() {
        return AppointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        AppointmentID = appointmentID;
    }

    public String getTestID() {
        return TestID;
    }

    public void setTestID(String testID) {
        TestID = testID;
    }

    public LabTestDetails getTestmaster() {
        return testmaster;
    }

    public void setTestmaster(LabTestDetails testmaster) {
        this.testmaster = testmaster;
    }
}
