package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class BPModel {
    @SerializedName("ID")
    private String id;

    @SerializedName("Date")
    private String Date;

    @SerializedName("Time")
    private String Time;

    @SerializedName("Systolic")
    private String Systolic;

    @SerializedName("Diastolic")
    private String Diastolic;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getSystolic() {
        return Systolic;
    }

    public void setSystolic(String systolic) {
        Systolic = systolic;
    }

    public String getDiastolic() {
        return Diastolic;
    }

    public void setDiastolic(String diastolic) {
        Diastolic = diastolic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
