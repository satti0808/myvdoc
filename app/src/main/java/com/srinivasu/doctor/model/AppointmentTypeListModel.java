package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentTypeListModel {
    @SerializedName("success")
    private String success;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<AppointmentTypeModel> data1;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AppointmentTypeModel> getData1() {
        return data1;
    }

    public void setData1(List<AppointmentTypeModel> data1) {
        this.data1 = data1;
    }
}
