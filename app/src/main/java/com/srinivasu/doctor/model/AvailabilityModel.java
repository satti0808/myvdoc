package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;

public class AvailabilityModel {
    @SerializedName("ID")
    private String ID;

    @SerializedName("Date")
    private String Date;

    @SerializedName("appointment_startTime")
    private String StartTime;

    @SerializedName("appointment_endTime")
    private String EndTime;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getStartTime() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            java.util.Date date = dt.parse(StartTime);
            SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }

    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            java.util.Date date = dt.parse(EndTime);
            SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }

    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }
}
