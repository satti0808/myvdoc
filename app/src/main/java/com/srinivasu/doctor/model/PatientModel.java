package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class PatientModel {
    @SerializedName("PatientName")
    private String PatientName;

    @SerializedName("PhoneNo")
    private String PhoneNo;

    @SerializedName("ID")
    private String ID;

    @SerializedName("Address")
    private String Address;

    @SerializedName("Sex")
    private String Sex;

    @SerializedName("DOB")
    private String DOB;


    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }
}
