package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class CitiesModel {
    @SerializedName("ID")
    private String ID;

    @SerializedName("city")
    private String city;

    @SerializedName("stateID")
    private String stateID;

    @SerializedName("countryID")
    private String countryID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }
}
