package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class AppointmentDetailsModel {

    private String appointment_ID;
    private String appointment_time;
    private String status;
    private String duration;
    private String patientID;
    private String ratinng;
    private String feedback;

    public String getAppointment_ID() {
        return appointment_ID;
    }

    public void setAppointment_ID(String appointment_ID) {
        this.appointment_ID = appointment_ID;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getRatinng() {
        return ratinng;
    }

    public void setRatinng(String ratinng) {
        this.ratinng = ratinng;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
