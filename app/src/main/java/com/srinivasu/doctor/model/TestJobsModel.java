package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestJobsModel {
    @SerializedName("category")
    private String category;

    @SerializedName("company")
    private String company;

    @SerializedName("created_on")
    private String created_on;

    @SerializedName("designation")
    private String designation;

    @SerializedName("job_valid_till")
    private String job_valid_till;

    @SerializedName("skills_required")
    @Expose
    private List<TestSkillModel> skills;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getJob_valid_till() {
        return job_valid_till;
    }

    public void setJob_valid_till(String job_valid_till) {
        this.job_valid_till = job_valid_till;
    }

    public List<TestSkillModel> getSkills() {
        return skills;
    }

    public void setSkills(List<TestSkillModel> skills) {
        this.skills = skills;
    }
}
