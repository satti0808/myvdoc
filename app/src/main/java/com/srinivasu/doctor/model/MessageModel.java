package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageModel {
    @SerializedName("Desc")
    @Expose
    private String Desc;

    @SerializedName("Urgency")
    @Expose
    private String Urgency;

    @SerializedName("dataTime")
    @Expose
    private String dataTime;


    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getUrgency() {
        return Urgency;
    }

    public void setUrgency(String urgency) {
        Urgency = urgency;
    }

    public String getDataTime() {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(dataTime);
            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }
    }
    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }
}
