package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class HomePojo {

    @SerializedName("image")
    private String image;

    @SerializedName("name")
    private String name;

    @SerializedName("time")
    private String time;

    public HomePojo(String image,String name,String time){
        this.setImage(image);
        this.setName(name);
        this.setTime(time);

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}