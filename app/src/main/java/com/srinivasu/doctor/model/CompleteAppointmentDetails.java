package com.srinivasu.doctor.model;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class CompleteAppointmentDetails {

    private int ID;
    private int DoctorID;
    private int PatientID;
    private String status;
    private String validFrom;
    private String validTo;
    private String startTime;
    private String endTime;
    private String AppointmentType;
    private String Note;
    private String VideoID;
    private UserDetails doctor;
    private UserDetails patient;
    private List<Attachments> attachments;
    private FeedbackDetails feedback;
    private List<PrescriptionModel> prescription;
    private List<LabTestAppointmentModel> testappointment;

    public CompleteAppointmentDetails() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(int doctorID) {
        DoctorID = doctorID;
    }

    public int getPatientID() {
        return PatientID;
    }

    public void setPatientID(int patientID) {
        PatientID = patientID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAppointmentType() {
        return AppointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        AppointmentType = appointmentType;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getVideoID() {
        return VideoID;
    }

    public void setVideoID(String videoID) {
        VideoID = videoID;
    }

    public UserDetails getDoctor() {
        return doctor;
    }

    public void setDoctor(UserDetails doctor) {
        this.doctor = doctor;
    }

    public UserDetails getPatient() {
        return patient;
    }

    public void setPatient(UserDetails patient) {
        this.patient = patient;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    public FeedbackDetails getFeedback() {
        return feedback;
    }

    public void setFeedback(FeedbackDetails feedback) {
        this.feedback = feedback;
    }

    public List<PrescriptionModel> getPrescription() {
        return prescription;
    }

    public void setPrescription(List<PrescriptionModel> prescription) {
        this.prescription = prescription;
    }

    public List<LabTestAppointmentModel> getTestappointment() {
        return testappointment;
    }

    public void setTestappointment(List<LabTestAppointmentModel> testappointment) {
        this.testappointment = testappointment;
    }
}
