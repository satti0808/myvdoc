package com.srinivasu.doctor.model;

import org.parceler.Parcel;

@Parcel
public class LabTestDetails {

    private String ID;
    private String Instructions;
    private String TestDesc;
    private String TestName;
    private String TestType;

    public LabTestDetails() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getInstructions() {
        return Instructions;
    }

    public void setInstructions(String instructions) {
        Instructions = instructions;
    }

    public String getTestDesc() {
        return TestDesc;
    }

    public void setTestDesc(String testDesc) {
        TestDesc = testDesc;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String testName) {
        TestName = testName;
    }

    public String getTestType() {
        return TestType;
    }

    public void setTestType(String testType) {
        TestType = testType;
    }
}
