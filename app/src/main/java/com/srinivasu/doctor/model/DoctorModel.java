package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorModel {
    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("ID")
    @Expose
    private String ID;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phoneNo")
    @Expose
    private String phoneNo;

    @SerializedName("role")
    @Expose
    private String role;

    @SerializedName("roleID")
    @Expose
    private String roleID;

    @SerializedName("token")
    @Expose
    private String token;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
