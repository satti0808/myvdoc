package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class StatesModel {
    @SerializedName("ID")
    private String ID;

    @SerializedName("state")
    private String state;

    @SerializedName("countryID")
    private String countryID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }
}
