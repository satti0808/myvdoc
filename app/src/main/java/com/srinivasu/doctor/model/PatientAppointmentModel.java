package com.srinivasu.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientAppointmentModel {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("data")
    @Expose
    private List<PatientAppointmentDataModel> data1;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<PatientAppointmentDataModel> getData1() {
        return data1;
    }

    public void setData1(List<PatientAppointmentDataModel> data1) {
        this.data1 = data1;
    }
}
