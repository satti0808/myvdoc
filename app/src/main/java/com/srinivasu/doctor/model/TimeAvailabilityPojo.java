package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class TimeAvailabilityPojo {

    @SerializedName("time")
    private String time;

    @SerializedName("status")
    private String status;


    public TimeAvailabilityPojo(String time, String status){
        this.setTime(time);
        this.setStatus(status);

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}