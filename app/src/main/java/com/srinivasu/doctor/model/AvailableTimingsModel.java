package com.srinivasu.doctor.model;

public class AvailableTimingsModel {
    private String time;
    private String status;

    public AvailableTimingsModel(String time,String status){
        this.time=time;
        this.status=status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
