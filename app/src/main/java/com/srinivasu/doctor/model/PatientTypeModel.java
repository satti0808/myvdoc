package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

public class PatientTypeModel {
    @SerializedName("ID")
    private String ID;

    @SerializedName("Type")
    private String Type;

    @SerializedName("Concession")
    private String Concession;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getConcession() {
        return Concession;
    }

    public void setConcession(String concession) {
        Concession = concession;
    }
}
