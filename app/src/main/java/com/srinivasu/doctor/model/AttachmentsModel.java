package com.srinivasu.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttachmentsModel {
    @SerializedName("images")
    private List<ImageModel> alImages;

    public List<ImageModel> getAlImages() {
        return alImages;
    }

    public void setAlImages(List<ImageModel> alImages) {
        this.alImages = alImages;
    }
}
