package com.srinivasu.doctor.dialogs;

import android.app.Activity;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import java.lang.ref.WeakReference;

public class CustomDialog {
    private WeakReference<Activity> mActivity;
    private AlertDialog customDialog;

    public CustomDialog(Activity activity){
        mActivity = new WeakReference<Activity>(activity);
    }

    public void show(View dialogView){
        show(true, dialogView);
    }

    public void show(String title, String message,  View dialogView){
        show(true, title, message, dialogView);
    }

    public void show(boolean isCancellable, View dialogView) {

        customDialog = new AlertDialog.Builder(mActivity.get()).create();
        customDialog.setView(dialogView);
        customDialog.setCancelable(isCancellable);

        customDialog.show();
    }

    public void show(boolean isCancellable, String title, String message, View dialogView) {

        customDialog = new AlertDialog.Builder(mActivity.get()).create();
        customDialog.setView(dialogView);
        customDialog.setTitle(title);
        customDialog.setMessage(message);
        customDialog.setCancelable(isCancellable);

        customDialog.show();
    }

    public void closeDialog(){
        if ( customDialog != null ){
            customDialog.dismiss();
        }
    }
}
