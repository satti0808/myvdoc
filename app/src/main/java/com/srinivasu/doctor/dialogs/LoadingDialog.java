package com.srinivasu.doctor.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.srinivasu.doctor.R;

import java.lang.ref.WeakReference;

public class LoadingDialog {

    private WeakReference<Activity> mActivity;
    Context context;
    private AlertDialog loadingDialog;
    ProgressBar loadingIndicator;

    public LoadingDialog(Activity activity){
        mActivity = new WeakReference<>(activity);
        context = activity;
    }

    public void show(CharSequence title) {
        show(false, title);
    }

    public void show(boolean isCancellable, CharSequence title) {

        loadingDialog = new AlertDialog.Builder(mActivity.get()).create();

        View loadingDialogView = LayoutInflater.from(mActivity.get()).inflate(R.layout.dialog_loading, null);
        loadingDialog.setView(loadingDialogView);
        loadingDialog.setCancelable(isCancellable);

        TextView loadingTitle = (TextView) loadingDialogView.findViewById(R.id.title);
        loadingTitle.setText(title);

        loadingDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK && !event.isCanceled()) {
                    if(loadingDialog.isShowing()) {
                        closeDialog();
                    }
                    return true;
                }
                return false;
            }
        });
        try {
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeDialog(){
        if ( loadingDialog != null ){
            loadingDialog.dismiss();
        }
    }

    public AlertDialog getDialog(){
        if (loadingDialog != null){
            return loadingDialog;
        }
        else {
            return null;
        }
    }
}
