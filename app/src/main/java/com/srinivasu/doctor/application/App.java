package com.srinivasu.doctor.application;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.ProcessLifecycleOwner;

import com.quickblox.auth.session.BaseService;
import com.quickblox.auth.session.QBSession;
import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.auth.session.QBSessionParameters;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.exception.BaseServiceException;
import com.srinivasu.doctor.util.QBResRequestExecutor;
import com.srinivasu.doctor.utils.BackgroundListener;

import java.util.Calendar;
import java.util.Date;


public class App extends Application {
    private static App instance;
    private QBResRequestExecutor qbResRequestExecutor;

    public static final String TAG = App.class.getSimpleName();

    private static final String APPLICATION_ID = "85262";
    private static final String AUTH_KEY = "77j3wW59OaPnYc-";
    private static final String AUTH_SECRET = "ELTzAAdwyzHDMfE";
    private static final String ACCOUNT_KEY = "RdTRtLZqDnzE7PFdaqrB";

    public static final String USER_DEFAULT_PASSWORD = "quickblox";

    private static final String QB_CONFIG_DEFAULT_FILE_NAME = "qb_config.json";
    private static final String API_DOMAIN = "https://apicustomdomain.quickblox.com";
    private static final String CHAT_DOMAIN = "chatcustomdomain.quickblox.com";

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        initQBSessionManager();
//        initQbConfigs();
        initCredentials();

//        setExpirationDate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new BackgroundListener());
    }

    public static App getInstance() {
        return instance;
    }

    private void setExpirationDate() {
        try {
            String token = BaseService.getBaseService().getToken();
            Date expirationDate = BaseService.getBaseService().getTokenExpirationDate();

            // recreate session on next start app
            Date currentDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.HOUR, 2);
            Date twoHoursAfter = cal.getTime();

            if (expirationDate.before(twoHoursAfter)) {

                QBSessionManager.getInstance().createActiveSession("31ed199120fb998dc472aea785a1825809ad5c04", expirationDate);

            } else {
                // create a session
                initQBSessionManager();
            }
        } catch (BaseServiceException e) {
            e.printStackTrace();
        }
    }

    public synchronized QBResRequestExecutor getQbResRequestExecutor() {
        return qbResRequestExecutor == null
                ? qbResRequestExecutor = new QBResRequestExecutor()
                : qbResRequestExecutor;
    }

    //////////////////////////

   /* private void initQbConfigs() {
        Log.e(TAG, "QB CONFIG FILE NAME: " + getQbConfigFileName());
        qbConfigs = ConfigUtils.getCoreConfigsOrNull(getQbConfigFileName());
    }*/


    public void initCredentials() {

        QBSettings.getInstance().init(getApplicationContext(), APPLICATION_ID, AUTH_KEY, AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(ACCOUNT_KEY);

        QBSettings.getInstance().setAutoCreateSession(true);
        Log.d("\n App.java", "initVideoCallingAPI \n \n");


        /*QBSettings.getInstance().setEndpoints(API_DOMAIN, CHAT_DOMAIN, ServiceZone.PRODUCTION);
        QBSettings.getInstance().setZone(ServiceZone.PRODUCTION);
        QBChatService.getInstance().setReconnectionAllowed(true);

        QBRTCMediaConfig.setAudioCodec(QBRTCMediaConfig.AudioCodec.ISAC);
        QBRTCMediaConfig.setAudioCodec(QBRTCMediaConfig.AudioCodec.OPUS);

        QBRTCMediaConfig.setVideoCodec(QBRTCMediaConfig.VideoCodec.H264);
        QBRTCMediaConfig.setVideoCodec(QBRTCMediaConfig.VideoCodec.VP8);
        QBRTCMediaConfig.setVideoCodec(QBRTCMediaConfig.VideoCodec.VP9);

        QBRTCMediaConfig.setVideoWidth(QBRTCMediaConfig.VideoQuality.QBGA_VIDEO.width);
        QBRTCMediaConfig.setVideoHeight(QBRTCMediaConfig.VideoQuality.QBGA_VIDEO.height);

        // Enable Hardware Acceleration if device supports it
        QBRTCMediaConfig.setVideoHWAcceleration(true);

        // Enable built-in AEC if device supports it
        QBRTCMediaConfig.setUseBuildInAEC(true);

        // Enable OpenSL ES audio if device supports it
        QBRTCMediaConfig.setUseOpenSLES(true);

        QBRTCMediaConfig.setAudioProcessingEnabled(true);*/


    }


//    public QbConfigs getQbConfigs() {
//        return qbConfigs;
//    }

    protected String getQbConfigFileName() {
        return QB_CONFIG_DEFAULT_FILE_NAME;
    }

    private void initQBSessionManager() {
        QBSessionManager.getInstance().addListener(new QBSessionManager.QBSessionListener() {
            @Override
            public void onSessionCreated(QBSession qbSession) {
                Log.d(TAG, "Session Created");
            }

            @Override
            public void onSessionUpdated(QBSessionParameters qbSessionParameters) {
                Log.d(TAG, "Session Updated");
            }

            @Override
            public void onSessionDeleted() {
                Log.d(TAG, "Session Deleted");
            }

            @Override
            public void onSessionRestored(QBSession qbSession) {
                Log.d(TAG, "Session Restored");
            }

            @Override
            public void onSessionExpired() {
                Log.d(TAG, "Session Expired");
            }

            @Override
            public void onProviderSessionExpired(String provider) {
                Log.d(TAG, "Session Expired for provider:" + provider);
            }
        });
    }
}
