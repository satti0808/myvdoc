package com.srinivasu.doctor.apis;

import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.AppointmentListApiModel;
import com.srinivasu.doctor.model.AppointmentTypeListModel;
import com.srinivasu.doctor.model.AttachmentsListModel;
import com.srinivasu.doctor.model.AvailabilityListModel1;
import com.srinivasu.doctor.model.BPListModel;
import com.srinivasu.doctor.model.CitiesModel;
import com.srinivasu.doctor.model.DiabetesListModel;
import com.srinivasu.doctor.model.DoctorAvailableSlot;
import com.srinivasu.doctor.model.HomeAppointmentListModel;
import com.srinivasu.doctor.model.MessageListModel;
import com.srinivasu.doctor.model.PatientAppointmentModel;
import com.srinivasu.doctor.model.PatientHomeAppointmenListtModel;
import com.srinivasu.doctor.model.PatientListModel1;
import com.srinivasu.doctor.model.PatientPaymentListModel;
import com.srinivasu.doctor.model.PatientTypeListModel;
import com.srinivasu.doctor.model.PaymentListModel;
import com.srinivasu.doctor.model.PrescriptionListModel;
import com.srinivasu.doctor.model.ProfileModel;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.model.ResultData;
import com.srinivasu.doctor.model.StatesListModel;
import com.srinivasu.doctor.model.TestListModel;
import com.srinivasu.doctor.model.UserDetails;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    String URL_BASE = "http://backend.myvdoc.in:3000/";

    @POST("user/Login")
    Call<ResponseObject<UserDetails>> getUser(@Body String body);

    /*Add Patient*/
    @POST("/doctor/create/patient/{dID}")
    Call<ResponseObject> addPatient( @Body String body, @Path("dID") String doctorID);

    @GET("/masters/cities/all")
    Call<ResponseObject<List<CitiesModel>>> getAllCities();

    @Headers("Content-Type: application/json")
    @POST("appointment/doctor/{doctorID}")
    Call<AppointmentListApiModel> getApointmentsOfADoctor(@Header("Authorization") String token, @Path("doctorID") String doctorID, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("appointment/list/{doctorID}")
    Call<HomeAppointmentListModel> getHomeApointmentsOfADoctor(@Header("Authorization") String token, @Path("doctorID") String doctorID, @Body String body);


    @Headers("Content-Type: application/json")
    @POST("appointment/patient/{pID}")
    Call<PatientAppointmentModel> getApointmentsOfAPatient(@Header("Authorization") String token, @Path("pID") String doctorID, @Body String body);

    @Headers("Content-Type: application/json")
    @GET("user/user/{id}")
    Call<ProfileModel> getProfile(@Header("Authorization") String token, @Path("id") String id);

    @Headers("Content-Type: application/json")
    @PUT("user/update/user/{id}")
    Call<ResultData> updateProfile(@Header("Authorization") String token, @Body String body, @Path("id") String id);



    @Headers("Content-Type: application/json")
    @GET("payment/doctor/{dID}")
    Call<PaymentListModel> getPaymentsOfAPatient(@Header("Authorization") String token, @Path("dID") String doctorID);

    @Headers("Content-Type: application/json")
    @GET("doctor/getpatientlist/{dID}")
    Call<PatientListModel1> getPaymentsOfAPatient2(@Header("Authorization") String token, @Path("dID") String doctorID);

    @Headers("Content-Type: application/json")
    @GET("payment/patient/{pid}")
    Call<PatientPaymentListModel> getPaymentsOfAPatient1(@Header("Authorization") String token, @Path("pid") String doctorID);



    @Headers("Content-Type: application/json")
    @GET("patient/emergencymsg/doctor/{dID}")
    Call<MessageListModel> getMessages(@Header("Authorization") String token, @Path("dID") String doctorID);

    @Headers("Content-Type: application/json")
    @POST("/doctor/leave/create")
    Call<ResponseObject>  addLeave(@Header("Authorization") String token, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("/medicines/create")
    Call<ResponseObject> addMedicine(@Header("Authorization") String token, @Body String body);

    @Headers("Content-Type: application/json")
    @PUT("/appointment/decline/{pID}")
    Call<ResultData> approve(@Header("Authorization") String token, @Body String body,@Path("pID") String pID);

    @Headers("Content-Type: application/json")
    @PUT("/appointment/postpone/{pID}")
    Call<ResultData> postpone(@Header("Authorization") String token, @Body String body,@Path("pID") String pID);



    @Headers("Content-Type: application/json")
    @GET("/masters/states/all")
    Call<StatesListModel> getAllStates(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("/patient/typesByDoctor/{ID}")
    Call<PatientTypeListModel> getPatientType(@Header("Authorization") String token, @Path("ID") String ID,
                                              @Query("apiVersion")String apiVersion );

    @Headers("Content-Type: application/json")
    @GET("/appointment/tests/all")
    Call<TestListModel> getTests(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("patient/bp/create")
    Call<ResultData> addBPDetails(@Header("Authorization") String token,  @Body String body);

    @Headers("Content-Type: application/json")
    @PUT("patient/update/patientbp/{id}")
    Call<ResultData> editBPDetails(@Header("Authorization") String token,  @Body String body,@Path("id") String id);

    @Headers("Content-Type: application/json")
    @PUT("patient/update/patientdiabeties/{id}")
    Call<ResultData> editDiabetesDetails(@Header("Authorization") String token,  @Body String body,@Path("id") String id);



    @Headers("Content-Type: application/json")
    @PUT("appointment/note/doctor/{aID}")
    Call<ResultData> addNote(@Header("Authorization") String token,  @Body String body,@Path("aID") String aID);

    @Headers("Content-Type: application/json")
    @POST("appointment/test/create")
    Call<ResultData> addTests(@Header("Authorization") String token,  @Body String body);

    @Headers("Content-Type: application/json")
    @POST("appointment/feedback/create")
    Call<ResultData> addFeedback(@Header("Authorization") String token,  @Body String body);

    @Headers("Content-Type: application/json")
    @POST("patient/diabetes/create")
    Call<ResultData> addDiabetesDetails(@Header("Authorization") String token,  @Body String body);

    @Headers("Content-Type: application/json")
    @POST("doctor/availability/{dID}")
    Call<AvailabilityListModel1> getAvailability(@Header("Authorization") String token, @Body String body, @Path("dID") String doctorID);

    @Headers("Content-Type: application/json")
    @GET("medicines/prescription/{pID}")
    Call<PrescriptionListModel> getPrescription(@Header("Authorization") String token, @Path("pID") String doctorID);

    @Headers("Content-Type: application/json")
    @GET("medicines/prescription/appointment/{aID}")
    Call<PrescriptionListModel> getMedicines(@Header("Authorization") String token, @Path("aID") String doctorID);

    @Headers("Content-Type: application/json")
    @POST("appointment/home/patient/{pID}")
    Call<ResponseObject<List<AppointmentDetails>>> getHomeApointmentsOfAPatient(@Header("Authorization") String token, @Path("pID") String doctorID, @Body String body);

    @Headers("Content-Type: application/json")
    @DELETE("patient/delete/diabeties/{id}")
    Call<ResultData> deleteDiabetes(@Header("Authorization") String token,  @Path("id") String id);

    @Headers("Content-Type: application/json")
    @DELETE("patient/delete/bp/{id}")
    Call<ResultData> deleteBP(@Header("Authorization") String token,  @Path("id") String id);
    //patient/delete/diabetes/:id

    @Multipart
    @POST("appointment/upload/images/{aID}")
    Call<ResultData> addAttachment(@Header("Authorization") String token,@Part MultipartBody.Part file, @Path("aID") String aid);

    @Headers("Content-Type: application/json")
    @GET("appointment/attachments/{aID}")
    Call<AttachmentsListModel> getImagesList(@Header("Authorization") String token, @Path("aID") String appointmentID);

}