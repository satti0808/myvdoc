package com.srinivasu.doctor.apis;

import android.content.Context;
import android.net.ConnectivityManager;

import com.srinivasu.doctor.BuildConfig;
import com.srinivasu.doctor.apis.interceptors.RequestInterceptor;
import com.srinivasu.doctor.db.LocalStorageData;
import com.srinivasu.doctor.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetroClient {
    private static Retrofit retrofit;
    private Context context;
    public LocalStorageData localStorageData;

    public RetroClient(Context context) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
    }

    public LocalStorageData getLocalStorageData(){
        if (localStorageData == null){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(Constants.ROOT_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHeaders())
                .build();
    }

    public static ApiInterface getApiService() {
        return getRetrofitInstance().create(ApiInterface.class);
    }

    private static OkHttpClient getHeaders() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(final Chain chain) throws IOException {
                Request original = chain.request();
                Request modifiedRequest = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .build();
                return chain.proceed(modifiedRequest);
            }
        });

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            builder.addInterceptor(interceptor);
        }

        return builder.build();
    }

    public Retrofit getNewRetrofit() {
        if(retrofit== null){
            return new Retrofit.Builder()
                    .baseUrl(Constants.ROOT_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getNewHeaders())
                    .build();
        }
       return retrofit;
    }
    private OkHttpClient getNewHeaders() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(final Chain chain) throws IOException {
                Request original = chain.request();
                Request modifiedRequest = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", getLocalStorageData().getToken())
                        .build();
                return chain.proceed(modifiedRequest);
            }
        });
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        builder.addInterceptor(new RequestInterceptor(connectivityManager));

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            builder.addInterceptor(interceptor);
        }

        return builder.build();

    }

}
