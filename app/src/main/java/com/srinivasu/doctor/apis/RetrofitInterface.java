package com.srinivasu.doctor.apis;

import com.srinivasu.doctor.apis.models.RequestAddMedicine;
import com.srinivasu.doctor.apis.models.RequestAddPatient;
import com.srinivasu.doctor.apis.models.RequestGetAppointments;
import com.srinivasu.doctor.apis.models.RequestLogin;
import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.AppointmentDetailsModel;
import com.srinivasu.doctor.model.AppointmentTypeListModel;
import com.srinivasu.doctor.model.BPListModel;
import com.srinivasu.doctor.model.CitiesModel;
import com.srinivasu.doctor.model.CompleteAppointmentDetails;
import com.srinivasu.doctor.model.DiabetesListModel;
import com.srinivasu.doctor.model.DoctorAvailableSlot;
import com.srinivasu.doctor.model.PatientPaymentModel;
import com.srinivasu.doctor.model.PatientTypeModel;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.model.ResultData;
import com.srinivasu.doctor.model.StatesModel;
import com.srinivasu.doctor.model.UserDetails;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitInterface {

    /*Login Services*/
    @Headers("Content-Type: application/json")
    @POST("user/Login")
    Call<ResponseObject<UserDetails>> getUser(@Body RequestLogin body);

    /*Add Patient Services*/
    @Headers("Content-Type: application/json")
    @POST("/doctor/create/patient/{dID}")
    Call<ResponseObject> addPatient(@Body RequestAddPatient body, @Path("dID") String doctorID);

    @Headers("Content-Type: application/json")
    @GET("/masters/cities/all")
    Call<ResponseObject<List<CitiesModel>>> getAllCities();

    @Headers("Content-Type: application/json")
    @GET("/masters/states/all")
    Call<ResponseObject<List<StatesModel>>> getAllStates();

    @Headers("Content-Type: application/json")
    @GET("/patient/typesByDoctor/{ID}")
    Call<ResponseObject<List<PatientTypeModel>>> getPatientType(@Path("ID") String ID,
                                                                @Query("apiVersion")String apiVersion );

    @Headers("Content-Type: application/json")
    @POST("/medicines/create")
    Call<ResponseObject> addMedicine(@Body RequestAddMedicine body);


    /*Patient Home appointments*/
//    @Headers("Content-Type: application/json")
    @POST("appointment/home/patient/{Pid}")
    Call<ResponseObject<List<AppointmentDetails>>>getPatientAppointments(@Path("Pid")String patientId, @Body RequestGetAppointments body);

    @Headers("Content-Type: application/json")
    @POST("doctor/availability/{dID}")
    Call<ResponseObject<List<DoctorAvailableSlot>>> getDoctorAvailability(@Body String body, @Path("dID") String doctorI,
                                                                          @Query("apiVersion") String apiVersion);

    @Headers("Content-Type: application/json")
    @POST("appointment/create")
    Call<ResultData> addAppointment(@Body String body);

    @Headers("Content-Type: application/json")
    @POST("appointment/type")
    Call<AppointmentTypeListModel> getAppointType(@Body String body);

    @Headers("Content-Type: application/json")
    @GET("patient/diabetes/{pID}")
    Call<DiabetesListModel> getDiabetesOfAPatient(@Path("pID") String patientId);

    @Headers("Content-Type: application/json")
    @GET("patient/bp/{pID}")
    Call<BPListModel> getBPOfAPatient(@Path("pID") String patientId);

    @Headers("Content-Type: application/json")
    @GET("appointment/patient/appointmentlist/{Pid}")
    Call<ResponseObject<List<AppointmentDetails>>>getPatientAppointmentHistory(@Path("Pid")String patientId, @Query("page")int page,
                                                                               @Query("limit")int limit);

    @Headers("Content-Type: application/json")
    @GET("payment/patient/{pID}")
    Call<ResponseObject<List<PatientPaymentModel>>> getPatientPayments(@Path("pID") String patientId);

    @Headers("Content-Type: application/json")
    @GET("appointment/{AppointmentID}")
    Call<ResponseObject<List<CompleteAppointmentDetails>>>getAppointmentDetailsByID(@Path("AppointmentID") String AppointmentID);

    @Multipart
    @POST("appointment/upload/images/{appointmentID}")
    Call<ResponseObject> uploadFile(@Path("appointmentID") String appointmentID, @PartMap() Map<String, RequestBody> partMap,
                                    @Part MultipartBody.Part image);
}
