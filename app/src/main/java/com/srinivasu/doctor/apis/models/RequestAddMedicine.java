package com.srinivasu.doctor.apis.models;

public class RequestAddMedicine {

    private String Name;
    private String DrugName;
    private String Route;
    private String Frequency;
    private String Strength;
    private int Price;;
    private String ExpDate;
    private String MfgName;
    private String Disp;

    public RequestAddMedicine(String name, String drugName, String route, String frequency, String strength) {
        Name = name;
        DrugName = drugName;
        Route = route;
        Frequency = frequency;
        Strength = strength;
        Price = 10;
        ExpDate = "202-12-11";
        MfgName = "2020-12-11";
        Disp = "1";
    }
}
