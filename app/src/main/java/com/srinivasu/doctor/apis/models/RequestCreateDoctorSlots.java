package com.srinivasu.doctor.apis.models;

import com.srinivasu.doctor.model.SingleDaySlotItem;

import java.util.List;

public class RequestCreateDoctorSlots {

    String doctorId;
    String slotDuration;
    List<SingleDaySlotItem> availableDays;

    public RequestCreateDoctorSlots(String doctorId, String slotDuration, List<SingleDaySlotItem> availableDays) {
        this.doctorId = doctorId;
        this.slotDuration = slotDuration;
        this.availableDays = availableDays;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getSlotDuration() {
        return slotDuration;
    }

    public void setSlotDuration(String slotDuration) {
        this.slotDuration = slotDuration;
    }

    public List<SingleDaySlotItem> getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(List<SingleDaySlotItem> availableDays) {
        this.availableDays = availableDays;
    }
}
