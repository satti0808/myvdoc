package com.srinivasu.doctor.apis.models;

public class RequestLogin {

    String email;
    String password;

    public RequestLogin(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
