package com.srinivasu.doctor.apis.models;

public class RequestAddPatient {

    private String  PatientName;
    private String  Address;
    private String  Email;
    private String  Sex;
    private String  age;
    private String  PhoneNo;
    private String  TblCountry_ID;
    private String  PatientType;
    private String  TblState_ID;
    private String  TblCity_ID;

    public RequestAddPatient(String patientName, String address, String email, String sex, String age, String phoneNo,
                             String tblCountry_ID, String patientType, String tblState_ID, String tblCity_ID) {
        PatientName = patientName;
        Address = address;
        Email = email;
        Sex = sex;
        this.age = age;
        PhoneNo = phoneNo;
        TblCountry_ID = tblCountry_ID;
        PatientType = patientType;
        TblState_ID = tblState_ID;
        TblCity_ID = tblCity_ID;
    }
}
