package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.interfaces.ActionCallback;
import com.srinivasu.doctor.model.AppointmentDetails;

import java.util.List;

public class PatientMoreAppointmentsAdapter extends RecyclerView.Adapter<PatientMoreAppointmentsAdapter.ViewHolder> {

    Context context;
    List<AppointmentDetails> data;
    ActionCallback callback;
    boolean isCardVisible;

    public PatientMoreAppointmentsAdapter(Context context, List<AppointmentDetails> appointmentDetailsList, boolean isCardVisible, ActionCallback callback) {
        this.context = context;
        this.data = appointmentDetailsList;
        this.callback = callback;
        this.isCardVisible = isCardVisible;
    }


    @Override
    public PatientMoreAppointmentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_patient_appointment_details, parent, false);
        return new PatientMoreAppointmentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientMoreAppointmentsAdapter.ViewHolder holder, final int pos) {

        if(data.get(pos)!= null){
            AppointmentDetails details = data.get(pos);
            if(isCardVisible){
                holder.cardAppointment.setElevation(context.getResources().getDimension(R.dimen._4sdp));
            }
            if(!TextUtils.isEmpty(details.getDoctorName())){
                holder.textDoctorName.setText(details.getDoctorName());
            }
            if(!TextUtils.isEmpty(details.getAppointmentDate())){
                holder.TextAppointmentDate.setText(details.getAppointmentDate());
            }
            if(!TextUtils.isEmpty(details.getAppointmentTime())){
                holder.textAppointmentTime.setText(details.getAppointmentTime());
            }

            holder.textAppointmentTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(pos);
                }
            });

            holder.TextAppointmentDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(pos);
                }
            });

            holder.textDoctorName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(pos);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    public void setItems(List<AppointmentDetails> items){
        data.clear();
        notifyDataSetChanged();
        addItems(items);
    }

    public void addItems(List<AppointmentDetails> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                data.add(items.get(i));
                notifyItemInserted(data.size() - 1);
            }
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardAppointment;
        TextView textDoctorName, textAppointmentTime, TextAppointmentDate;


        public ViewHolder(View itemView) {
            super(itemView);
            cardAppointment= (CardView) itemView.findViewById(R.id.card_appointment_details);

            textDoctorName = (TextView) itemView.findViewById(R.id.tv_doctor_name);
            textAppointmentTime = (TextView) itemView.findViewById(R.id.tv_appointment_time);
            TextAppointmentDate = (TextView) itemView.findViewById(R.id.tv_appointment_date);
        }
    }
}