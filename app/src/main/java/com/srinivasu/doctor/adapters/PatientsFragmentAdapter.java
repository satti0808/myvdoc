package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.ReportsActivity;
import com.srinivasu.doctor.model.PaymentModel;

import java.util.List;

public class PatientsFragmentAdapter extends RecyclerView.Adapter<PatientsFragmentAdapter.MyviewHolder> {

    Context context;
    List<PaymentModel> a1;

    public PatientsFragmentAdapter(Context context, List<PaymentModel> patient) {
        this.context = context;
        this.a1 = patient;
    }


    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_patients, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {

        holder.tv_name.setText(a1.get(pos).getPatientName());
        holder.tv_age.setText(a1.get(pos).getDate());


        holder.tv_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intentg = new Intent(context, ReportsActivity.class);
                intentg.putExtra("pid",a1.get(pos).getPatientID());
                intentg.putExtra("name",a1.get(pos).getPatientName());
                context.startActivity(intentg);

            }
        });

    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_age;
        CardView tv_layout;


        public MyviewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_age = (TextView) itemView.findViewById(R.id.tv_age);
            tv_layout = (CardView) itemView.findViewById(R.id.card_view);


        }
    }
}