package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.LabTestAppointmentModel;
import com.srinivasu.doctor.model.LabTestDetails;
import com.srinivasu.doctor.model.PrescriptionModel;

import java.util.ArrayList;
import java.util.List;

public class ViewPatLabTestAdapter extends RecyclerView.Adapter<ViewPatLabTestAdapter.ViewHolder> {

    private Context context;
    private List<LabTestAppointmentModel> data;

    public ViewPatLabTestAdapter(Context context, List<LabTestAppointmentModel> list) {
        this.context = context;
        this.data = list;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewPatLabTestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_lab_test, parent, false);
        return new ViewPatLabTestAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewPatLabTestAdapter.ViewHolder holder, final int position) {

        if(data!= null){
            LabTestDetails item = data.get(position).getTestmaster();
            holder.textTestName.setText(item.getTestName());
            holder.textDescription.setText(item.getTestDesc());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItems(List<LabTestAppointmentModel> items){
        data = new ArrayList<>();
        notifyDataSetChanged();
        addItems(items);
    }

    public void addItems(List<LabTestAppointmentModel> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                data.add(items.get(i));
                notifyItemInserted(data.size() - 1);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textTestName, textDescription;

        public ViewHolder(View itemView) {
            super(itemView);

            textTestName = (TextView) itemView.findViewById(R.id.tv_test_name);
            textDescription = (TextView) itemView.findViewById(R.id.tv_description);



        }
    }
}
