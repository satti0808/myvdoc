package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.PrescriptionModel;

import java.util.List;

public class MedicineListAdapter extends RecyclerView.Adapter<MedicineListAdapter.MyviewHolder> {

    Context context;
    List<PrescriptionModel> a1;

    public MedicineListAdapter(Context context, List<PrescriptionModel> messages) {
        this.context = context;
        this.a1 = messages;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_medicine_list, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {
        holder.tv_name.setText(a1.get(pos).getMedicineMaster().getName());
        holder.tv_drugName.setText(a1.get(pos).getMedicineMaster().getDrugName());
        holder.tv_route.setText(a1.get(pos).getMedicineMaster().getRoute());
        holder.tv_frequency.setText(a1.get(pos).getMedicineMaster().getFrequency());
        holder.tv_strength.setText(a1.get(pos).getMedicineMaster().getStrength());
        holder.tv_disp.setText(a1.get(pos).getMedicineMaster().getDisp());
        holder.tv_disp.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_drugName,tv_route,tv_frequency,tv_strength,tv_disp;
        public MyviewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_drugName = (TextView) itemView.findViewById(R.id.tv_drugName);
            tv_route = (TextView) itemView.findViewById(R.id.tv_route);
            tv_frequency = (TextView) itemView.findViewById(R.id.tv_frequency);
            tv_strength = (TextView) itemView.findViewById(R.id.tv_strength);
            tv_disp = (TextView) itemView.findViewById(R.id.tv_disp);
        }
    }
}