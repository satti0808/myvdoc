package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.PatientHomeAppointmentModel;
import com.srinivasu.doctor.utils.Utils;

import java.util.List;


public class PatientHomeAdapter extends RecyclerView.Adapter<PatientHomeAdapter.MyviewHolder> {

    Context context;
    List<PatientHomeAppointmentModel> a1;

    public PatientHomeAdapter(Context context, List<PatientHomeAppointmentModel> patient) {
        this.context = context;
        this.a1 = patient;
    }


    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_patient_home, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {

        holder.tv_name.setText(a1.get(pos).getDoctor_name());
        holder.tv_date.setText(Utils.displayDateFormat(a1.get(pos).getAppointment_date()));
        holder.tv_time.setText(a1.get(pos).getAppointment_time());
        holder.ivPhone.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_date,tv_time;
        ImageView ivPhone;


        public MyviewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);

            ivPhone = (ImageView) itemView.findViewById(R.id.ivPhone);

        }
    }
}