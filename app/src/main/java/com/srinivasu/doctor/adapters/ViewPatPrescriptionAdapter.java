package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.PrescriptionModel;
import com.srinivasu.doctor.model.SingleDaySlotItem;

import java.util.ArrayList;
import java.util.List;

public class ViewPatPrescriptionAdapter extends RecyclerView.Adapter<ViewPatPrescriptionAdapter.ViewHolder> {

    private Context context;
    private List<PrescriptionModel> data;

    public ViewPatPrescriptionAdapter(Context context, List<PrescriptionModel> list) {
        this.context = context;
        this.data = list;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewPatPrescriptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_medicine, parent, false);
        return new ViewPatPrescriptionAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewPatPrescriptionAdapter.ViewHolder holder, final int position) {

        if(data!= null){

            PrescriptionModel item = data.get(position);
            holder.textMedicineName.setText(item.getMedicineMaster().getName());
            holder.textDrugName.setText(item.getMedicineMaster().getDrugName());
            holder.textStrength.setText(item.getMedicineMaster().getStrength());
            holder.textFrequency.setText(item.getMedicineMaster().getFrequency());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItems(List<PrescriptionModel> items){
        data = new ArrayList<>();
        notifyDataSetChanged();
        addItems(items);
    }

    public void addItems(List<PrescriptionModel> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                data.add(items.get(i));
                notifyItemInserted(data.size() - 1);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textMedicineName, textDrugName, textFrequency, textStrength;

        public ViewHolder(View itemView) {
            super(itemView);

            textMedicineName = (TextView) itemView.findViewById(R.id.tv_medicine_name);
            textDrugName = (TextView) itemView.findViewById(R.id.tv_drug_name);
            textFrequency = (TextView) itemView.findViewById(R.id.tv_frequency);
            textStrength = (TextView) itemView.findViewById(R.id.tv_strength);


        }
    }
}
