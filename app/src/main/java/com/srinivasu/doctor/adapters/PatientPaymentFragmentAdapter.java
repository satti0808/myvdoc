package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.PatientPaymentModel;
import com.srinivasu.doctor.utils.DateMethods;
import com.srinivasu.doctor.utils.Utils;

import java.util.List;

public class PatientPaymentFragmentAdapter extends RecyclerView.Adapter<PatientPaymentFragmentAdapter.MyviewHolder> {

    Context context;
    List<PatientPaymentModel> a1;

    public PatientPaymentFragmentAdapter(Context context, List<PatientPaymentModel> payment) {
        this.context = context;
        this.a1 = payment;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_payment, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {


        holder.tv_name.setText(a1.get(pos).getDoctorName());
        holder.tv_date.setText(DateMethods.getDateInFormat(a1.get(pos).getDate(), DateMethods.DATE_FORMAT ));
        holder.tv_amount.setText("\u20B9"+a1.get(pos).getAmount()+"/-");
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,a1.get(pos).getAppoitmentID(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_date,tv_amount;


        public MyviewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_amount = (TextView) itemView.findViewById(R.id.tv_amount);
        }
    }
}
