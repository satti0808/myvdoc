package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.PatientDetailsActivity;
import com.srinivasu.doctor.model.AppointmentDetailsModel;

import java.util.List;

public class TimeAvailabilityAdapter extends RecyclerView.Adapter<TimeAvailabilityAdapter.MyviewHolder> {

    Context context;
    List<AppointmentDetailsModel> appointments;
    String sdate="";

    public TimeAvailabilityAdapter(Context context, List<AppointmentDetailsModel> timeavailability,String sdate) {
        this.context = context;
        this.appointments = timeavailability;
        this.sdate=sdate;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_time_availability, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull MyviewHolder holder, final int pos) {

        holder.tv_time.setText(appointments.get(pos).getAppointment_time() + " (" + appointments.get(pos).getDuration() + ")");
        holder.tv_status.setText(appointments.get(pos).getStatus());

        holder.ratingBar.setMax(5);
        holder.ratingBar.setStepSize(0.1f);
        if (appointments.get(pos).getRatinng() != null){
            holder.ratingBar.setRating(Float.parseFloat(appointments.get(pos).getRatinng()));
    }else{
            holder.ratingBar.setRating(0.0f);
        }
        holder.tv_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.tv_feedback_det.getVisibility()==View.VISIBLE){
                    holder.tv_feedback_det.setVisibility(View.GONE);
                    holder.tv_feedback.setText("View Feedback (+)");
                }else{
                    holder.tv_feedback_det.setVisibility(View.VISIBLE);
                    holder.tv_feedback.setText("View Feedback (-)");
                }
            }
        });
        holder.tv_feedback_det.setText(appointments.get(pos).getFeedback());

        holder.tv_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context,""+appointments.get(pos).getPatientID(),Toast.LENGTH_SHORT).show();
                Intent intentg= new Intent(context, PatientDetailsActivity.class);
                intentg.putExtra("pid",appointments.get(pos).getPatientID());
                intentg.putExtra("aid",appointments.get(pos).getAppointment_ID());
                intentg.putExtra("sdate",sdate);
                context.startActivity(intentg);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (appointments != null) {
            return appointments.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView  tv_time,tv_status,tv_feedback,tv_feedback_det;
        RatingBar ratingBar;
        CardView tv_layout;



        public MyviewHolder(View itemView) {
            super(itemView);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_feedback= (TextView) itemView.findViewById(R.id.tv_feedback);
            tv_feedback_det= (TextView) itemView.findViewById(R.id.tv_feedback_det);
            ratingBar= (RatingBar) itemView.findViewById(R.id.ratingBar);

            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
            tv_layout = (CardView) itemView.findViewById(R.id.cv);

        }
    }
}