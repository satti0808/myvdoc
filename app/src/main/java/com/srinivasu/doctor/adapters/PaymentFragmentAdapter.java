package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.PaymentModel;
import com.srinivasu.doctor.utils.Utils;

import java.util.List;

public class PaymentFragmentAdapter extends RecyclerView.Adapter<PaymentFragmentAdapter.MyviewHolder> {

    Context context;
    List<PaymentModel> a1;

    public PaymentFragmentAdapter(Context context, List<PaymentModel> payment) {
        this.context = context;
        this.a1 = payment;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_payment, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {

        holder.tv_name.setText(a1.get(pos).getPatientName());
        holder.tv_date.setText(Utils.displayDateFormat(a1.get(pos).getDate()));
        holder.tv_amount.setText("Rs "+a1.get(pos).getAmount()+"/-");
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,a1.get(pos).getAppoitmentID(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView  tv_name,tv_date,tv_amount;


        public MyviewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_amount = (TextView) itemView.findViewById(R.id.tv_amount);
        }
    }
}

/*extends BaseAdapter {
    List<PaymentPojo> ar;
    Context cnt;

    public PaymentFragmentAdapter(List<PaymentPojo> ar, Context cnt) {
        this.ar = ar;
        this.cnt = cnt;
    }

    @Override
    public int getCount() {
        return ar.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int pos, View view, ViewGroup viewGroup) {
        LayoutInflater obj1 = (LayoutInflater) cnt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View obj2 = obj1.inflate(R.layout.list_payment, null);

        TextView tv_name = (TextView) obj2.findViewById(R.id.tv_name);
        tv_name.setText(ar.get(pos).getName());

        TextView tv_date = (TextView) obj2.findViewById(R.id.tv_date);
        tv_date.setText(ar.get(pos).getDate());

        TextView tv_amount = (TextView) obj2.findViewById(R.id.tv_amount);
        tv_amount.setText(ar.get(pos).getAmount());


        return obj2;
    }
}*/