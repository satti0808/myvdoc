package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.MessageModel;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyviewHolder> {

    Context context;
    List<MessageModel> a1;

    public MessageAdapter(Context context, List<MessageModel> messages) {
        this.context = context;
        this.a1 = messages;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_messages, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {


        holder.tv_desc.setText(a1.get(pos).getDesc());
        holder.tv_date.setText("Post Date : "+ a1.get(pos).getDataTime());
        holder.tv_urgency.setText("Level : "+a1.get(pos).getUrgency());

    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_desc,tv_date,tv_urgency;


        public MyviewHolder(View itemView) {
            super(itemView);
            tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_urgency = (TextView) itemView.findViewById(R.id.tv_urgency);





        }
    }
}

/*extends BaseAdapter {
    List<PaymentPojo> ar;
    Context cnt;

    public PaymentFragmentAdapter(List<PaymentPojo> ar, Context cnt) {
        this.ar = ar;
        this.cnt = cnt;
    }

    @Override
    public int getCount() {
        return ar.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int pos, View view, ViewGroup viewGroup) {
        LayoutInflater obj1 = (LayoutInflater) cnt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View obj2 = obj1.inflate(R.layout.list_payment, null);

        TextView tv_name = (TextView) obj2.findViewById(R.id.tv_name);
        tv_name.setText(ar.get(pos).getName());

        TextView tv_date = (TextView) obj2.findViewById(R.id.tv_date);
        tv_date.setText(ar.get(pos).getDate());

        TextView tv_amount = (TextView) obj2.findViewById(R.id.tv_amount);
        tv_amount.setText(ar.get(pos).getAmount());


        return obj2;
    }
}*/