package com.srinivasu.doctor.adapters.interfaces;

public interface ActionCallback {

    void onItemClick(int position);
}
