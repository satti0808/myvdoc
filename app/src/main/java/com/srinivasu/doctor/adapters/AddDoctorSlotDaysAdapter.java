package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.SingleDaySlotItem;
import com.srinivasu.doctor.widgets.SwipeToDeleteCallback;

import java.util.ArrayList;
import java.util.List;

public class AddDoctorSlotDaysAdapter extends RecyclerView.Adapter<AddDoctorSlotDaysAdapter.ViewHolder> {

    private Context context;
    private List<SingleDaySlotItem> data = new ArrayList<>();
    private CallBack callBack;

    public AddDoctorSlotDaysAdapter(Context context, List<SingleDaySlotItem> list, AddDoctorSlotDaysAdapter.CallBack callback) {
        this.context = context;
        this.data = list;
        this.callBack = callback;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_add_doctor_slot_availability, parent, false);
        return new AddDoctorSlotDaysAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AddDoctorSlotDaysAdapter.ViewHolder holder, int position) {
        if (data.get(position) != null) {
            SingleDaySlotItem singleDaySlotItem = data.get(position);
            holder.textDay.setText(singleDaySlotItem.getDay());
            holder.switchCompat.setChecked(singleDaySlotItem.isDoctorAvailable());
            holder.switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        holder.llHoursSlot.setVisibility(View.VISIBLE);
                        holder.textAddHours.setVisibility(View.VISIBLE);
                        holder.addSlotHoursAdapter.setItems(data.get(holder.getAdapterPosition()).getSlots());
                    } else {
                        holder.llHoursSlot.setVisibility(View.GONE);
                        holder.textAddHours.setVisibility(View.GONE);
                        holder.addSlotHoursAdapter.clearItems();
                    }
                }
            });

            holder.textAddHours.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.slotHoursHolder.getVisibility() == View.GONE) {
                        holder.slotHoursHolder.setVisibility(View.VISIBLE);
                        holder.addSlotHoursAdapter.setItems(data.get(holder.getAdapterPosition()).getSlots());
                    } else {
                        List<SingleDaySlotItem.TimeSlots> timeSlots = new ArrayList<>();
                        timeSlots.add(new SingleDaySlotItem.TimeSlots("", ""));
                        holder.addSlotHoursAdapter.addItems(timeSlots);

                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItems(List<SingleDaySlotItem> items) {
        data.clear();
        notifyDataSetChanged();
        addItems(items);
    }

    public void addItems(List<SingleDaySlotItem> items) {
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                data.add(items.get(i));
                notifyItemInserted(data.size() - 1);
            }
        }
    }

    public void setItem(SingleDaySlotItem dataEntity, int position) {

        data.set(position, dataEntity);
        notifyItemChanged(position);

    }

    public void deleteItem(int position) {
        if (position >= 0 && position < data.size()) {
            data.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, data.size());
        }
    }

    public SingleDaySlotItem getItem(int position) {
        if (position < data.size()) {
            return data.get(position);
        }
        return null;
    }

    public interface CallBack {
        void onSwitchChanged(int position);

        void onAddHoursClicked(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        SwitchCompat switchCompat;
        TextView textDay, textAddHours;

        RecyclerView slotHoursHolder;
        AddSlotHoursAdapter addSlotHoursAdapter;
        LinearLayout llHoursSlot;

        public ViewHolder(View itemView) {
            super(itemView);

            switchCompat = (SwitchCompat) itemView.findViewById(R.id.day_switch);
            textDay = (TextView) itemView.findViewById(R.id.tv_day_name);
            textAddHours = (TextView) itemView.findViewById(R.id.tv_add_hour);

            llHoursSlot = (LinearLayout) itemView.findViewById(R.id.ll_hours_slot);
            slotHoursHolder = (RecyclerView) itemView.findViewById(R.id.rv_layout_hours);
            slotHoursHolder.setHasFixedSize(true);
            slotHoursHolder.setNestedScrollingEnabled(false);
            slotHoursHolder.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
            addSlotHoursAdapter = new AddSlotHoursAdapter(context, new ArrayList<>());
            slotHoursHolder.setAdapter(addSlotHoursAdapter);
            SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(context) {
                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                    final int position = viewHolder.getAdapterPosition();
                    addSlotHoursAdapter.deleteItem(position);
                    if (addSlotHoursAdapter.getItemCount() <= 0) {
                        slotHoursHolder.setVisibility(View.GONE);
                    }
                }
            };
            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
            itemTouchhelper.attachToRecyclerView(slotHoursHolder);
            RecyclerView.OnItemTouchListener mScrollTouchListener = new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    int action = e.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_MOVE:
                            rv.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                    }
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            };

            slotHoursHolder.addOnItemTouchListener(mScrollTouchListener);
        }
    }
}
