package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.SingleDaySlotItem;

import java.util.ArrayList;
import java.util.List;

public class AddSlotHoursAdapter extends RecyclerView.Adapter<AddSlotHoursAdapter.ViewHolder> {

    private Context context;
    private List<SingleDaySlotItem.TimeSlots> data = new ArrayList<>();


    public AddSlotHoursAdapter(Context context, List<SingleDaySlotItem.TimeSlots> list) {
        this.context = context;
        this.data = list;
    }

    public interface AddHoursCallBack{

        void onDeleted(int position);
        void onItemCLicked(int position, View view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.layout_hours, parent, false);
        return new AddSlotHoursAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AddSlotHoursAdapter.ViewHolder holder, final int position) {
        if(data.get(position)!= null){
            SingleDaySlotItem.TimeSlots singleItem = data.get(position);
            holder.textStartTime.setText(singleItem.getStartTime());
            holder.textEndTime.setText(singleItem.getEndTime());

            holder.textStartTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    displayTimePickerDialog(v);
                }
            });

            holder.textEndTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayTimePickerDialog(v);
                }
            });
        }
    }

    private void displayTimePickerDialog(View v) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItems(List<SingleDaySlotItem.TimeSlots> items){
        data.clear();
        notifyDataSetChanged();
        addItems(items);
    }

    public void addItems(List<SingleDaySlotItem.TimeSlots> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                data.add(items.get(i));
                notifyItemInserted(data.size() - 1);
                notifyItemRangeChanged(0, data.size());
            }
        }
    }

    public void setItem(SingleDaySlotItem.TimeSlots dataEntity, int position){

        data.set(position, dataEntity);
        notifyItemChanged(position);

    }
    public void deleteItem(int position){
        if ( position >= 0 && position < data.size() ) {
            data.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, data.size());
        }
    }

    public void clearItems() {
        data.clear();
        notifyDataSetChanged();
    }

    public SingleDaySlotItem.TimeSlots getItem(int position){
        if ( position < data.size() ) {
            return data.get(position);
        }
        return null;
    }

    public List<SingleDaySlotItem.TimeSlots> getItems(){
        if ( data.size()>0 ) {
            return data;
        }
        return null;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textStartTime, textEndTime;

        public ViewHolder(View itemView) {
            super(itemView);

            textStartTime = (TextView) itemView.findViewById(R.id.tv_start_time);
            textEndTime = (TextView) itemView.findViewById(R.id.tv_end_time);

        }
    }
}
