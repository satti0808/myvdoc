package com.srinivasu.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.barteksc.pdfviewer.PDFView;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.interfaces.ActionCallback;
import com.srinivasu.doctor.fragments.PatientHomeFragment;
import com.srinivasu.doctor.model.Attachments;
import com.srinivasu.doctor.model.PrescriptionModel;
import com.srinivasu.doctor.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ViewPatAttachmentsAdapter extends RecyclerView.Adapter<ViewPatAttachmentsAdapter.ViewHolder> {

    private Context context;
    private List<Attachments> data;
    ActionCallback callback;

    public ViewPatAttachmentsAdapter(Context context, List<Attachments> list, ActionCallback callback) {
        this.context = context;
        this.data = list;
        this.callback = callback;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewPatAttachmentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_attachment_images, parent, false);
        return new ViewPatAttachmentsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewPatAttachmentsAdapter.ViewHolder holder, final int position) {

        if(data!= null){

            Attachments item = data.get(position);
           if(item.getAttachmentType().equals("image")){
               if(!TextUtils.isEmpty(item.getURL())){
                   Glide.with(context).load(Constants.ROOT_URL+""+item.getURL()).into(holder.imageView);
               }
               holder.pdfView.setVisibility(View.GONE);
               holder.layoutOverlay.setVisibility(View.GONE);
           }else if(item.getAttachmentType().equals("pdf")){
               holder.pdfView.setVisibility(View.VISIBLE);
               holder.layoutOverlay.setVisibility(View.VISIBLE);
               new AsyncTask<Void, Void, Void>() {
                   @SuppressLint("WrongThread")
                   @Override
                   protected Void doInBackground(Void... voids) {
                       InputStream input;
                       try {
                           input = new URL(Constants.ROOT_URL+""+item.getURL()).openStream();
                           holder.pdfView.fromStream(input)
                                   .enableSwipe(true) // allows to block changing pages using swipe
                                   .swipeHorizontal(false)
                                   .enableDoubletap(true)
                                   .defaultPage(0)
                                   .load();
                       } catch (IOException e) {
                           e.printStackTrace();
                       }

                       return null;
                   }
               }.execute();
              /* holder.pdfView.fromUri(Uri.parse(Constants.ROOT_URL+""+item.getURL()))
                       .enableSwipe(true) // allows to block changing pages using swipe
                       .swipeHorizontal(false)
                       .enableDoubletap(false)
                       .defaultPage(0)
                       .load();*/
               holder.imageView.setVisibility(View.GONE);
           }

           holder.imageView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   callback.onItemClick(position);
               }
           });

            holder.layoutOverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItems(List<Attachments> items){
        data = new ArrayList<>();
        notifyDataSetChanged();
        addItems(items);
    }

    public void addItems(List<Attachments> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                data.add(items.get(i));
                notifyItemInserted(data.size() - 1);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        PDFView pdfView;
        FrameLayout layoutOverlay;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.iv_image);
            pdfView = (PDFView) itemView.findViewById(R.id.pdf_view);
            layoutOverlay = (FrameLayout) itemView.findViewById(R.id.layout_pdf_overlay);

        }
    }
}
