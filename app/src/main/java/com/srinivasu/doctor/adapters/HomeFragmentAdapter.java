package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.HomeAppointmentModel;
import com.srinivasu.doctor.utils.Utils;

import java.util.List;

public class HomeFragmentAdapter extends RecyclerView.Adapter<HomeFragmentAdapter.MyviewHolder> {

    Context context;
    List<HomeAppointmentModel> a1;
    HomeCallBackInterface callBackInterface;
    String imgs[]={"https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg",
            "https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg"};

    public HomeFragmentAdapter(Context context, List<HomeAppointmentModel> home, HomeCallBackInterface callBackInterface) {
        this.context = context;
        this.a1 = home;
        this.callBackInterface = callBackInterface;
    }

    public interface HomeCallBackInterface{
        void onStartCall(int position);
    }
    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_home_screen, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {
        if(pos==0){
            holder.rl1.setVisibility(View.VISIBLE);
            holder.rl2.setVisibility(View.GONE);
            if(!TextUtils.isEmpty(a1.get(pos).getImg_url())){
                Glide.with(context).load(a1.get(pos).getImg_url()).into(holder.image_view1);
            }
            holder.tv_name1.setText(a1.get(pos).getPatient_name());
            holder.tv_date1.setText(Utils.displayDateFormat(a1.get(pos).getAppointment_date()));

            holder.tv_time1.setText(a1.get(pos).getAppointment_time()+" ("+a1.get(pos).getDuration()+")");
            if(a1.get(pos).getAppointment_type().contentEquals("Video Call")){
             holder.tv_call.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_video_call_white, 0, 0, 0);
            }
            holder.tv_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBackInterface.onStartCall(pos);
                    /*Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + a1.get(pos).getPhone()));
                    context.startActivity(callIntent);*/
                }
            });
        }else{
            holder.rl1.setVisibility(View.GONE);
            holder.rl2.setVisibility(View.VISIBLE);
            holder.tv_name.setText(a1.get(pos).getPatient_name());
            holder.tv_time.setText(a1.get(pos).getAppointment_time()+" ("+a1.get(pos).getDuration()+")");
            holder.tv_date.setText(a1.get(pos).getAppointment_date());
            Glide.with(context).load("https://www.avtarwomen.com/blog/wp-content/uploads/2018/03/2.jpg").into(holder.image_view);
            //Glide.with(context).load(imgs[pos]).into(holder.image_view);
            if(a1.get(pos).getAppointment_type().contentEquals("Video Call")){

                holder.ivPhone.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_video_cal));
            }
            holder.ivPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBackInterface.onStartCall(pos);
                   /* Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + a1.get(pos).getPhone()));
                    context.startActivity(callIntent);*/
                }
            });
        }

        //Toast.makeText(context,a1.get(pos).getImg_url(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public  class MyviewHolder extends RecyclerView.ViewHolder {
        TextView  tv_name,tv_name1,tv_time,tv_time1,tv_call,tv_date,tv_date1;
        ImageView image_view,image_view1,ivPhone;
        RelativeLayout rl1,rl2;
        public MyviewHolder(View itemView) {
            super(itemView);
            tv_name  = (TextView) itemView.findViewById(R.id.tv_name);
            tv_name1  = (TextView) itemView.findViewById(R.id.tv_name1);
            tv_time  = (TextView) itemView.findViewById(R.id.tv_time );
            tv_time1  = (TextView) itemView.findViewById(R.id.tv_time1 );
            tv_call  = (TextView) itemView.findViewById(R.id.tv_call );
            tv_date  = (TextView) itemView.findViewById(R.id.tv_date );
            tv_date1  = (TextView) itemView.findViewById(R.id.tv_date1 );
            image_view=(ImageView)itemView.findViewById(R.id.image_view);
            image_view1=(ImageView)itemView.findViewById(R.id.image_view1);
            ivPhone=(ImageView)itemView.findViewById(R.id.ivPhone);
            rl1=(RelativeLayout)itemView.findViewById(R.id.rl1);
            rl2=(RelativeLayout)itemView.findViewById(R.id.rl2);
        }
    }
}