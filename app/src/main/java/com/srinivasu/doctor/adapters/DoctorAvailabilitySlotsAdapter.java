package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.razorpay.PaymentResultListener;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.interfaces.ActionCallback;
import com.srinivasu.doctor.model.DoctorAvailableSlot;

import java.util.List;

public class DoctorAvailabilitySlotsAdapter extends RecyclerView.Adapter<DoctorAvailabilitySlotsAdapter.ViewHolder>  implements PaymentResultListener {

    List<DoctorAvailableSlot> availableSlotList;
    Context context;
    private ActionCallback callback;

    public DoctorAvailabilitySlotsAdapter(List<DoctorAvailableSlot> availableSlotList, Context context, ActionCallback callback) {
        this.availableSlotList = availableSlotList;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public DoctorAvailabilitySlotsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_row_availability, parent, false);
        return new DoctorAvailabilitySlotsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(availableSlotList.get(position) != null){

            if(!TextUtils.isEmpty(availableSlotList.get(position).getSlotTime())){
                holder.textSlotTime.setText(availableSlotList.get(position).getSlotTime());
            }

            if(availableSlotList.get(position).getIsAvailable() != 1){
                holder.textSlotTime.setBackground(context.getResources().getDrawable(R.drawable.bg_appointment_selected1));
            }

            holder.textSlotTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (availableSlotList.get(position).getIsAvailable() != 1) {
                        Toast.makeText(context, "Please select another slot, Doctor is not available at " + availableSlotList.get(position).getSlotTime(), Toast.LENGTH_SHORT).show();
                    } else {
                        callback.onItemClick(position);
                    }
                }

            });

        }
    }

    @Override
    public int getItemCount() {
        return availableSlotList.size();
    }

    @Override
    public void onPaymentSuccess(String s) {

    }

    @Override
    public void onPaymentError(int i, String s) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textSlotTime;

        public ViewHolder(View itemView) {
            super(itemView);

            textSlotTime = (TextView) itemView.findViewById(R.id.tv);
        }
    }
}
