package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.ImageFunctionality;
import com.srinivasu.doctor.activities.PatAppointmentDetailsActivity;
import com.srinivasu.doctor.activities.PatientMainActivity;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.PatientHomeAppointmentModel;
import com.srinivasu.doctor.model.ResultData;
import com.srinivasu.doctor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PatientAppointmentAdapter extends RecyclerView.Adapter<PatientAppointmentAdapter.MyviewHolder> {

    Context context;
    List<AppointmentDetails> a1;
    SharedPreferences pref;
    public PatientAppointmentAdapter(Context context, List<AppointmentDetails> patient) {
        this.context = context;
        this.a1 = patient;
        pref = context.getSharedPreferences("mydoc", 0);
    }


    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_patient_appointment, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {

        holder.tv_date.setText(Utils.displayDateFormat(a1.get(pos).getAppointmentDate()));
        holder.tv_time.setText(a1.get(pos).getAppointmentTime());
        holder.tv_add_attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PatAppointmentDetailsActivity.class);
                intent.putExtra("screen", "history");
                intent.putExtra("appointment_details", Parcels.wrap(a1.get(pos)));
                context.startActivity(intent);
            }
        });
        holder.tv_add_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFeedback(a1.get(pos).getAppointmentID());
            }
        });
    }
    private void addFeedback(final  String aid) {
        final View view = ((PatientMainActivity)context).getLayoutInflater().inflate(R.layout.dialogbox_feedback, null);
        //final AlertDialog alertDialog = new AlertDialog.Builder(context,R.style.ProgressDialogTheme).create();
        //final AlertDialog alertDialog = new AlertDialog.Builder(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen).create();
        final AlertDialog alertDialog = new AlertDialog.Builder(context,R.style.AppTheme).create();
        alertDialog.setTitle("");
        alertDialog.setCancelable(false);
        final EditText etFeedback = (EditText) view.findViewById(R.id.etFeedback);
        final RatingBar rb=(RatingBar) view.findViewById(R.id.rb);
        final ImageView iv_back=(ImageView) view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        TextView btn_save = (TextView) view.findViewById(R.id.btn_save);
        TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                submitFeedbackDetails(aid,rb.getRating()+"",etFeedback.getText().toString());
               // submitBPDetails();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }
    private void submitFeedbackDetails(String aid,String rating,String feedback){
        /*pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();*/
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("AppointmentID", aid);
            paramObject.put("rating",   rating);
            paramObject.put("Feedback",   feedback);
            Call<ResultData> call = api.addFeedback(pref.getString("token","-"),paramObject.toString());
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    //pd.dismiss();
                    Toast.makeText(context,"Feedback is submitted successfully.",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    // pd.dismiss();
                    //pb.setVisibility(View.GONE);
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_date,tv_time,tv_add_fb,tv_add_attachment;


        public MyviewHolder(View itemView) {
            super(itemView);

            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_add_fb = (TextView) itemView.findViewById(R.id.tv_add_fb);
            tv_add_attachment= (TextView) itemView.findViewById(R.id.tv_add_attachment);
        }
    }
}