package com.srinivasu.doctor.adapters;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.AppointmentApprovePostponeActivity;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.AppointmentDetailsModel;
import com.srinivasu.doctor.model.ResultData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApprovePostponeAdapter extends RecyclerView.Adapter<ApprovePostponeAdapter.MyviewHolder> {

    Context context;
    List<AppointmentDetailsModel> appointments;
    String sdate="";
    private int mStartYear, mStartMonth, mStartDay, mStartHour, mStartMinute;
    private int mEndYear, mEndMonth, mEndDay, mEndHour, mEndMinute;
    public ApprovePostponeAdapter(Context context, List<AppointmentDetailsModel> timeavailability,String sdate) {
        this.context = context;
        this.appointments = timeavailability;
        this.sdate=sdate;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_approve_postpone, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull MyviewHolder holder, final int pos) {
        holder.tv_time.setText(appointments.get(pos).getAppointment_time()+" ("+appointments.get(pos).getDuration()+")");
       // Toast.makeText(context,appointments.get(pos).getStatus(),Toast.LENGTH_SHORT).show();
        /*if(!appointments.get(pos).getStatus().equals("pending")){
            holder.tv_approve.setVisibility(View.GONE);
            holder.tv_postpone.setVisibility(View.GONE);
            holder.tv_status.setVisibility(View.VISIBLE);

        }else{
            holder.tv_approve.setVisibility(View.VISIBLE);
            holder.tv_postpone.setVisibility(View.VISIBLE);
            holder.tv_status.setVisibility(View.GONE);
        }*/
        holder.tv_approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approve(appointments.get(pos).getAppointment_ID());
            }
        });
        holder.tv_postpone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // holder.ll.setVisibility(View.VISIBLE);
               // holder.tv_submit.setVisibility(View.VISIBLE);
                postpone_date="";postpone_time="";postpone_duration="";postpone_aid="";
                 postpone_duration = appointments.get(pos).getDuration();
                 postpone_aid=appointments.get(pos).getAppointment_ID();
                 getStartDate();
            }
        });
        holder.tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // postpone(appointments.get(pos).getAppointment_ID(),holder.tv_postpone_start_date.getText().toString()+" "+holder.tv_postpone_start_time.getText().toString(),holder.tv_postpone_end_date.getText().toString()+" "+holder.tv_postpone_end_time.getText().toString());
            }
        });
        holder.tv_postpone_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // getStartDate(holder.tv_postpone_start_date);
            }
        });
        holder.tv_postpone_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  getStartTime(holder.tv_postpone_start_time);
            }
        });

        holder.tv_postpone_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEndDate(holder.tv_postpone_end_date);
            }
        });
        holder.tv_postpone_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEndTime(holder.tv_postpone_end_time);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (appointments != null) {
            return appointments.size();
        }
        return 0;
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_time,tv_status,tv_approve,tv_postpone,tv_postpone_start_date,tv_postpone_start_time,tv_submit,tv_postpone_end_date,tv_postpone_end_time;
        CardView tv_layout;
        LinearLayout ll;
        public MyviewHolder(View itemView) {
            super(itemView);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_approve = (TextView) itemView.findViewById(R.id.tv_approve);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
            tv_postpone = (TextView) itemView.findViewById(R.id.tv_postpone);

            tv_postpone_start_date = (TextView) itemView.findViewById(R.id.tv_postpone_start_date);
            tv_postpone_end_date = (TextView) itemView.findViewById(R.id.tv_postpone_end_date);
            tv_postpone_start_time = (TextView) itemView.findViewById(R.id.tv_postpone_start_time);
            tv_postpone_end_time = (TextView) itemView.findViewById(R.id.tv_postpone_end_time);
            tv_submit = (TextView) itemView.findViewById(R.id.tv_submit);

            tv_layout = (CardView) itemView.findViewById(R.id.cv);
            ll=(LinearLayout)itemView.findViewById(R.id.ll);
        }
    }
    String startDateTime="",endDateTime="";
    String postpone_date,postpone_time,postpone_duration,postpone_aid;
    private void getStartDate(){
        final Calendar c = Calendar.getInstance();
        mStartYear = c.get(Calendar.YEAR);
        mStartMonth = c.get(Calendar.MONTH);
        mStartDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        postpone_date = year + "-"+(monthOfYear+1)+"-"+dayOfMonth;
                        getStartTime();
                    }
                }, mStartYear, mStartMonth, mStartDay);
        datePickerDialog.show();
    }
    private void getStartTime(){
        final Calendar c = Calendar.getInstance();
        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        //tvStartTime.setText(hourOfDay + ":" + minute);
                        //Toast.makeText(context,postpone_duration+" : "+postpone_date,Toast.LENGTH_SHORT).show();
                        postpone_time =hourOfDay + ":" + minute;
                        postpone();
                    }
                }, mStartHour, mStartMinute, false);
        timePickerDialog.show();
    }

    private void getEndDate(final TextView tvStartDate){
        final Calendar c = Calendar.getInstance();
        mStartYear = c.get(Calendar.YEAR);
        mStartMonth = c.get(Calendar.MONTH);
        mStartDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tvStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, mStartYear, mStartMonth, mStartDay);
        datePickerDialog.show();
    }
    private void getEndTime(final TextView tvStartTime){
        final Calendar c = Calendar.getInstance();
        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        tvStartTime.setText(hourOfDay + ":" + minute);
                    }
                }, mStartHour, mStartMinute, false);
        timePickerDialog.show();
    }

    ProgressDialog pd;
    SharedPreferences pref;

    private void approve(String aid) {
        pref = context.getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(context);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("status", "declined");
            //paramObject.put("password", "1234");
            Call<ResultData> call = api.approve(pref.getString("token", "-"),  paramObject.toString(),aid);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        ResultData list_appointments = response.body();
                        if(list_appointments.getSuccess().equals("true")){
                            Toast.makeText(context, "Appointment is successfully approved", Toast.LENGTH_SHORT).show();
                            ((AppointmentApprovePostponeActivity)context).finish();
                        }
                    }
                }
                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postpone() {
        pref = context.getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(context);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("status", "accepted");
            paramObject.put("date", postpone_date);
            paramObject.put("time", postpone_time);
            paramObject.put("duration", "60");
            paramObject.put("doctorID", pref.getString("doctorId", "-"));
            //paramObject.put("password", "1234");
            Call<ResultData> call = api.postpone(pref.getString("token", "-"),  paramObject.toString(),postpone_aid);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        ResultData list_appointments = response.body();
                        if(list_appointments.getSuccess().equals("true")){
                            Toast.makeText(context, "Appointment is successfully postponed.", Toast.LENGTH_SHORT).show();
                            ((AppointmentApprovePostponeActivity)context).finish();
                        }else{
                            Toast.makeText(context, "Another Appointment is booked for this date and time.Try new date and time.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, response.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}