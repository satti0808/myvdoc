package com.srinivasu.doctor.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.MedicineListActivity;
import com.srinivasu.doctor.model.PrescriptionModel;

import java.util.List;


public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.MyviewHolder> {

    Context context;
    List<PrescriptionModel> a1;

    public MedicineAdapter(Context context, List<PrescriptionModel> messages) {
        this.context = context;
        this.a1 = messages;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_medicine, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int pos) {
        holder.tv_drug.setText("Appointment ID : "+a1.get(pos).getAppointmentID());
        holder.tv_drug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, MedicineListActivity.class);
                /*intent.putExtra("name",a1.get(pos).getMedicineMaster().getName());
                intent.putExtra("drugName",a1.get(pos).getMedicineMaster().getDrugName());
                intent.putExtra("route",a1.get(pos).getMedicineMaster().getRoute());
                intent.putExtra("frequency",a1.get(pos).getMedicineMaster().getFrequency());
                intent.putExtra("strength",a1.get(pos).getMedicineMaster().getStrength());
                intent.putExtra("disp",a1.get(pos).getMedicineMaster().getDisp());*/
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (a1 != null) {
            return a1.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tv_drug;


        public MyviewHolder(View itemView) {
            super(itemView);
            tv_drug = (TextView) itemView.findViewById(R.id.tv_drug);
        }
    }
}