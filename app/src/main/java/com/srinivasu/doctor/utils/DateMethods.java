package com.srinivasu.doctor.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DateMethods {

    public static final String HH_MM_A = "hh:mm a";
    public static  final String  dd_MM_yyyy_hh_mm_a="dd-MM-yyyy, hh:mm a";
    public static final String NORMAL_DATE_FORMAT = "dd-MM-yyyy";
    public static final String UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DD_MM_YYYY = "dd/MM/yyyy";

    public static String getCurrentTimeInSlotFormat(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dd_MM_yyyy_hh_mm_a, Locale.ENGLISH);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getDayFromDate(String date){

        try {
            SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date dt1= format1.parse(date);
            DateFormat format2=new SimpleDateFormat("EE", Locale.ENGLISH);
            String finalDay=format2.format(dt1);
            return finalDay;
        } catch (ParseException e) {
            e.printStackTrace();
        }
       return "";
    }

    public static String getDateInFormat(String dateString, String requiredDateFormat) {
        if (dateString == null) {dateString="";}
        String dateInRequiredFormat = dateString;
        try {
            SimpleDateFormat inputTimeFormat = new SimpleDateFormat(UTC_FORMAT, Locale.ENGLISH);
            SimpleDateFormat onlyDateFormat = new SimpleDateFormat(requiredDateFormat, Locale.ENGLISH);
            dateInRequiredFormat = onlyDateFormat.format(inputTimeFormat.parse(dateString));
        }catch (Exception e) {
            e.printStackTrace();
        }
        return dateInRequiredFormat;
    }

    public static String getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            Long time =  TimeUnit.MINUTES.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
            return String.valueOf(time);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
