package com.srinivasu.doctor.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static String displayDateFormat(String mdate){
        String inputPattern = "yyyy-MM-dd";
        try {
            SimpleDateFormat dt = new SimpleDateFormat(inputPattern);
            Date date = dt.parse(mdate);
            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MMM/yyyy");
            return dt1.format(date);
        }catch (Exception e){
            return null;
        }
    }
}
