package com.srinivasu.doctor.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.DoctorAvailabilitySlotsAdapter;
import com.srinivasu.doctor.adapters.interfaces.ActionCallback;
import com.srinivasu.doctor.model.DoctorAvailableSlot;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.utils.DateMethods;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorAvailabilityActivity extends BaseActivity {
    SharedPreferences pref;
    RecyclerView recyclerView;
    String selectedDate = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability);

        getSupportActionBar().setTitle("Doctor Availabilities");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        pref = getSharedPreferences("mydoc", 0);

        try {
            selectedDate = getIntent().getStringExtra("sel_date");
            paramObject.put("Date", selectedDate);
            paramObject.put("Day", DateMethods.getDayFromDate(selectedDate));
            getDoctorAvailability(paramObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getDoctorAvailability(String date) throws JSONException {
        getLoadingDialog().show("Loading....");
        retrofitInterface.getDoctorAvailability(paramObject.toString(), getLocalStorageData().getPatientsDotcotrId(), "1").
                enqueue(new Callback<ResponseObject<List<DoctorAvailableSlot>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<DoctorAvailableSlot>>> call, Response<ResponseObject<List<DoctorAvailableSlot>>> response) {
                if(response.isSuccessful() && response.body()!= null){
                    if(response.body().isSuccess()){
                        closeLoadingDialog();
                        List<DoctorAvailableSlot> slotList = new ArrayList<>();
                        slotList.addAll(response.body().getData());

                        recyclerView.setAdapter(new DoctorAvailabilitySlotsAdapter(slotList, DoctorAvailabilityActivity.this, new ActionCallback() {
                            @Override
                            public void onItemClick(int position) {
                                Intent intent = new Intent(DoctorAvailabilityActivity.this, AppointmentTypeActivity.class);
                                intent.putExtra("sel_date", selectedDate);
                                intent.putExtra("sel_time_slot", slotList.get(position).getSlotID());
                                intent.putExtra("sel_time", slotList.get(position).getSlotTime());
                                startActivity(intent);
                            }
                        }));
                    }
                }else{
                    closeLoadingDialog();
                    showToast("Doctor slots available. Please try after some time");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<DoctorAvailableSlot>>> call, Throwable t) {

                closeLoadingDialog();
                call.cancel();
                showToast("Unable to get doctor availability slots. Please try after some time");
            }
        });
    }
}
