package com.srinivasu.doctor.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.github.barteksc.pdfviewer.PDFView;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.Attachments;
import com.srinivasu.doctor.utils.Constants;

import org.parceler.Parcels;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class PdfImageViewActivity extends BaseActivity {

    Attachments attachments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_image_view);

        setupActionBar();
        setToolbarTitle(" ");

        Bundle bundle = getIntent().getExtras();
        if(bundle!= null){
            attachments = Parcels.unwrap(bundle.getParcelable("attachment_details"));
        }
        ImageView imageView = (ImageView) findViewById(R.id.iv_image);
        PDFView pdfView = (PDFView) findViewById(R.id.pdf_view);

        if(attachments.getAttachmentType().equals("image")){
            if(!TextUtils.isEmpty(attachments.getURL())){
                Glide.with(context).load(Constants.ROOT_URL+""+attachments.getURL()).into(imageView);
            }
            pdfView.setVisibility(View.GONE);
        }else if(attachments.getAttachmentType().equals("pdf")){

            new AsyncTask<Void, Void, Void>() {
                @SuppressLint("WrongThread")
                @Override
                protected Void doInBackground(Void... voids) {
                    InputStream input;
                    try {
                        input = new URL(Constants.ROOT_URL+""+attachments.getURL()).openStream();
                        pdfView.fromStream(input)
                                .enableSwipe(true) // allows to block changing pages using swipe
                                .swipeHorizontal(false)
                                .enableDoubletap(true)
                                .defaultPage(0)
                                .load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return null;
                }
            }.execute();

//            pdfView.fromUri(Uri.fromFile(new File("file://"+Constants.ROOT_URL+""+attachments.getURL())))

            imageView.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_home) {
            Intent intent = new Intent(this, PatientMainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}