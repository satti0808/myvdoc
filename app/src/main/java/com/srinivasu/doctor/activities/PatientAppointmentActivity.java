package com.srinivasu.doctor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CalendarView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;

public class PatientAppointmentActivity  extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_appointments);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Add Appointment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CalendarView calender=(CalendarView)findViewById(R.id.calender);
        calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                // Toast.makeText(getActivity(), ""+dayOfMonth,Toast.LENGTH_LONG).show();// TODO Auto-generated method stub
                Intent intentg= new Intent(PatientAppointmentActivity.this, DoctorAvailabilityActivity.class);
                intentg.putExtra("sel_date",year+"-"+(month+1)+"-"+dayOfMonth);
                startActivity(intentg);
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
