package com.srinivasu.doctor.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.quickblox.chat.QBChatService;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.fragments.AppointmentsFragment;
import com.srinivasu.doctor.fragments.HomeFragment;
import com.srinivasu.doctor.fragments.MoreOptionsFragment;
import com.srinivasu.doctor.fragments.PatientsFragment;
import com.srinivasu.doctor.fragments.PaymentsFragment;
import com.srinivasu.doctor.services.LoginService;

import static com.srinivasu.doctor.utils.Consts.COMMAND_LOGIN;
import static com.srinivasu.doctor.utils.Consts.EXTRA_COMMAND_TO_SERVICE;
import static com.srinivasu.doctor.utils.Consts.EXTRA_QB_USER;


public class DoctorMainActivity extends BaseActivity implements View.OnClickListener {
    BottomNavigationView bottomNavigationView;
    Fragment homeFragment = null;
    Fragment accountFragment = null;

    ImageView imgAddPatient, imgAddMedicine, imgAddSlots, imgChat, imgLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_main);

        imgAddPatient=(ImageView)findViewById(R.id.iv_add_patient);
        imgAddMedicine =(ImageView)findViewById(R.id.iv_add_medicine);
        imgAddSlots=(ImageView)findViewById(R.id.iv_add_slots);
        imgChat=(ImageView)findViewById(R.id.iv_chat);
        imgLogout=(ImageView)findViewById(R.id.iv_logout);

        imgAddPatient.setOnClickListener(this);
        imgAddMedicine.setOnClickListener(this);
        imgAddSlots.setOnClickListener(this);
        imgChat.setOnClickListener(this);
        imgLogout.setOnClickListener(this);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        View activeLabel = bottomNavigationView.findViewById(R.id.action_appointments);
        if (activeLabel != null && activeLabel instanceof TextView) {
            ((TextView)activeLabel).setPadding(0,0,0,0);
        }

        bottomNavigationView.setSelectedItemId(R.id.action_home);
        replace_fragment(new HomeFragment());
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                replace_fragment(new HomeFragment());
                                break;
                            case R.id.action_appointments:
                                replace_fragment(new AppointmentsFragment());
                                break;
                            case R.id.action_payments:
                                replace_fragment(new PaymentsFragment());
                                break;
                            case R.id.action_patients:
                                replace_fragment(new PatientsFragment());
                                break;
                            case R.id.action_more:
                                replace_fragment(new MoreOptionsFragment());
                                break;
                        }

                        return true;
                    }
                });
        //bottomNavigationView.setSelectedItemId(R.id.action_home);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v == imgAddPatient){
            intent= new Intent(getApplicationContext(), AddPatientActivity.class);
        }else if(v == imgAddMedicine){
            intent= new Intent(getApplicationContext(), AddMedicineActivity.class);
        }else if(v == imgAddSlots){
//            showToast("In progress");
            intent= new Intent(getApplicationContext(), AddDoctorSlotsActivity.class);
        }else if(v == imgChat){
            showToast("In progress");
//            intent= new Intent(getApplicationContext(), ChatMainActivity.class);
        }else if(v == imgLogout ){
            displayConfirmationDialog();
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    private void replace_fragment(Fragment fragment) {

        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();

        Fragment curFrag = getSupportFragmentManager().getPrimaryNavigationFragment();
        Fragment cacheFrag = getSupportFragmentManager().findFragmentByTag(tag);

        if (curFrag != null)
            tr.hide(curFrag);

        if (cacheFrag == null) {
            tr.add(R.id.frame_layout, fragment, tag);
        } else {
            tr.show(cacheFrag);
            fragment = cacheFrag;
        }

        tr.setPrimaryNavigationFragment(fragment);
        tr.commit();

    }

    @Override
    public void onResume() {
        super.onResume();
        QBChatService chatService = QBChatService.getInstance();
        if(!chatService.isLoggedIn()){
            if(getLocalStorageData().getQBUser()!= null){
                createChatConnection();
            }
        }
    }

    private void createChatConnection() {
        Intent intent = new Intent(this, LoginService.class);
        intent.putExtra(EXTRA_COMMAND_TO_SERVICE, COMMAND_LOGIN);
        intent.putExtra(EXTRA_QB_USER, getLocalStorageData().getQBUser());
        startService(intent);
    }

    private void displayConfirmationDialog() {

        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure, you want to logout?");
        // add the buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("mydoc", 0);
                SharedPreferences.Editor et = pref.edit();
                et.putString("is_login", "no");
                et.commit();
                getLocalStorageData().clearQBUser();
                getLocalStorageData().clearUserDetails();
                Intent intentg= new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intentg);
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        dialog = builder.create();
        dialog.show();
    }


}
