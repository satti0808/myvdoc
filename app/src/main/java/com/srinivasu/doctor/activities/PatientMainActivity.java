package com.srinivasu.doctor.activities;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.quickblox.chat.QBChatService;
import com.quickblox.users.model.QBUser;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.fragments.AppointmentsFragment;
import com.srinivasu.doctor.fragments.PatientHomeFragment;
import com.srinivasu.doctor.fragments.PatientMedicineFragment;
import com.srinivasu.doctor.fragments.PatientPaymentsFragment;
import com.srinivasu.doctor.fragments.PatientReportsFragment;
import com.srinivasu.doctor.services.CallService;
import com.srinivasu.doctor.services.LoginService;
import com.srinivasu.doctor.utils.Consts;
import com.srinivasu.doctor.utils.SharedPrefsHelper;

import static com.srinivasu.doctor.utils.Consts.COMMAND_LOGIN;
import static com.srinivasu.doctor.utils.Consts.EXTRA_COMMAND_TO_SERVICE;
import static com.srinivasu.doctor.utils.Consts.EXTRA_QB_USER;

public class PatientMainActivity extends BaseActivity implements PatientHomeFragment.ActionCallback, BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;
    Fragment homeFragment = null;
    Fragment accountFragment = null;
    Toolbar toolbar;
    ImageView logout, profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_main);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        logout = (ImageView) findViewById(R.id.logout);
        profile = (ImageView) findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(PatientMainActivity.this, ProfileActivity.class);
                startActivity(pr);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayConfirmationDialog();
            }
        });
        View activeLabel = bottomNavigationView.findViewById(R.id.action_appointments);
        if (activeLabel != null && activeLabel instanceof TextView) {
            ((TextView) activeLabel).setPadding(0, 0, 0, 0);
        }

        loadFragment(new PatientHomeFragment());
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        createChatConnection();
    }

    private void displayConfirmationDialog() {

        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure, you want to logout?");
        // add the buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("mydoc", 0);
                SharedPreferences.Editor et = pref.edit();
                et.putString("is_login", "no");
                et.commit();
                getLocalStorageData().clearQBUser();
                getLocalStorageData().clearUserDetails();
                Intent intentg = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intentg);
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        dialog = builder.create();
        dialog.show();
    }

    private void createChatConnection() {
        if (getLocalStorageData().getQBUser() != null) {
            Intent intent = new Intent(this, LoginService.class);
            intent.putExtra(EXTRA_COMMAND_TO_SERVICE, COMMAND_LOGIN);
            intent.putExtra(EXTRA_QB_USER, getLocalStorageData().getQBUser());
            startService(intent);
        }
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.action_home:
                selectedFragment = new PatientHomeFragment();
                break;
            case R.id.action_appointments:
                selectedFragment = new AppointmentsFragment();
                break;
            case R.id.action_fee:
                selectedFragment = new PatientPaymentsFragment();
                break;
            case R.id.action_reports:
                selectedFragment = new PatientReportsFragment();
                break;
            case R.id.action_history:
                selectedFragment = new PatientMedicineFragment();
                break;

        }
        return loadFragment(selectedFragment);
    }

    @Override
    public void onAddAppointmentClicked() {
        loadFragment(new AppointmentsFragment());
        bottomNavigationView.setSelectedItemId(R.id.action_appointments);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isIncomingCall = SharedPrefsHelper.getInstance().get(Consts.EXTRA_IS_INCOMING_CALL, false);
        if (isCallServiceRunning(CallService.class)) {
            Log.d(this.getClass().getSimpleName(), "CallService is running now");
            CallActivity.start(this, isIncomingCall);
        }
        clearAppNotifications();

        QBChatService chatService = QBChatService.getInstance();
        if (!chatService.isLoggedIn()) {
            createChatConnection();
        }
    }


    private boolean isCallServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void clearAppNotifications() {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }

    private boolean checkIsLoggedInChat() {
        if (!QBChatService.getInstance().isLoggedIn()) {
            Toast.makeText(this, "Logging into chat", Toast.LENGTH_SHORT).show();
            startLoginService();
            return false;
        }
        return true;
    }

    private void startLoginService() {
        if (getLocalStorageData().getQBUser() != null) {
            QBUser qbUser = getLocalStorageData().getQBUser();
            Intent intent = new Intent(this, LoginService.class);
            intent.putExtra(EXTRA_COMMAND_TO_SERVICE, COMMAND_LOGIN);
            intent.putExtra(EXTRA_QB_USER, qbUser);
            startService(intent);
//            getActivity().LoginService.start(getActivity(), qbUser);
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof PatientHomeFragment) {
            PatientHomeFragment patientHomeFragment = (PatientHomeFragment) fragment;
            patientHomeFragment.setActionCallBackListener(this);
        }
    }
}
