package com.srinivasu.doctor.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.model.AppointmentDetails;

import org.parceler.Parcels;

public class DocAppointmentDetailsActivity extends BaseActivity implements View.OnClickListener {

    TextView textPatientName, textAge, textGender, textAmount, textTime, textDate,
            textAddPrescription, textAddNote, textAddLabTests, textUploadImage,
            textCompleteAppointment;

    AppointmentDetails appointmentDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);

        setupActionBar();
        setToolbarTitle("Appointment Details");
        initialiseViews();

        Bundle bundle = getIntent().getExtras();
        if(bundle!= null){
            appointmentDetails = Parcels.unwrap(bundle.getParcelable("appointment_details"));
            setAppointmentData();
        }

    }

    private void initialiseViews() {
        textPatientName = (TextView) findViewById(R.id.tv_patient_name);
        textAge = (TextView) findViewById(R.id.tv_patient_age);
        textGender = (TextView) findViewById(R.id.tv_patient_gender);

        textAmount = (TextView) findViewById(R.id.tv_amount);
        textTime = (TextView) findViewById(R.id.tv_AppointmentTime);
        textDate = (TextView) findViewById(R.id.tv_appointment_date);

        textAddPrescription = (TextView) findViewById(R.id.tv_add_prescription);
        textAddNote = (TextView) findViewById(R.id.tv_add_note);
        textAddLabTests = (TextView) findViewById(R.id.tv_add_lab_tests);
        textUploadImage = (TextView) findViewById(R.id.tv_upload_image);
        textCompleteAppointment = (TextView) findViewById(R.id.tv_complete_appointment);

        textAddPrescription.setOnClickListener(this);
        textAddNote.setOnClickListener(this);
        textAddLabTests.setOnClickListener(this);
        textUploadImage.setOnClickListener(this);
        textCompleteAppointment.setOnClickListener(this);
    }

    private void setAppointmentData() {

        textPatientName.setText(getLocalStorageData().getUserName());
        textAge.setText(getLocalStorageData().getAge());
        textGender.setText(getLocalStorageData().getGender());

       /* textAmount.setText("\u20B9"+appointmentDetails.getAppointmentAmount());
        textTime.setText(appointmentDetails.getAppointmentTime());
        textDate.setText(appointmentDetails.getAppointmentDate());*/

    }

    @Override
    public void onClick(View v) {

        if(v == textAddPrescription){

        }else if(v == textAddNote){

        }else if(v == textAddLabTests){

        }else if(v == textUploadImage){

        }else if(v == textCompleteAppointment){

        }
    }
}