package com.srinivasu.doctor.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;

public class MedicineDetailsActivity  extends AppCompatActivity {
    TextView tv_name,tv_drugName,tv_route,tv_frequency,tv_strength,tv_disp;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_details);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Medicine Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_name=(TextView)findViewById(R.id.tv_name);
        tv_name.setText(getIntent().getStringExtra("name"));
        tv_drugName=(TextView)findViewById(R.id.tv_drugName);
        tv_drugName.setText(getIntent().getStringExtra("drugName"));
        tv_route=(TextView)findViewById(R.id.tv_route);
        tv_route.setText(getIntent().getStringExtra("route"));
        tv_frequency=(TextView)findViewById(R.id.tv_frequency);
        tv_frequency.setText(getIntent().getStringExtra("frequency"));
        tv_strength=(TextView)findViewById(R.id.tv_strength);
        tv_strength.setText(getIntent().getStringExtra("strength"));
        tv_disp=(TextView)findViewById(R.id.tv_disp);
        tv_disp.setText(getIntent().getStringExtra("disp"));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
