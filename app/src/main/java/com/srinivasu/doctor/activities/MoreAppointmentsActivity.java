package com.srinivasu.doctor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.PatientAppointmentsAdapter;
import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.ResponseObject;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreAppointmentsActivity  extends BaseActivity {

    RecyclerView appointmentsHolder;
    TextView textNoData;

    PatientAppointmentsAdapter patientAppointmentsAdapter;
    List<AppointmentDetails> appointmentDetailsList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_appointments);

        setupActionBar();
        setToolbarTitle("Appointments");

        appointmentsHolder=(RecyclerView) findViewById(R.id.rv_appointment);
        textNoData=(TextView) findViewById(R.id.tv_no_data);
        appointmentsHolder.setHasFixedSize(true);
        appointmentsHolder.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        patientAppointmentsAdapter = new PatientAppointmentsAdapter(MoreAppointmentsActivity.this, new ArrayList<>(),
                new com.srinivasu.doctor.adapters.interfaces.ActionCallback() {
                    @Override
                    public void onItemClick(int position) {
                        Intent intent = new Intent(MoreAppointmentsActivity.this, PatAppointmentDetailsActivity.class);
                        intent.putExtra("appointment_details", Parcels.wrap(appointmentDetailsList.get(position)));
                        startActivity(intent);
                    }
                });

        appointmentsHolder.setAdapter(patientAppointmentsAdapter);

        getAppointmentList();
    }

    private void getAppointmentList() {
        getLoadingDialog().show("Loading...");
        retrofitInterface.getPatientAppointmentHistory(getLocalStorageData().getID(),1, 10 ).enqueue(new Callback<ResponseObject<List<AppointmentDetails>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<AppointmentDetails>>> call, Response<ResponseObject<List<AppointmentDetails>>> response) {
                closeLoadingDialog();
                if(response.isSuccessful() && response.body()!= null){
                    if(response.body().getData()!=null && response.body().getData().size()>0){
                        for(int i=0; i<response.body().getData().size(); i++){
                            AppointmentDetails appointmentDetails = response.body().getData().get(i);
                            appointmentDetailsList.add(appointmentDetails);
                        }
                        patientAppointmentsAdapter.setItems(appointmentDetailsList);
                    }else{
                        appointmentsHolder.setVisibility(View.GONE);
                        textNoData.setVisibility(View.VISIBLE);
                    }
                }else{
                    shortToast("Unable to get appointments for today");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<AppointmentDetails>>> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                showToast(t.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_home) {
            finish();
            /*Intent intent = new Intent(this, PatientMainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/

        }else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
