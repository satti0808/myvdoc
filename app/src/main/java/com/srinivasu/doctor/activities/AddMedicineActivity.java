package com.srinivasu.doctor.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.models.RequestAddMedicine;
import com.srinivasu.doctor.model.ResponseObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMedicineActivity extends BaseActivity {
    EditText editName, editDrugName, editRoute, editFrequencies, editStrength;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medicine);

        setupActionBar();
        setToolbarTitle("Add Medicine");

        editName=(EditText)findViewById(R.id.et_name);
        editDrugName=(EditText)findViewById(R.id.et_drug_name);
        editRoute=(EditText)findViewById(R.id.et_route);
        editFrequencies=(EditText)findViewById(R.id.et_freq);
        editStrength=(EditText)findViewById(R.id.et_strength);

        Button btn_save=(Button)findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMedicine();
            }
        });
    }

    private void addMedicine(){
       getLoadingDialog().show("Loading...");
        RequestAddMedicine addMedicine = new RequestAddMedicine(editName.getText().toString(), editDrugName.getText().toString(),
                editRoute.getText().toString(), editFrequencies.getText().toString(), editStrength.getText().toString());

        retrofitInterface.addMedicine(addMedicine).enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if(response.isSuccessful() && response.body()!= null){
                    if(response.body().isSuccess()){
                        showToast("Medicine Added successfully");
                        clearAllFields();
                    }else{
                        showToast(response.body().getMessage());
                    }
                }else{
                    showToast("Unable to add medicine");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showToast(t.getMessage());
            }
        });
    }
    public void clearAllFields(){
        editName.setText(null);
        editDrugName.setText(null);
        editRoute.setText(null);
        editFrequencies.setText(null);
        editStrength.setText(null);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_home) {
            finish();
        }else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
