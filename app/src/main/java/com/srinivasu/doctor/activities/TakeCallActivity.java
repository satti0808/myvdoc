package com.srinivasu.doctor.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.quickblox.users.model.QBUser;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.application.App;
import com.srinivasu.doctor.gcm.GooglePlayServicesHelper;
import com.srinivasu.doctor.model.ResultData;
import com.srinivasu.doctor.model.TestListModel;
import com.srinivasu.doctor.util.QBResRequestExecutor;
import com.srinivasu.doctor.utils.Consts;
import com.srinivasu.doctor.utils.SharedPrefsHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TakeCallActivity extends AppCompatActivity {
    ImageView ivPhone,iv_tests,iv_note,iv_video_cal;
    String phno;
    SharedPreferences pref;
    SharedPrefsHelper sharedPrefsHelper;
    protected GooglePlayServicesHelper googlePlayServicesHelper;
    protected QBResRequestExecutor requestExecutor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_call);
        pref = getSharedPreferences("mydoc", 0);
        requestExecutor = App.getInstance().getQbResRequestExecutor();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        googlePlayServicesHelper = new GooglePlayServicesHelper();
        tryReLoginToChat();
        getSupportActionBar().setTitle(getIntent().getStringExtra("name"));
        //Toast.makeText(TakeCallActivity.this,getIntent().getStringExtra("pid"),Toast.LENGTH_LONG).show();
        phno = getIntent().getStringExtra("phno");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ivPhone=(ImageView)findViewById(R.id.ivPhone);
        iv_tests=(ImageView)findViewById(R.id.iv_tests);
        iv_note=(ImageView)findViewById(R.id.iv_note);
        iv_video_cal=(ImageView)findViewById(R.id.iv_video_cal);
        iv_video_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // OpponentsActivity1.start(TakeCallActivity.this, false);
                Intent intent = new Intent(getApplicationContext(), OpponentsTest1Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(Consts.EXTRA_IS_STARTED_FOR_CALL, false);
                intent.putExtra("pid", getIntent().getStringExtra("pid"));
                startActivity(intent);
            }
        });
        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPermissionGranted()){
                   call_action();
                }
            }
        });
        iv_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    addNote();

            }
        });
        
        iv_tests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suggestTests();
            }
        });
        getTests();
    }
    private void tryReLoginToChat() {
        //Toast.makeText(getApplicationContext(),"Please call again..."+sharedPrefsHelper.hasQbUser(),Toast.LENGTH_LONG).show();
        if (sharedPrefsHelper.hasQbUser()) {
            QBUser qbUser = sharedPrefsHelper.getQbUser();
//            CallService.start(this, qbUser);
        }
    }
    private void addNote() {
        final View view = getLayoutInflater().inflate(R.layout.dialog_note, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(TakeCallActivity.this,R.style.ProgressDialogTheme).create();
        alertDialog.setTitle("");
        alertDialog.setCancelable(false);
        final EditText etNote = (EditText) view.findViewById(R.id.etNote);

        TextView btn_save = (TextView) view.findViewById(R.id.btn_save);
        TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                submitNote(etNote.getText().toString(),getIntent().getStringExtra("aid"));
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }

    private void submitNote(String note,final String aID) {
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("Note",   note);
            Call<ResultData> call = api.addNote(pref.getString("token","-"),paramObject.toString(),aID);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    //pd.dismiss();
                    //pb.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),"Note is submitted sucessfully.",Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    // pd.dismiss();
                    //pb.setVisibility(View.GONE);
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void submitTest(String TestIDs,final String aID) {
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("AppointmentID",   aID);
            paramObject.put("TestID",   TestIDs);
            Call<ResultData> call = api.addNote(pref.getString("token","-"),paramObject.toString(),aID);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    //pd.dismiss();
                    //pb.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),"Tests are submitted sucessfully.",Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    // pd.dismiss();
                    //pb.setVisibility(View.GONE);
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getTests(){

        ApiInterface api = RetroClient.getApiService();
        //paramObject.put("password", "1234");
        Call<TestListModel> call = api.getTests(pref.getString("token","-"));
        call.enqueue(new Callback<TestListModel>() {
            @Override
            public void onResponse(Call<TestListModel> call, Response<TestListModel> response) {

                TestListModel pm = response.body();
                if(pm!=null) {
                    if(pm.getData1().size()>0) {
                        tests=new String[pm.getData1().size()];
                        testids=new String[pm.getData1().size()];
                        for(int i=0;i<pm.getData1().size();i++){
                            tests[i]=pm.getData1().get(i).getTestName();
                            testids[i]=pm.getData1().get(i).getID();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<TestListModel> call, Throwable t) {

                Toast.makeText(TakeCallActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

    }

     String[] tests=null,testids=null;
    private void suggestTests(){
        Dialog dialog;
        final ArrayList itemsSelected = new ArrayList();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Tests");
        builder.setMultiChoiceItems(tests, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedItemId, boolean isSelected) {
                        if (isSelected) {
                            itemsSelected.add(testids[selectedItemId]);
                        } else if (itemsSelected.contains(testids[selectedItemId])) {
                            itemsSelected.remove(testids[selectedItemId]);
                        }
                    }
                })
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String str=android.text.TextUtils.join(",", itemsSelected);
                //Toast.makeText(getApplicationContext(),str,Toast.LENGTH_SHORT).show();
                        submitTest(str,getIntent().getStringExtra("aid"));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        dialog = builder.create();
        dialog.show();
    }
    public  boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    public void call_action(){

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phno));
        startActivity(callIntent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
