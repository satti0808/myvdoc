package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.BackendAPIs;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.apis.RetrofitInterface;
import com.srinivasu.doctor.application.App;
import com.srinivasu.doctor.db.LocalStorageData;
import com.srinivasu.doctor.dialogs.LoadingDialog;
import com.srinivasu.doctor.util.QBResRequestExecutor;
import com.srinivasu.doctor.utils.Consts;
import com.srinivasu.doctor.utils.ErrorUtils;
import com.srinivasu.doctor.utils.SharedPrefsHelper;

import org.json.JSONObject;

import java.lang.reflect.Field;

import okhttp3.RequestBody;

/**
 * Anže Kožar
 */
public abstract class BaseActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public ActionBar actionBar;
    public Context context;

    SharedPrefsHelper sharedPrefsHelper;
    private ProgressDialog progressDialog;
    protected QBResRequestExecutor requestExecutor;

    private LocalStorageData localStorageData;
    private RetroClient retroClient;
    public ApiInterface api;
    public ApiInterface apiInterface;
    private LoadingDialog loadingDialog;

    private BackendAPIs backendAPIs;
    public RetrofitInterface retrofitInterface;

    public JSONObject paramObject;

    Handler myHandler;

    String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = getSupportActionBar();
        context = getApplicationContext();

        // Hack. Forcing overflow button on actionbar on devices with hardware menu button
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }
        requestExecutor = App.getInstance().getQbResRequestExecutor();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();

        myHandler = new Handler();

        api = RetroClient.getApiService();
        apiInterface =getRetroClient().getNewRetrofit().create(ApiInterface.class);
        retrofitInterface = getBackendAPIs().baseAdapter().create(RetrofitInterface.class);

        paramObject = new JSONObject();
    }

    //==============  ACTIONBAR METHODS  ==============//

    protected void setupActionBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            /*actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);*/
            actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);

            }
        }
    }

    public void setToolbarTitle(String title){
        if (toolbar != null){
            actionBar = getSupportActionBar();
            TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_text);
            if (actionBar != null) {
                actionBar.setTitle("");
                toolbarTitle.setText(title);
            }
        }
    }

    public void initDefaultActionBar() {
        String currentUserFullName = "";
        String currentRoomName = sharedPrefsHelper.get(Consts.PREF_CURREN_ROOM_NAME, "");

        if (sharedPrefsHelper.getQbUser() != null) {
            currentUserFullName = sharedPrefsHelper.getQbUser().getFullName();
        }

        setActionBarTitle(currentRoomName);
        setActionbarSubTitle(String.format(getString(R.string.subtitle_text_logged_in_as), currentUserFullName));
    }


    public void setActionbarSubTitle(String subTitle) {
        if (actionBar != null)
            actionBar.setSubtitle(subTitle);
    }

    public void removeActionbarSubTitle() {
        if (actionBar != null)
            actionBar.setSubtitle(null);
    }

    /*
     *   Initialize LocalStorage Class
     * */
    public LocalStorageData getLocalStorageData(){
        if (localStorageData == null){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    /*
     *   Initialize Backened Class
     * */
    public BackendAPIs getBackendAPIs(){
        if (backendAPIs == null){
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }


    public RetroClient getRetroClient(){
        if (retroClient == null){
            retroClient = new RetroClient(context);
        }
        return retroClient;
    }

    public LoadingDialog getLoadingDialog() {
        closeLoadingDialog();
        if (loadingDialog == null){
            loadingDialog = new LoadingDialog(this);

        }
        return loadingDialog;
    }

    public void closeLoadingDialog(){
        if ( loadingDialog != null ){
            loadingDialog.closeDialog();
        }
    }

    void showProgressDialog(@StringRes int messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            // Disable the back button
            DialogInterface.OnKeyListener keyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
            progressDialog.setOnKeyListener(keyListener);
        }

        progressDialog.setMessage(getString(messageId));

        progressDialog.show();

    }

    void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    protected void showErrorSnackbar(@StringRes int resId, Exception e,
                                     View.OnClickListener clickListener) {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        if (rootView != null) {
            ErrorUtils.showSnackbar(rootView, resId, e, R.string.dlg_retry, clickListener);
        }
    }

    public void setActionBarTitle(CharSequence title) {
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void longToast(String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }

    public void longToast(int resId){
        Toast.makeText(context,resId,Toast.LENGTH_LONG).show();
    }

    public void shortToast(String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public void showToast(String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    public void hideSoftKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}




