package com.srinivasu.doctor.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.ResponseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLeaveActivity extends BaseActivity {
    EditText tv_date,tv_start_time,tv_end_time, et_reason;
    CheckBox chbFullDay,chbRecurring;
    Button btn_submit;
    boolean fg_fullday,fg_recurring;
    int mStartHour,mStartMinute;
    int mEndHour,mEndMinute;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave);
        pref = getSharedPreferences("mydoc", 0);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Apply Leave");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_date=(EditText) findViewById(R.id.tv_date);
        btn_submit=(Button)findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chbFullDay.isChecked()){
                    fg_fullday=true;
                }else{
                    fg_fullday=false;
                }

                if(chbRecurring.isChecked()){
                    fg_recurring=true;
                }else{
                    fg_recurring=false;
                }
                addLeave();
            }
        });
        tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDate();
            }
        });
        tv_start_time=(EditText)findViewById(R.id.tv_start_time);
        tv_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStartTime();
            }
        });
        tv_end_time=(EditText)findViewById(R.id.tv_end_time);
        tv_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEndTime();
            }
        });

        et_reason=(EditText)findViewById(R.id.et_reason);

        chbFullDay=(CheckBox)findViewById(R.id.chbFullDay);
        chbRecurring=(CheckBox)findViewById(R.id.chbRecurring);
    }
    int mYear,mMonth,mDay;
    private void getDate(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddLeaveActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tv_date.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();

    }

    private void getStartTime(){
        final Calendar c = Calendar.getInstance();
        mStartHour = c.get(Calendar.HOUR_OF_DAY);
        mStartMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddLeaveActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        tv_start_time.setText(hourOfDay + ":" + minute);
                    }
                }, mStartHour, mStartMinute, false);
        timePickerDialog.show();
    }

    private void getEndTime(){
        final Calendar c = Calendar.getInstance();
        mEndHour = c.get(Calendar.HOUR_OF_DAY);
        mEndMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddLeaveActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        tv_end_time.setText(hourOfDay + ":" + minute);
                    }
                }, mEndHour, mEndMinute, false);
        timePickerDialog.show();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    private void addLeave(){
        pd = new ProgressDialog(AddLeaveActivity.this);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("Date", tv_date.getText().toString());
            paramObject.put("StartTime", tv_date.getText().toString()+" "+tv_start_time.getText().toString()+":00");
            paramObject.put("EndTime", tv_date.getText().toString()+" "+tv_end_time.getText().toString()+":00");
            paramObject.put("Reason", "Test Reason");
            paramObject.put("FullDay", fg_fullday);
            paramObject.put("recurring", fg_recurring);
            paramObject.put("doctorID", pref.getString("doctorId","-"));

            Call<ResponseObject> call = api.addLeave(pref.getString("token","-"),paramObject.toString());
            call.enqueue(new Callback<ResponseObject>() {
                @Override
                public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                    pd.dismiss();
                    if(response.isSuccessful()){
                        if(response.body()!= null && response.body().isSuccess()){
                            shortToast("Leave Added Successfully");
                            finish();
                        }else{
                            shortToast(response.body().getMessage());
                        }
                    }else{
                        shortToast("Unable to add leave, Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<ResponseObject> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(AddLeaveActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
