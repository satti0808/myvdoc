package com.srinivasu.doctor.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.ResultData;
import com.srinivasu.doctor.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentScreenActivity extends BaseActivity implements PaymentResultListener {
    Button payNow;
    Checkout checkout;
    SharedPreferences pref;
    TextView tv_name, tv_email, tv_phone, tv_date, tv_time, tv_fee, tv_call_type;

    String amount = "", slotTime = "", selectedDate = "", slotId = "", appointmentType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        pref = getSharedPreferences("mydoc", 0);
        getSupportActionBar().setTitle("Payment");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        slotId = getIntent().getStringExtra("slot_id");
        selectedDate = getIntent().getStringExtra("sel_date");
        slotTime = getIntent().getStringExtra("slot_time");
        appointmentType = getIntent().getStringExtra("appointment_type");

        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_fee = (TextView) findViewById(R.id.tv_fee);
        tv_call_type = (TextView) findViewById(R.id.tv_call_type);

        tv_name.setText(pref.getString("username", "-"));
        tv_email.setText(pref.getString("email", "-"));
        tv_phone.setText(pref.getString("phno", "-"));
        tv_date.setText(selectedDate);
        tv_time.setText(slotTime);
        tv_fee.setText(getIntent().getStringExtra("amount"));
        tv_call_type.setText(getIntent().getStringExtra("type"));
        //tv_audio_call_fee.setText("\u20B9"+list_appointment_types.get(1).getAmount());

        amount = getIntent().getStringExtra("amount");
        checkout = new Checkout();
        checkout.setKeyID(Constants.RAZORPAY_KEY);
        payNow = (Button) findViewById(R.id.payNow);
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amount.contentEquals("0")) {
                    addAppointment("");
                } else {
                    try {
                        JSONObject options = new JSONObject();
                        options.put("name", getLocalStorageData().getUserName());
                        options.put("description", "MyDoc");
                        options.put("image", "https://dhrutidesign.com/myvdoc/index_files/logo.png");
                        //options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
                        options.put("theme.color", "#F2A322");
                        options.put("currency", "INR");
                        options.put("amount", getIntent().getStringExtra("amount") + "00");
                        //options.put("amount", amount+"00");//pass amount in currency subunits
                        options.put("prefill.email", getLocalStorageData().getPatientsDotcotrId());
                        options.put("prefill.contact", pref.getString("phno", "-"));
                        checkout.open(PaymentScreenActivity.this, options);
                    } catch (Exception e) {
                        //Log.e(TAG, "Error in starting Razorpay Checkout", e);
                    }
                }
            }
        });
    }

    @Override
    public void onPaymentSuccess(String payment_id) {
        addAppointment(payment_id);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showToast(s);
    }

    private void addAppointment(String payment_id) {
        try {
            JSONObject paramObject = new JSONObject();
            if (getLocalStorageData().getQBUser().getId() > 0) {
                paramObject.put("VideoID", getLocalStorageData().getQBUser().getId());
                paramObject.put("date", selectedDate +" "+slotTime);
                paramObject.put("slotID", slotId);
                if (!TextUtils.isEmpty(payment_id)) {
                    paramObject.put("ConfirmationCode", payment_id);
                }
                paramObject.put("DoctorID", getLocalStorageData().getPatientsDotcotrId());
                paramObject.put("Source", "Paytm");
                paramObject.put("Amount", amount);
                paramObject.put("AppointmentType", appointmentType);
                paramObject.put("status", "Accepted");
                paramObject.put("startTime", selectedDate);
                paramObject.put("endTime", selectedDate);
                paramObject.put("PatientID", getLocalStorageData().getID());

                //paramObject.put("password", "1234");
                retrofitInterface.addAppointment(paramObject.toString()).enqueue(new Callback<ResultData>() {
                    @Override
                    public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                        closeLoadingDialog();
                        if (response.isSuccessful()) {
                            ResultData pm = response.body();
                            Toast.makeText(getApplicationContext(), "Appointment is created successfully.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PaymentScreenActivity.this, PatientMainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {
                            //Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResultData> call, Throwable t) {
                        closeLoadingDialog();
                        call.cancel();
                        //Toast.makeText(context,t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                displayConfirmationDialog();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), PatientMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void displayConfirmationDialog() {

        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Session has been expired, Please login again");
        // add the buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("mydoc", 0);
                SharedPreferences.Editor et = pref.edit();
                et.putString("is_login", "no");
                et.commit();
                getLocalStorageData().clearQBUser();
                getLocalStorageData().clearUserDetails();
                Intent intentg = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intentg);
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        dialog = builder.create();
        dialog.show();
    }

}
