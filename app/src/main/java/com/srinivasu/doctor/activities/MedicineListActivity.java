package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.MedicineListAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.PrescriptionListModel;
import com.srinivasu.doctor.model.PrescriptionModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicineListActivity extends AppCompatActivity {
    RecyclerView rv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_list);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Medicine Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv=(RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MedicineListActivity.this, LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(linearLayoutManager);
        getDetails();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    MedicineListAdapter ma;
    private void getDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(MedicineListActivity.this);
        pd.setTitle("Please wait,Data is being retrived.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {

            Call<PrescriptionListModel> call = api.getMedicines(pref.getString("token","-"),"108");
            call.enqueue(new Callback<PrescriptionListModel>() {
                @Override
                public void onResponse(Call<PrescriptionListModel> call, Response<PrescriptionListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<PrescriptionModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(MedicineListActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    //Toast.makeText(getActivity(),""+list_appointments.get(0).getMedicineMaster().getName(),Toast.LENGTH_SHORT).show();
                                    ma=new MedicineListAdapter(MedicineListActivity.this,list_appointments);
                                    rv.setAdapter(ma);

                                }else{
                                    Toast.makeText(MedicineListActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(MedicineListActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(MedicineListActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PrescriptionListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(MedicineListActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
