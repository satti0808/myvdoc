package com.srinivasu.doctor.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.ApprovePostponeAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.AppointmentListApiModel;
import com.srinivasu.doctor.model.AppointmentModel;
import com.srinivasu.doctor.model.TimeAvailabilityPojo;
import com.srinivasu.doctor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentApprovePostponeActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<TimeAvailabilityPojo> a1;
    ApprovePostponeAdapter timeAvailabilityAdapter;
TextView tv_TODAY;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_availability);
        tv_TODAY=(TextView)findViewById(R.id.tv_TODAY);
        tv_TODAY.setVisibility(View.VISIBLE);
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
       String dd= Utils.displayDateFormat(mYear+"-"+(mMonth+1)+"-"+mDay);
        tv_TODAY.setText("Today ("+ dd +")");

        getSupportActionBar().setTitle("Approve or Postpone Appointment");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        getDetails(mYear+"-"+(mMonth+1)+"-"+mDay);

        Button btn_sel_date=(Button)findViewById(R.id.btn_sel_date);
        btn_sel_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStartDate();
            }
        });
    }
    int mStartYear,mStartMonth,mStartDay;
    private void getStartDate(){
        final Calendar c = Calendar.getInstance();
        mStartYear = c.get(Calendar.YEAR);
        mStartMonth = c.get(Calendar.MONTH);
        mStartDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AppointmentApprovePostponeActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getDetails(year + "-"+(monthOfYear+1)+"-"+dayOfMonth);
                        String ds=Utils.displayDateFormat(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
                        tv_TODAY.setText(ds);
                    }
                }, mStartYear, mStartMonth, mStartDay);
        datePickerDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    ProgressDialog pd;
    SharedPreferences pref;
    int mYear,mMonth,mDay;
    private void getDetails(String sdate) {

        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(AppointmentApprovePostponeActivity.this);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("date", sdate);
            //paramObject.put("password", "1234");
            Call<AppointmentListApiModel> call = api.getApointmentsOfADoctor(pref.getString("token", "-"), pref.getString("doctorId", "-"), paramObject.toString());
            call.enqueue(new Callback<AppointmentListApiModel>() {
                @Override
                public void onResponse(Call<AppointmentListApiModel> call, Response<AppointmentListApiModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        AppointmentModel list_appointments = response.body().getData();
                        if (list_appointments == null) {
                            Toast.makeText(AppointmentApprovePostponeActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                        } else {
                            if (list_appointments.getAppointments() != null) {
                                if (list_appointments.getAppointments().size() > 0) {
                                    //Toast.makeText(TimeAvailabilityActivity.this, list_appointments.getAppointments().get(0).getAppointment_time(), Toast.LENGTH_SHORT).show();
                                    timeAvailabilityAdapter = new ApprovePostponeAdapter(AppointmentApprovePostponeActivity.this, list_appointments.getAppointments(), getIntent().getStringExtra("sel_date"));  //attach adapter class with therecyclerview
                                    recyclerView.setAdapter(timeAvailabilityAdapter);
                                } else {
                                    Toast.makeText(AppointmentApprovePostponeActivity.this, "No Appointments for this date.", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(AppointmentApprovePostponeActivity.this, "No Appointments for this date.", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        Toast.makeText(AppointmentApprovePostponeActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AppointmentListApiModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(AppointmentApprovePostponeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}