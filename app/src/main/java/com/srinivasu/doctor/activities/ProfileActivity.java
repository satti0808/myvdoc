package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.ProfileModel;
import com.srinivasu.doctor.model.ResultData;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    EditText et_uname,et_email,et_phoneNo;
    String pwd;
    Button btn_save;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        et_uname=(EditText) findViewById(R.id.et_uname);
        et_email=(EditText) findViewById(R.id.et_email);
        et_phoneNo=(EditText) findViewById(R.id.et_phoneNo);
        btn_save =(Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDetails();
            }
        });
        getDetails();
    }

    ProgressDialog pd;
    SharedPreferences pref;
    String phno="";
    private void getDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(ProfileActivity.this);
        pd.setTitle("Please wait,Data is being fetched.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            //paramObject.put("date", getIntent().getStringExtra("sdate"));
            paramObject.put("password", "1234");
            Call<ProfileModel> call = api.getProfile(pref.getString("token","-"),pref.getString("userID","-"));
            call.enqueue(new Callback<ProfileModel>() {
                @Override
                public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        ProfileModel pm= response.body();
                        if(pm==null){
                            Toast.makeText(ProfileActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            pwd=pm.getPassword();
                            et_uname.setText(pm.getUserName());
                            et_email.setText(pm.getEmail());
                            et_phoneNo.setText(pm.getPhoneNo());
                        }

                    }else{
                        Toast.makeText(ProfileActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProfileModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(ProfileActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(ProfileActivity.this);
        pd.setTitle("Please wait,Data is being fetched.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("userName", et_uname.getText().toString());
            paramObject.put("password", pwd);
            paramObject.put("email", et_email.getText().toString());
            paramObject.put("phoneNo", et_phoneNo.getText().toString());
            Call<ResultData> call = api.updateProfile(pref.getString("token","-"),paramObject.toString(),pref.getString("userID","-"));
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        ResultData pm= response.body();
                        if(pm==null){
                            Toast.makeText(ProfileActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ProfileActivity.this,"Updated successfully.",Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }else{
                        Toast.makeText(ProfileActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(ProfileActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
