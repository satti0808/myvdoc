package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.PatientAppointmentDataModel;
import com.srinivasu.doctor.model.PatientAppointmentModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientDetailsActivity extends AppCompatActivity {
    Button btn_take_cal;
    TextView tv_name,tv_sex,tv_date,tv_time;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        getSupportActionBar().setTitle("Patient Details");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_name=(TextView)findViewById(R.id.tv_name);
        tv_sex=(TextView)findViewById(R.id.tv_sex);
        tv_date=(TextView)findViewById(R.id.tv_date);
        tv_time=(TextView)findViewById(R.id.tv_time);


        btn_take_cal=(Button)findViewById(R.id.btn_take_cal);
        btn_take_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentg= new Intent(PatientDetailsActivity.this, TakeCallActivity.class);
                intentg.putExtra("phno",phno);
                intentg.putExtra("aid",getIntent().getStringExtra("aid"));
                intentg.putExtra("pid",getIntent().getStringExtra("pid"));

                intentg.putExtra("name",tv_name.getText().toString());
                startActivity(intentg);
            }
        });
        getDetails();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    String phno="";
    private void getDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(PatientDetailsActivity.this);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("date", getIntent().getStringExtra("sdate"));
            //paramObject.put("password", "1234");
            Call<PatientAppointmentModel> call = api.getApointmentsOfAPatient(pref.getString("token","-"),getIntent().getStringExtra("pid"),paramObject.toString());
            call.enqueue(new Callback<PatientAppointmentModel>() {
                @Override
                public void onResponse(Call<PatientAppointmentModel> call, Response<PatientAppointmentModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<PatientAppointmentDataModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(PatientDetailsActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{

                            if(list_appointments.size()>0){
                                if(list_appointments.size()>0) {
                                    tv_name.setText(list_appointments.get(0).getAppointments().getPatientName());
                                    tv_sex.setText(list_appointments.get(0).getAppointments().getSex());
                                    tv_date.setText(list_appointments.get(0).getAppointments().getDOB());
                                    tv_time.setText(list_appointments.get(0).getStartTime());
                                    //Toast.makeText(PatientDetailsActivity.this, list_appointments.get(0).getAppointments().getPhoneNo(), Toast.LENGTH_SHORT).show();
                                    //timeAvailabilityAdapter=new TimeAvailabilityAdapter(TimeAvailabilityActivity.this,list_appointments.getAppointments(),getIntent().getStringExtra("sel_date"));  //attach adapter class with therecyclerview
                                    //recyclerView.setAdapter(timeAvailabilityAdapter);
                                    phno=list_appointments.get(0).getAppointments().getPhoneNo();
                                }else{
                                    Toast.makeText(PatientDetailsActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(PatientDetailsActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(PatientDetailsActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PatientAppointmentModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(PatientDetailsActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
