package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.PatientsFragmentAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.PatientPojo;
import com.srinivasu.doctor.model.PaymentListModel;
import com.srinivasu.doctor.model.PaymentModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientReportsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<PatientPojo> a1;
    PatientsFragmentAdapter patientsFragmentAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_patients);
        getSupportActionBar().setTitle("Reports");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PatientReportsActivity.this, LinearLayoutManager.VERTICAL,false);recyclerView.setLayoutManager(linearLayoutManager);

        getDetails();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    private void getDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(PatientReportsActivity.this);
        pd.setTitle("Please wait,Data is being retrived.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {

            Call<PaymentListModel> call = api.getPaymentsOfAPatient(pref.getString("token","-"),pref.getString("doctorId","-"));
            call.enqueue(new Callback<PaymentListModel>() {
                @Override
                public void onResponse(Call<PaymentListModel> call, Response<PaymentListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<PaymentModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(PatientReportsActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    patientsFragmentAdapter=new PatientsFragmentAdapter(PatientReportsActivity.this,list_appointments);
                                    recyclerView.setAdapter(patientsFragmentAdapter);

                                }else{
                                    Toast.makeText(PatientReportsActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(PatientReportsActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(PatientReportsActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PaymentListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(PatientReportsActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
