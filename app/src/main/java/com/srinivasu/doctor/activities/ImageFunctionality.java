package com.srinivasu.doctor.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.AttachmentAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.AttachmentsListModel;
import com.srinivasu.doctor.model.AttachmentsModel;
import com.srinivasu.doctor.model.ResultData;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageFunctionality extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    Button select_image,bt_reg;
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    private static final String SERVER_PATH = "http://backend.myvdoc.in:3000/";
    //http://backend.myvdoc.in:3000/appointment/upload/images/105
    private Uri uri;
    SharedPreferences pref;
    RecyclerView rv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.functionality_image);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Attachments");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getSharedPreferences("mydoc", 0);
        //Toast.makeText(this,getIntent().getStringExtra("aid"),Toast.LENGTH_LONG).show();
        select_image=(Button)findViewById(R.id.select_image);
        bt_reg=(Button)findViewById(R.id.bt_reg);

        rv=(RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ImageFunctionality.this, LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(new GridLayoutManager(this, 2));

        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
            }
        });
        bt_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageToServer();
            }
        });
        getDetails();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, ImageFunctionality.this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            uri = data.getData();
            if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                String filePath = getRealPathFromURIPath(uri, ImageFunctionality.this);
                file = new File(filePath);

            } else {
                EasyPermissions.requestPermissions(this, "check", READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
    }
    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
    File file;
    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if(uri != null){
            String filePath = getRealPathFromURIPath(uri, ImageFunctionality.this);
            file = new File(filePath);
             uploadImageToServer();
        }
    }
    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        //Log.d(TAG, "Permission has been denied");

    }
    ProgressDialog pd;
    private void uploadImageToServer(){
        pd=new ProgressDialog(ImageFunctionality.this);
        pd.setTitle("Loading");
        pd.show();
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface uploadImage = retrofit.create(ApiInterface.class);
        Call<ResultData> fileUpload = uploadImage.addAttachment(pref.getString("token","-"),fileToUpload,getIntent().getStringExtra("aid"));
        fileUpload.enqueue(new Callback<ResultData>() {
            @Override
            public void onResponse(Call<ResultData> call, Response<ResultData> response) {

                pd.dismiss();
                Toast.makeText(ImageFunctionality.this, "Uploaded successfully. ", Toast.LENGTH_LONG).show();
                finish();
            }
            @Override
            public void onFailure(Call<ResultData> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ImageFunctionality.this, "Error"+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getDetails(){
        ApiInterface api = RetroClient.getApiService();
        try {
            Call<AttachmentsListModel> call = api.getImagesList(pref.getString("token","-"),getIntent().getStringExtra("aid"));
            call.enqueue(new Callback<AttachmentsListModel>() {
                @Override
                public void onResponse(Call<AttachmentsListModel> call, Response<AttachmentsListModel> response) {
                    if (response.isSuccessful()) {
                        AttachmentsModel attachments = response.body().getData1();
                        if(attachments==null){
                            //tv_nodata_appointment.setVisibility(View.VISIBLE);
                            //recyclerView.setVisibility(View.GONE);
                            //Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(attachments.getAlImages().size()>0) {
                                AttachmentAdapter homeFragmentAdapter=new AttachmentAdapter(ImageFunctionality.this,attachments.getAlImages());  //attach adapter class with therecyclerview
                                rv.setAdapter(homeFragmentAdapter);
                                Toast.makeText(ImageFunctionality.this, attachments.getAlImages().get(0).getURL(), Toast.LENGTH_SHORT).show();
                            }else{
                                //tv_nodata_appointment.setVisibility(View.VISIBLE);
                                //recyclerView.setVisibility(View.GONE);
                               // Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else{
                        //Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<AttachmentsListModel> call, Throwable t) {
                    //Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
