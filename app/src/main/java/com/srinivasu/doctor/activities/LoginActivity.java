package com.srinivasu.doctor.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.connections.tcp.QBTcpChatConnectionFabric;
import com.quickblox.chat.connections.tcp.QBTcpConfigurationBuilder;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.Utils;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCConfig;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.RetrofitInterface;
import com.srinivasu.doctor.apis.models.RequestLogin;
import com.srinivasu.doctor.application.App;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.model.UserDetails;
import com.srinivasu.doctor.util.ChatPingAlarmManager;
import com.srinivasu.doctor.util.QBResRequestExecutor;
import com.srinivasu.doctor.utils.Consts;
import com.srinivasu.doctor.utils.SettingsUtil;
import com.srinivasu.doctor.utils.SharedPrefsHelper;
import com.srinivasu.doctor.utils.ToastUtils;
import com.srinivasu.doctor.utils.WebRtcSessionManager;

import org.jivesoftware.smackx.ping.PingFailedListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    EditText et_email, et_pwd;
    Button btnLogin;

    protected QBResRequestExecutor requestExecutor;
    private QBUser userForSave;

    private QBChatService chatService;
    private QBRTCClient rtcClient;

    String uname= "",roleID="", ID="";

    SharedPreferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        pref = getApplicationContext().getSharedPreferences("mydoc", 0);
        requestExecutor = App.getInstance().getQbResRequestExecutor();
        et_email = (EditText) findViewById(R.id.et_email);
        et_pwd = (EditText) findViewById(R.id.et_pwd);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(et_email.getText().toString().trim()) && !TextUtils.isEmpty(et_pwd.getText().toString().trim())) {
                    login();
                } else {
                    ToastUtils.shortToast(R.string.no_data_found);
                }
            }
        });
    }

    private void login() {
       getLoadingDialog().show("Loading....");
       hideSoftKeyboard();
        RetrofitInterface retrofitInterface = getBackendAPIs().accessTokenAdapter().create(RetrofitInterface.class);

        retrofitInterface.getUser(new RequestLogin(et_email.getText().toString(), et_pwd.getText().toString())).enqueue(new Callback<ResponseObject<UserDetails>>() {
            @Override
            public void onResponse(Call<ResponseObject<UserDetails>> call, Response<ResponseObject<UserDetails>> response) {
                if (response.isSuccessful() && response.body().isSuccess()) {
                    assert response.body() != null;
                    if (response.body().getData().getRole().contentEquals("Doctor")) {
                        UserDetails userDetails = response.body().getData();

                        SharedPreferences.Editor et = pref.edit();
                        et.putString("username", userDetails.getUserName());
                        uname = userDetails.getUserName();
                        et.putString("token", userDetails.getToken());
                        et.putString("doctorId", userDetails.getID());
                        ID = userDetails.getID();
                        roleID = userDetails.getRoleID();
                        et.putString("pid", "0");
                        et.putString("roleID", userDetails.getRoleID());
                        et.putString("is_login", "yes");
                        et.commit();
                        getLocalStorageData().storeUserDetails(userDetails.getRoleID(), userDetails.getID(),
                                userDetails.getUserName(), userDetails.getToken(), userDetails.getEmail(), userDetails.getPhoneNo(),
                                userDetails.getAge(), userDetails.getSex());
                        Log.e(this.getClass().getSimpleName(), "Login Success");
                        startSignUpNewUser(createUserWithEnteredData());
                    } else if (response.body().getData().getRole().contentEquals("Patient")) {
                        UserDetails userDetails = response.body().getData();
                        SharedPreferences.Editor et = pref.edit();
                        et.putString("username", userDetails.getUserName());
                        et.putString("userID", userDetails.getUserID());
                        uname = userDetails.getUserName();
                        et.putString("token", userDetails.getToken());
                        et.putString("doctorId", userDetails.getDoctorID());
                        et.putString("pid", userDetails.getID());
                        ID = /*"mydocp" +*/ userDetails.getID();
                        roleID = userDetails.getRoleID();
                        et.putString("roleID", userDetails.getRoleID());
                        et.putString("email", userDetails.getEmail());
                        et.putString("phno", userDetails.getPhoneNo());
                        et.putString("is_login", "yes");
                        et.commit();
                        getLocalStorageData().storeUserDetails(userDetails.getRoleID(), userDetails.getID(),
                                userDetails.getUserName(), userDetails.getToken(), userDetails.getEmail(), userDetails.getPhoneNo(),
                                userDetails.getAge(), userDetails.getSex());
                        getLocalStorageData().storePatientsDoctor(userDetails.getDoctorID());
                        startSignUpNewUser(createUserWithEnteredData());

                    }
                }else {

                    shortToast(response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseObject<UserDetails>> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showToast(t.getMessage());

            }
        });
    }

    private void startSignUpNewUser(final QBUser newUser) {
        if (requestExecutor != null) {
            requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                        @Override
                        public void onSuccess(QBUser result, Bundle params) {
                            signInCreatedUser(newUser);
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            if (e.getHttpStatusCode() == 422) {
                                if (newUser != null) {
                                    signInCreatedUser(newUser);
                                }
                            } else {
                                moveToHomeScreen();
                            }
                        }
                    }
            );
        }
    }

    private void signInCreatedUser(final QBUser qbUser) {
        Log.d(this.getClass().getSimpleName(), "SignIn Started");
        requestExecutor.signInUser(qbUser, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                getLocalStorageData().storeQBUser(qbUser);
//                createChatService(qbUser);
                moveToHomeScreen();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(this.getClass().getSimpleName(), "Error SignIn" + e.getMessage());
                Toast.makeText(LoginActivity.this, "QB Sign in error", Toast.LENGTH_SHORT).show();
                moveToHomeScreen();
            }
        });
    }

    private void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        sharedPrefsHelper.saveQbUser(qbUser);
    }

    private QBUser createUserWithEnteredData() {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(uname) && !TextUtils.isEmpty(ID)) {
            qbUser = new QBUser();
            qbUser.setLogin(ID);
            qbUser.setFullName(uname);
            qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
        }
        return qbUser;
    }

    public void moveToHomeScreen() {
        closeLoadingDialog();
        if (roleID.contentEquals("1")) {
            Intent intentg = new Intent(LoginActivity.this, PatientMainActivity.class);
            startActivity(intentg);
            finish();
        } else {
            Intent intentg = new Intent(LoginActivity.this, DoctorMainActivity.class);
            startActivity(intentg);
            finish();
        }
    }

    private String getCurrentDeviceId() {
        return Utils.generateDeviceId(this);
    }

    private void createChatService(QBUser qbUser) {
        if (chatService == null) {
            QBTcpConfigurationBuilder configurationBuilder = new QBTcpConfigurationBuilder();
            configurationBuilder.setSocketTimeout(300);
            configurationBuilder.setReconnectionAllowed(true);
            configurationBuilder.setKeepAlive(true);
            QBChatService.setConnectionFabric(new QBTcpChatConnectionFabric(configurationBuilder));
            QBChatService.setDebugEnabled(true);
            QBChatService.setDefaultPacketReplyTimeout(10000);
            chatService = QBChatService.getInstance();
            loginToChat(qbUser);
        }
    }

    private void loginToChat(QBUser qbUser) {
        Log.d(TAG, qbUser.getId() + " " + qbUser.getLogin() + " " + qbUser.getPassword());
        chatService.login(qbUser, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                Log.d(TAG, "ChatService Login Successful");
                startActionsOnSuccessLogin(qbUser);
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(TAG, "ChatService Login Error: " + e.getMessage());
                moveToHomeScreen();
            }
        });
    }

    private void startActionsOnSuccessLogin(QBUser qbUser) {
        initPingListener();
        initQBRTCClient();
    }

    private void initPingListener() {
        ChatPingAlarmManager.onCreate(this);
        ChatPingAlarmManager.getInstanceFor().addPingListener(new PingFailedListener() {
            @Override
            public void pingFailed() {
                Log.d(TAG, "Ping Chat Server Failed");
            }
        });
    }

    private void initQBRTCClient() {
        rtcClient = QBRTCClient.getInstance(getApplicationContext());
        // Add signalling manager
        chatService.getVideoChatWebRTCSignalingManager().addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
            @Override
            public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                if (!createdLocally) {
                    rtcClient.addSignaling((QBWebRTCSignaling) qbSignaling);
                    Log.d(TAG, "\n \n RTC created locally");
                }
            }
        });

        // Configure
        QBRTCConfig.setDebugEnabled(true);
        SettingsUtil.configRTCTimers(this);

        // Add service as callback to RTCClient
        rtcClient.addSessionCallbacksListener(WebRtcSessionManager.getInstance(this));
        rtcClient.prepareToProcessCalls();
        moveToHomeScreen();
    }
}

/* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Consts.EXTRA_LOGIN_RESULT_CODE) {
            hideProgressDialog();
            boolean isLoginSuccess = data.getBooleanExtra(Consts.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(Consts.EXTRA_LOGIN_ERROR_MESSAGE);

            if (isLoginSuccess) {
                saveUserData(userForSave);
                signInCreatedUser(userForSave);
            } else {
                ToastUtils.longToast(getString(R.string.login_chat_login_error) + errorMessage);
                moveToHomeScreen();
            }
        }
    }*/