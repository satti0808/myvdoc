package com.srinivasu.doctor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;

public class PatientDrugDetailsActivity extends AppCompatActivity {
    Button btn_take_cal;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_drug_details);

        //getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_take_cal=(Button)findViewById(R.id.btn_take_cal);
        btn_take_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intentg= new Intent(PatientDrugDetailsActivity.this, TakeCallActivity.class);
                startActivity(intentg);

            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
