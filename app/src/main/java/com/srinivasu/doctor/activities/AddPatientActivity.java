package com.srinivasu.doctor.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.apis.models.RequestAddPatient;
import com.srinivasu.doctor.model.CitiesModel;
import com.srinivasu.doctor.model.PatientTypeModel;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.model.StatesModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPatientActivity extends BaseActivity {
    EditText etPatientName, editAddress, editMobileNumber, editEmail, editAge;
    int mYear, mMonth, mDay;
    String DAY, MONTH, YEAR, mGender;
    Spinner spinnerPatientType, spinnerState, spinnerCity;
    RadioButton rbMale, rbFemale;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    String cities[], citiesid[], states[], stateids[], patienttype[], patienttypeids[];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        setupActionBar();
        setToolbarTitle("Add Patient");

        rbMale = (RadioButton) findViewById(R.id.rbMale);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);

        mGender = "M";
        etPatientName = (EditText) findViewById(R.id.et_patient_name);
        editAge = (EditText) findViewById(R.id.tv_dob);
        editEmail = (EditText) findViewById(R.id.et_email_ID);
        editAddress = (EditText) findViewById(R.id.et_city);
        editMobileNumber = (EditText) findViewById(R.id.et_mobile_number);

        spinnerPatientType = (Spinner) findViewById(R.id.spin_patient_type);
        spinnerState = (Spinner) findViewById(R.id.spin_State_ID);
        spinnerCity = (Spinner) findViewById(R.id.spin_City_ID);

        getAllCities();
        getAllStates();
        getPatientTypes();

        Button btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),citiesid[spin_City_ID.getSelectedItemPosition()],Toast.LENGTH_SHORT).show();
                if (etPatientName.getText().toString().isEmpty()) {
                    showToast("Enter Patient Name");
                } else if (!editEmail.getText().toString().matches(emailPattern)) {
                    showToast("Invalid Email");
                } else if (editAddress.getText().toString().isEmpty()) {
                    showToast("Add Address");
                } else if (editMobileNumber.getText().toString().isEmpty()) {
                    showToast("Enter Mobile Number");
                } else if (editAge.getText().toString().isEmpty()) {
                    showToast("Enter Age");
                } else if (spinnerState.getSelectedItem().toString().equals("Country_ID")) {
                    showToast("Please select State");
                } else if (spinnerCity.getSelectedItem().toString().equals("Country_ID")) {
                    showToast("Please Select City");
                } else {
                    addPatient();
                }
            }
        });

    }

    private void addPatient() {
        getLoadingDialog().show("Loading....");
        String email = "";
        if (editEmail.getText().toString().isEmpty()) {
            email = "-";
        } else {
            email = editEmail.getText().toString();
        }

        RequestAddPatient addPatient = new RequestAddPatient(etPatientName.getText().toString(), editAddress.getText().toString(), email, mGender,
                editAge.getText().toString(), editMobileNumber.getText().toString(), "1", patienttypeids[spinnerPatientType.getSelectedItemPosition()],
                stateids[spinnerState.getSelectedItemPosition()], citiesid[spinnerCity.getSelectedItemPosition()]);
        retrofitInterface.addPatient(addPatient, getLocalStorageData().getID()).enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                closeLoadingDialog();
                if(response.isSuccessful() && response.body()!= null){
                    if(response.body().isSuccess()){
                        showToast("Patient added successfully");
                        finish();
                    }else{
                        showToast("Failed to add patient");
                    }
                }else{
                    showToast("Unable to add patient");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showToast(t.getMessage());
            }
        });
    }

    private void getAllCities() {
        retrofitInterface.getAllCities().enqueue(new Callback<ResponseObject<List<CitiesModel>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<CitiesModel>>> call, Response<ResponseObject<List<CitiesModel>>> response) {
                if(response.isSuccessful()){
                    if (response.body() != null) {
                        if (response.body().getData()!= null && response.body().getData().size()>0) {
                            List<CitiesModel> citiesList = new ArrayList<>();
                            citiesList.addAll(response.body().getData());
                            cities = new String[citiesList.size()];
                            citiesid = new String[citiesList.size()];
                            for (int i = 0; i < citiesList.size(); i++) {
                                cities[i] = citiesList.get(i).getCity();
                                citiesid[i] = citiesList.get(i).getID();
                            }
                            //Toast.makeText(AddPatientActivity.this, pm.getData1().get(0).getCity(), Toast.LENGTH_SHORT).show();
                            ArrayAdapter<String> adp = new ArrayAdapter<String>(AddPatientActivity.this, android.R.layout.simple_spinner_dropdown_item, cities);
                            // APP CURRENTLY CRASHING HERE
                            spinnerCity.setAdapter(adp);
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseObject<List<CitiesModel>>> call, Throwable t) {
                Toast.makeText(AddPatientActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAllStates() {
        retrofitInterface.getAllStates().enqueue(new Callback<ResponseObject<List<StatesModel>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<StatesModel>>> call, Response<ResponseObject<List<StatesModel>>> response) {
                if (response.body() != null) {
                    if (response.body().getData().size() > 0) {
                        states = new String[response.body().getData().size()];
                        stateids = new String[response.body().getData().size()];
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            states[i] = response.body().getData().get(i).getState();
                            stateids[i] = response.body().getData().get(i).getID();
                        }
                        //Toast.makeText(AddPatientActivity.this, "->"+pm.getData1().get(0).getState(), Toast.LENGTH_SHORT).show();
                        ArrayAdapter<String> adp = new ArrayAdapter<String>(AddPatientActivity.this, android.R.layout.simple_spinner_dropdown_item, states);
                        // APP CURRENTLY CRASHING HERE
                        spinnerState.setAdapter(adp);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<StatesModel>>> call, Throwable t) {
                showToast("Unable to get States, Please try after some time");
            }
        });
    }

    private void getPatientTypes() {
        ApiInterface api = RetroClient.getApiService();
        retrofitInterface.getPatientType(getLocalStorageData().getID(), "1").enqueue(new Callback<ResponseObject<List<PatientTypeModel>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<PatientTypeModel>>> call, Response<ResponseObject<List<PatientTypeModel>>> response) {
                if(response.isSuccessful() && response.body()!= null){
                    if (response.body().getData().size() > 0) {
                        patienttype = new String[response.body().getData().size()];
                        patienttypeids = new String[response.body().getData().size()];
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            patienttype[i] = response.body().getData().get(i).getType();
                            patienttypeids[i] = response.body().getData().get(i).getID();
                        }
                        //Toast.makeText(AddPatientActivity.this, pm.getData1().get(0).getCity(), Toast.LENGTH_SHORT).show();
                        ArrayAdapter<String> adp = new ArrayAdapter<String>(AddPatientActivity.this, android.R.layout.simple_spinner_dropdown_item, patienttype);
                        // APP CURRENTLY CRASHING HERE
                        spinnerPatientType.setAdapter(adp);
                    }
                }else{
                    showToast("Unable to get Patient types");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<PatientTypeModel>>> call, Throwable t) {
                showToast("Failed to fetch patient types");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_home) {
           finish();
        }else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}

