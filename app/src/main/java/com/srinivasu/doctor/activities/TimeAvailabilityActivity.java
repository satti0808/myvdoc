package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.TimeAvailabilityAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.AppointmentListApiModel;
import com.srinivasu.doctor.model.AppointmentModel;
import com.srinivasu.doctor.model.TimeAvailabilityPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeAvailabilityActivity extends BaseActivity {
    RecyclerView recyclerView;
    List<TimeAvailabilityPojo> a1;
    TimeAvailabilityAdapter timeAvailabilityAdapter;
    TextView tv_TODAY;
    Button btn_sel_date;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_availability);

        getSupportActionBar().setTitle(getIntent().getStringExtra("sel_date")+" Appointments");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        tv_TODAY=(TextView)findViewById(R.id.tv_TODAY);
        btn_sel_date=(Button)findViewById(R.id.btn_sel_date);
        btn_sel_date.setVisibility(View.GONE);
        tv_TODAY.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL,false);recyclerView.setLayoutManager(linearLayoutManager);

        getDetails();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    ProgressDialog pd;
    SharedPreferences pref;
    private void getDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(TimeAvailabilityActivity.this);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("date", getIntent().getStringExtra("sel_date"));
            //paramObject.put("password", "1234");
            Call<AppointmentListApiModel> call = api.getApointmentsOfADoctor(pref.getString("token","-"),pref.getString("doctorId","-"),paramObject.toString());
            call.enqueue(new Callback<AppointmentListApiModel>() {
                @Override
                public void onResponse(Call<AppointmentListApiModel> call, Response<AppointmentListApiModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        AppointmentModel list_appointments = response.body().getData();
                        if(list_appointments==null){
                            Toast.makeText(TimeAvailabilityActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{

                            if(list_appointments.getAppointments()!=null){
                                if(list_appointments.getAppointments().size()>0) {
                                    //Toast.makeText(TimeAvailabilityActivity.this, list_appointments.getAppointments().get(0).getAppointment_time(), Toast.LENGTH_SHORT).show();
                                    timeAvailabilityAdapter=new TimeAvailabilityAdapter(TimeAvailabilityActivity.this,list_appointments.getAppointments(),getIntent().getStringExtra("sel_date"));  //attach adapter class with therecyclerview
                                    recyclerView.setAdapter(timeAvailabilityAdapter);
                                }else{
                                    Toast.makeText(TimeAvailabilityActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(TimeAvailabilityActivity.this,"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(TimeAvailabilityActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AppointmentListApiModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(TimeAvailabilityActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
