package com.srinivasu.doctor.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.ViewPatAttachmentsAdapter;
import com.srinivasu.doctor.adapters.ViewPatLabTestAdapter;
import com.srinivasu.doctor.adapters.ViewPatPrescriptionAdapter;
import com.srinivasu.doctor.adapters.interfaces.ActionCallback;
import com.srinivasu.doctor.images.FileUtils;
import com.srinivasu.doctor.images.ImageCompressionAsyncTask;
import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.AppointmentDetailsModel;
import com.srinivasu.doctor.model.CompleteAppointmentDetails;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.utils.Constants;
import com.srinivasu.doctor.utils.DateMethods;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import org.parceler.ParcelerRuntimeException;
import org.parceler.Parcels;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatAppointmentDetailsActivity extends BaseActivity implements View.OnClickListener {

    TextView textName, textTime, textDuration, textNote, textCamera, textFile;
    RecyclerView prescriptionHolder, labTestHolder, attachmentHolder;

    ImageView imgCamera, imageFile;

    AppointmentDetails appointmentDetails;

    File compressedImageFile;
    File pdfFile;

    boolean isPDFFile= false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pat_appointment_details);

        setupActionBar();
        setToolbarTitle("Appointment Details");

        initialiseViews();
        Bundle bundle = getIntent().getExtras();
        if(bundle!= null){
            appointmentDetails = Parcels.unwrap(bundle.getParcelable("appointment_details"));
        }
        getAppointmentDetails();
    }

    private void initialiseViews() {

        textName = (TextView) findViewById(R.id.tv_doctor_name);
        textTime = (TextView) findViewById(R.id.tv_appointment_time);
        textDuration = (TextView) findViewById(R.id.tv_duration);
        textNote = (TextView) findViewById(R.id.tv_note);

        prescriptionHolder = (RecyclerView) findViewById(R.id.rv_prescription_holder);
        labTestHolder = (RecyclerView) findViewById(R.id.rv_tests_holder);
        attachmentHolder = (RecyclerView) findViewById(R.id.rv_attachment_holder);

        prescriptionHolder.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        prescriptionHolder.setHasFixedSize(true);
        labTestHolder.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        labTestHolder.setHasFixedSize(true);
        attachmentHolder.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        attachmentHolder.setHasFixedSize(true);

        textCamera = (TextView) findViewById(R.id.tv_camera);
        imgCamera = (ImageView) findViewById(R.id.iv_camera);

        textFile = (TextView) findViewById(R.id.tv_file);
        imageFile= (ImageView) findViewById(R.id.iv_file);

        textCamera.setOnClickListener(this);
        imgCamera.setOnClickListener(this);
        textFile.setOnClickListener(this);
        imageFile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == imgCamera || v == textCamera){
            Pix.start(PatAppointmentDetailsActivity.this,
                    Options.init()
                            .setRequestCode(Constants.REQUEST_CODE_OPEN_IMAGE_PICKER)
                            .setExcludeVideos(true)
                            .setCount(1));
        } else if(v == imageFile || v == textFile){

            Intent intent4 = new Intent(this, NormalFilePickActivity.class);
            intent4.putExtra("MaxNumber", 1);
            intent4.putExtra("isNeedFolderList", true);
            intent4.putExtra(NormalFilePickActivity.SUFFIX,
                    new String[] {"doc", "docx", "pdf", "xlsx","xls","csv","ppt","pptx"});
            startActivityForResult(intent4, Constants.REQUEST_CODE_PICK_FILE);

        }
    }

    private void getAppointmentDetails() {
        getLoadingDialog().show("Loading....");
        retrofitInterface.getAppointmentDetailsByID(String.valueOf(appointmentDetails.getAppointmentID()))
                .enqueue(new Callback<ResponseObject<List<CompleteAppointmentDetails>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<CompleteAppointmentDetails>>> call, Response<ResponseObject<List<CompleteAppointmentDetails>>> response) {
             closeLoadingDialog();
              if(response.isSuccessful() && response.body()!= null){
                  if(response.body().getData()!= null && response.body().getData().size()>0){
                      setData(response.body().getData().get(0));
                  }else{
                      showToast(response.message());
                      myHandler.postDelayed(new Runnable() {
                          @Override
                          public void run() {
                              finish();
                          }
                      },2500);
                  }
              }else {
                  showToast("Unable to get appointment details");
              }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<CompleteAppointmentDetails>>> call, Throwable t) {
                call.cancel();
                closeLoadingDialog();
                t.printStackTrace();
                showToast(t.getMessage());
            }
        });

    }

    private void setData(CompleteAppointmentDetails completeAppointmentDetails) {

        textName.setText(completeAppointmentDetails.getDoctor().getDoctorName());
        textTime.setText(DateMethods.getDateInFormat(completeAppointmentDetails.getStartTime(), DateMethods.HH_MM_A));
        textDuration.setText(DateMethods.getDateDiff(new SimpleDateFormat(DateMethods.UTC_FORMAT, Locale.ENGLISH),
                completeAppointmentDetails.getStartTime(), completeAppointmentDetails.getEndTime()));
        if(!TextUtils.isEmpty(completeAppointmentDetails.getNote())){
            textNote.setText(completeAppointmentDetails.getNote());
        }else{
            textNote.setVisibility(View.GONE);
        }

        if(completeAppointmentDetails.getPrescription()!= null && completeAppointmentDetails.getPrescription().size()>0){
            ViewPatPrescriptionAdapter prescriptionAdapter = new ViewPatPrescriptionAdapter(this, new ArrayList<>());
            prescriptionHolder.setAdapter(prescriptionAdapter);
            prescriptionAdapter.setItems(completeAppointmentDetails.getPrescription());
        }else{
            prescriptionHolder.setVisibility(View.GONE);
        }

        if(completeAppointmentDetails.getTestappointment()!= null && completeAppointmentDetails.getTestappointment().size()>0){
            ViewPatLabTestAdapter viewPatLabTestAdapter = new ViewPatLabTestAdapter(this, new ArrayList<>());
            labTestHolder.setAdapter(viewPatLabTestAdapter);
            viewPatLabTestAdapter.setItems(completeAppointmentDetails.getTestappointment());
        }else{
            labTestHolder.setVisibility(View.GONE);
        }

        if(completeAppointmentDetails.getAttachments()!= null && completeAppointmentDetails.getAttachments().size()>0){
            ViewPatAttachmentsAdapter attachmentsAdapter = new ViewPatAttachmentsAdapter(this, new ArrayList<>(), new ActionCallback() {
                @Override
                public void onItemClick(int position) {

                    Intent intent = new Intent(PatAppointmentDetailsActivity.this, PdfImageViewActivity.class);
                    intent.putExtra("attachment_details", Parcels.wrap(completeAppointmentDetails.getAttachments().get(position)));
                    startActivity(intent);
                }
            });
            attachmentHolder.setAdapter(attachmentsAdapter);
            attachmentsAdapter.setItems(completeAppointmentDetails.getAttachments());
        }else{
            attachmentHolder.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE_OPEN_IMAGE_PICKER && resultCode == RESULT_OK) {

            ArrayList<String> list = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            Uri uri = Uri.fromFile(new File(list.get(0)));
            compressImage(uri);
            isPDFFile = false;
        }else if(requestCode == Constants.REQUEST_CODE_PICK_FILE && resultCode == RESULT_OK){

            if(data!= null){
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);

                if(list!=null && list.size()>0){
                    NormalFile normalFile = list.get(0);
                    pdfFile = new File(normalFile.getPath());
                    isPDFFile = true;
                    /*application/pdf*/
                    String mime = FileUtils.getMimeType(pdfFile);
                    if(pdfFile.exists()){
                        shortToast("Uploading file");
                        uploadImage();
                    }
                }
            }
        }

    }

    private void compressImage(Uri resultUri) {

        @SuppressLint("StaticFieldLeak")
        ImageCompressionAsyncTask imageCompression = new ImageCompressionAsyncTask() {
            @Override
            protected void onPostExecute(String imageBytes) {
                // image here is compressed & ready to be sent to the server
                compressedImageFile = new File(imageBytes);
//                imgLogo.setImageURI(Uri.fromFile(compressedImageFile));
                uploadImage();
            }
        };
        // imagePath as a string
        imageCompression.execute(FileUtils.getPath(this, resultUri));
    }

    private void uploadImage() {

        MultipartBody.Part fileToUpload;

        if(isPDFFile){
            Uri uri = FileUtils.getUri(pdfFile);
            String mimeType = FileUtils.getMimeType(context, uri);

            RequestBody mFile = RequestBody.create(MediaType.parse(mimeType), pdfFile);
            fileToUpload = MultipartBody.Part.createFormData("image", pdfFile.getName(), mFile);
        }else{
            Uri uri = FileUtils.getUri(compressedImageFile);
            String mimeType = FileUtils.getMimeType(context, uri);

            RequestBody mFile = RequestBody.create(MediaType.parse(mimeType), compressedImageFile);
            fileToUpload = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), mFile);
        }


        RequestBody patientID = createPartFromString(getLocalStorageData().getID());
        RequestBody doctorID = createPartFromString("");

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("patientID",patientID);
        map.put("doctorID", doctorID);
        retrofitInterface.uploadFile(appointmentDetails.getAppointmentID(), map, fileToUpload).enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if(response.isSuccessful() && response.body()!= null){
                    closeLoadingDialog();
                    if(isPDFFile){
                        pdfFile.delete();
                    }else{
                        compressedImageFile.delete();
                    }
                    getAppointmentDetails();
                }else{
                    showToast("Unable to upload images");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                t.printStackTrace();
                call.cancel();
                showToast(t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_home) {
            Intent intent = new Intent(this, PatientMainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}