package com.srinivasu.doctor.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.AppointmentTypeListModel;
import com.srinivasu.doctor.model.AppointmentTypeModel;
import com.srinivasu.doctor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentTypeActivity extends BaseActivity {
    SharedPreferences pref;
    TextView tv_audio_call_fee,tv_video_call_fee;
    RelativeLayout rl_video_call,rl_audio_call;

    boolean isPaymentDone = false;
    String selectedDate = "", slotId="" , slotTime="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_type);

        getSupportActionBar().setTitle(Utils.displayDateFormat(getIntent().getStringExtra("sel_date"))+"  "+getIntent().getStringExtra("sel_time"));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedDate = getIntent().getStringExtra("sel_date");
        slotId = getIntent().getStringExtra("sel_time_slot");
        slotTime = getIntent().getStringExtra("sel_time");

        tv_audio_call_fee=(TextView)findViewById(R.id.tv_audio_call_fee);
        tv_video_call_fee=(TextView)findViewById(R.id.tv_video_call_fee);

        rl_video_call=(RelativeLayout)findViewById(R.id.rl_video_call);
        rl_audio_call=(RelativeLayout)findViewById(R.id.rl_audio_call);
        pref = getSharedPreferences("mydoc", 0);

        getAppointType();
        rl_audio_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), PaymentScreenActivity.class);
                intent.putExtra("sel_date",getIntent().getStringExtra("sel_date"));
                intent.putExtra("slot_id",slotId);
                intent.putExtra("slot_time", slotTime);
                intent.putExtra("amount",list_appointment_types.get(1).getAmount());
                intent.putExtra("appointment_type",list_appointment_types.get(1).getID());
                intent.putExtra("type","Audio Call");
                startActivity(intent);
//                finish();
            }
        });
        rl_video_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), PaymentScreenActivity.class);
                intent.putExtra("sel_date",selectedDate);
                intent.putExtra("slot_id",slotId);
                intent.putExtra("slot_time", slotTime);
                intent.putExtra("amount",list_appointment_types.get(0).getAmount());
                intent.putExtra("appointment_type",list_appointment_types.get(0).getID());
                intent.putExtra("type","Video Call");
                startActivity(intent);
//                finish();
            }
        });

    }
    List<AppointmentTypeModel> list_appointment_types=null;
    private void getAppointType(){
        getLoadingDialog().show("Loading...");

        try {
            paramObject.put("patientID", getLocalStorageData().getID());
            paramObject.put("slotID", slotId);
            paramObject.put("doctorID", getLocalStorageData().getPatientsDotcotrId());
            retrofitInterface.getAppointType(paramObject.toString())
                    .enqueue(new Callback<AppointmentTypeListModel>() {
                @Override
                public void onResponse(Call<AppointmentTypeListModel> call, Response<AppointmentTypeListModel> response) {
                    if (response.isSuccessful()) {
                        AppointmentTypeListModel appointments = response.body();
                        if(appointments!=null){
                            closeLoadingDialog();
                            if(appointments.getData1().size()>0){
                                list_appointment_types=appointments.getData1();
                                tv_audio_call_fee.setText("\u20B9"+list_appointment_types.get(1).getAmount());
                                tv_video_call_fee.setText("\u20B9"+list_appointment_types.get(0).getAmount());
                                if(list_appointment_types.get(0).getAmount().contentEquals("0")){
                                }else if(list_appointment_types.get(1).getAmount().contentEquals("0")){
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<AppointmentTypeListModel> call, Throwable t) {
                    closeLoadingDialog();
                    call.cancel();
                    showToast("Unable to get Doctor Fees");
                    //Toast.makeText(context,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
