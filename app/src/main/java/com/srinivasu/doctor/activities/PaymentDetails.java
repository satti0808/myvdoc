package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;

public class PaymentDetails  extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        getSupportActionBar().setTitle("Payment Details");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getDetails();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    private void getDetails(){
       /* pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(PaymentDetails.this);
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {

            Call<PaymentListModel> call = api.getPaymentsOfAPatient(pref.getString("token","-"),pref.getString("doctorId","-"));
            call.enqueue(new Callback<PaymentListModel>() {
                @Override
                public void onResponse(Call<PaymentListModel> call, Response<PaymentListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<PaymentModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    paymentFragmentAdapter=new PaymentFragmentAdapter(getActivity(),list_appointments);
                                    recyclerView.setAdapter(paymentFragmentAdapter);
                                    //Toast.makeText(getActivity(), list_appointments.get(0).getDate(), Toast.LENGTH_SHORT).show();
                                    //timeAvailabilityAdapter=new TimeAvailabilityAdapter(TimeAvailabilityActivity.this,list_appointments.getAppointments(),getIntent().getStringExtra("sel_date"));  //attach adapter class with therecyclerview
                                    //recyclerView.setAdapter(timeAvailabilityAdapter);
                                }else{
                                    Toast.makeText(getActivity(),"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(),"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PaymentListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }*/
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}