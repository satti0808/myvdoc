package com.srinivasu.doctor.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.BPListModel;
import com.srinivasu.doctor.model.BPModel;
import com.srinivasu.doctor.model.DiabetesListModel;
import com.srinivasu.doctor.model.DiabetesModel;
import com.srinivasu.doctor.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportsActivity extends BaseActivity {
    TableLayout tb_diabetes,tb_bp;
    TextView tv_add_bp,tv_add_diabetes;
    TextView tv_bp_no_details,tv_diabetes_no_details;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        tb_diabetes=(TableLayout)findViewById(R.id.tb_diabetes);
        tv_bp_no_details=(TextView)findViewById(R.id.tv_bp_no_details);
        tv_diabetes_no_details=(TextView)findViewById(R.id.tv_diabetes_no_details);
        tb_bp=(TableLayout)findViewById(R.id.tb_bp);
        tv_add_bp=(TextView)findViewById(R.id.tv_add_bp);
        tv_add_diabetes=(TextView)findViewById(R.id.tv_add_diabetes);
        tv_add_diabetes.setVisibility(View.GONE);
        tv_add_bp.setVisibility(View.GONE);
        getSupportActionBar().setTitle(getIntent().getStringExtra("name")+" Reports");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getBPDetails();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    private void getDiabetesDetails(){
        pref = getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(ReportsActivity.this);
        pd.setTitle("Please wait,Data is being retrived.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {

            retrofitInterface.getDiabetesOfAPatient(getIntent().getStringExtra("pid")).enqueue(new Callback<DiabetesListModel>() {
                @Override
                public void onResponse(Call<DiabetesListModel> call, Response<DiabetesListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<DiabetesModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(ReportsActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    tv_diabetes_no_details.setVisibility(View.GONE);
                                    LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService
                                            (Context.LAYOUT_INFLATER_SERVICE);
                                    TableRow row0 = (TableRow) inflater.inflate(R.layout.row_table_row1,null);
                                    TextView tv_date0=(TextView)row0.findViewById(R.id.tv_date);
                                    tv_date0.setText("Date");

                                    TextView tv01=(TextView)row0.findViewById(R.id.tv_pre);
                                    tv01.setText("Pre");

                                    TextView tv02=(TextView)row0.findViewById(R.id.tv_post);
                                    tv02.setText("Post");

                                    LinearLayout ll=(LinearLayout)row0.findViewById(R.id.ll);
                                    ll.setVisibility(View.GONE);

                                    tb_diabetes.addView(row0,0);
                                    for(int i = 0; i < list_appointments.size(); i ++){
                                        TableRow row = (TableRow) inflater.inflate(R.layout.row_table_row,null);
                                        LinearLayout ll1=(LinearLayout)row.findViewById(R.id.ll);
                                        ll1.setVisibility(View.GONE);
                                        TextView tv_date=(TextView)row.findViewById(R.id.tv_date);
                                        tv_date.setText(Utils.displayDateFormat(list_appointments.get(i).getDate()));

                                        TextView tv1=(TextView)row.findViewById(R.id.tv_pre);
                                        tv1.setText(""+list_appointments.get(i).getPerLunch());

                                        TextView tv2=(TextView)row.findViewById(R.id.tv_post);
                                        tv2.setText(""+list_appointments.get(i).getPostLunch());
                                        tb_diabetes.addView(row,(i+1));

                                    }

                                }else{
                                    tv_diabetes_no_details.setVisibility(View.VISIBLE);
                                    Toast.makeText(ReportsActivity.this,"No diabetes details for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                tv_diabetes_no_details.setVisibility(View.VISIBLE);
                                Toast.makeText(ReportsActivity.this,"No diabetes details for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(ReportsActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DiabetesListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(ReportsActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getBPDetails(){
        pref = getSharedPreferences("mydoc", 0);
        ApiInterface api = RetroClient.getApiService();
        try {

            retrofitInterface.getBPOfAPatient(getIntent().getStringExtra("pid")).enqueue(new Callback<BPListModel>() {
                @Override
                public void onResponse(Call<BPListModel> call, Response<BPListModel> response) {
                    getDiabetesDetails();
                    if (response.isSuccessful()) {
                        List<BPModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(ReportsActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    tv_bp_no_details.setVisibility(View.GONE);
                                    LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService
                                            (Context.LAYOUT_INFLATER_SERVICE);
                                    TableRow row0 = (TableRow) inflater.inflate(R.layout.row_table_row1,null);
                                    TextView tv_date0=(TextView)row0.findViewById(R.id.tv_date);
                                    tv_date0.setText("Date");

                                    TextView tv01=(TextView)row0.findViewById(R.id.tv_pre);
                                    tv01.setText("Systolic");

                                    TextView tv02=(TextView)row0.findViewById(R.id.tv_post);
                                    tv02.setText("Dastolic");
                                    LinearLayout ll=(LinearLayout)row0.findViewById(R.id.ll);
                                    ll.setVisibility(View.GONE);
                                    tb_bp.addView(row0,0);
                                    for(int i = 0; i < list_appointments.size(); i ++){
                                        TableRow row = (TableRow) inflater.inflate(R.layout.row_table_row,null);

                                        TextView tv_date=(TextView)row.findViewById(R.id.tv_date);
                                        tv_date.setText(Utils.displayDateFormat(list_appointments.get(i).getDate()));

                                        TextView tv1=(TextView)row.findViewById(R.id.tv_pre);
                                        tv1.setText(""+list_appointments.get(i).getSystolic());

                                        TextView tv2=(TextView)row.findViewById(R.id.tv_post);
                                        LinearLayout llr=(LinearLayout)row.findViewById(R.id.ll);
                                        llr.setVisibility(View.GONE);
                                        tv2.setText(""+list_appointments.get(i).getDiastolic());
                                        tb_bp.addView(row,(i+1));
                                    }

                                }else{
                                    tv_bp_no_details.setVisibility(View.VISIBLE);
                                    Toast.makeText(ReportsActivity.this,"No BP details for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                tv_bp_no_details.setVisibility(View.VISIBLE);
                                Toast.makeText(ReportsActivity.this,"No BP details for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(ReportsActivity.this,response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BPListModel> call, Throwable t) {

                    Toast.makeText(ReportsActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
