package com.srinivasu.doctor.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.AddDoctorSlotDaysAdapter;
import com.srinivasu.doctor.model.SingleDaySlotItem;

import java.util.ArrayList;
import java.util.List;

public class AddDoctorSlotsActivity extends BaseActivity {

    RecyclerView daysHolder;
    AddDoctorSlotDaysAdapter addDoctorSlotDaysAdapter;

    TextView textSlotDuration;
    String slotDuration = "";

    List<SingleDaySlotItem> availableDaysList;
    List<SingleDaySlotItem.TimeSlots> timeSlotsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor_slots);

        setupActionBar();
        setToolbarTitle("Available Hours");

        initialiseViews();
    }

    private void initialiseViews() {
        textSlotDuration = (TextView) findViewById(R.id.tv_slot_duration);
        textSlotDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopup(textSlotDuration);
            }
        });

        daysHolder = (RecyclerView) findViewById(R.id.item_holder);
        daysHolder.setHasFixedSize(true);
        daysHolder.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        timeSlotsList = new ArrayList<>();
        timeSlotsList.add(new SingleDaySlotItem.TimeSlots("10:00 AM", "12:00 PM"));

        availableDaysList = new ArrayList<>();
        availableDaysList.add(new SingleDaySlotItem("Monday", "1", true, timeSlotsList));
        availableDaysList.add(new SingleDaySlotItem("Tuesday", "2", true, timeSlotsList));
        availableDaysList.add(new SingleDaySlotItem("Wednesday", "3", true, timeSlotsList));
        availableDaysList.add(new SingleDaySlotItem("Thursday", "4", true, timeSlotsList));
        availableDaysList.add(new SingleDaySlotItem("Friday", "5", true, timeSlotsList));
        availableDaysList.add(new SingleDaySlotItem("Saturday", "6", true, timeSlotsList));
        availableDaysList.add(new SingleDaySlotItem("Sunday", "7", true, timeSlotsList));

        addDoctorSlotDaysAdapter = new AddDoctorSlotDaysAdapter(this, availableDaysList, new AddDoctorSlotDaysAdapter.CallBack() {
            @Override
            public void onSwitchChanged(int position) {

            }

            @Override
            public void onAddHoursClicked(int position) {

            }
        });

        daysHolder.setAdapter(addDoctorSlotDaysAdapter);

    }

    private void showPopup(TextView textSlotDuration) {

        PopupMenu popup = new PopupMenu(this, textSlotDuration);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_slot_time_duration, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
//                if (item.getItemId() == R.id.action_15min) {
//                    slotDuration = "15 Mins";
//                } else if (item.getItemId() == R.id.action_30min) {
//                    slotDuration = "30 Mins";
//                } else if (item.getItemId() == R.id.action_45min) {
//                    slotDuration = "45 Mins";
//                } else if (item.getItemId() == R.id.action_1hr) {
//                    slotDuration = "60 Mins";
//                }
                textSlotDuration.setText(item.getTitle());
                return false;
            }
        });
        popup.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_home) {
            finish();
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}