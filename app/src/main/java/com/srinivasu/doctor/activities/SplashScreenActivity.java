package com.srinivasu.doctor.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.utils.Consts;

public class SplashScreenActivity extends BaseActivity {
    SharedPreferences pref;

    public String[] permissions = {Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        TAG = this.getClass().getSimpleName();

        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)) {
            askForPermission(permissions, Consts.CAMERA_PERMISSION_CODE);
        }
        else{
            if (getLocalStorageData().getQBUser() != null) {
                signInCreatedUser(getLocalStorageData().getQBUser());
            } else {
                Intent intentg = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(intentg);
                finish();
            }
        }
    }

    public void runNExtScreen() {
        if (getLocalStorageData().getRoleId().equals("1")) {
            Intent intentg = new Intent(SplashScreenActivity.this, PatientMainActivity.class);
            startActivity(intentg);
            finish();
        } else {
            Intent intentg = new Intent(SplashScreenActivity.this, DoctorMainActivity.class);
            startActivity(intentg);
            finish();
        }
    }

    private void signInCreatedUser(final QBUser qbUser) {
        requestExecutor.signInUser(qbUser, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                getLocalStorageData().storeQBUser(qbUser);
                runNExtScreen();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(this.getClass().getSimpleName(), "Error SignIn" + e.getMessage());
                Toast.makeText(SplashScreenActivity.this, "QB Sign in error", Toast.LENGTH_SHORT).show();
                runNExtScreen();
            }
        });
    }

    public void askForPermission(String[] permission, Integer requestCode) {
        for (String aPermission : permission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, aPermission)) {
                ActivityCompat.requestPermissions(this, permission, requestCode);
            } else {
                ActivityCompat.requestPermissions(this, permission, requestCode);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Consts.CAMERA_PERMISSION_CODE){
            if (grantResults.length > 0){
                boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean recordAudioPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if(cameraPermission && recordAudioPermission) {
                    if (getLocalStorageData().getQBUser() != null) {
                        signInCreatedUser(getLocalStorageData().getQBUser());
                    } else {
                        Intent intentg = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intentg);
                        finish();
                    }
                }
                else {

                    if (getLocalStorageData().getQBUser() != null) {
                        signInCreatedUser(getLocalStorageData().getQBUser());
                    } else {
                        Intent intentg = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intentg);
                        finish();
                    }
                }
            }
        }
    }
}