package com.srinivasu.doctor.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.PaymentFragmentAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.PaymentListModel;
import com.srinivasu.doctor.model.PaymentModel;
import com.srinivasu.doctor.model.PaymentPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentsFragment extends Fragment {
    RecyclerView recyclerView;
    List<PaymentPojo> a1;
    PaymentFragmentAdapter paymentFragmentAdapter;
    View view;
    TextView tv_nodata;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_payments, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=(RecyclerView)view.findViewById(R.id.payments_holder);
        tv_nodata=(TextView)view.findViewById(R.id.tv_nodata);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);recyclerView.setLayoutManager(linearLayoutManager);

        getDetails();


    }

    ProgressDialog pd;
    SharedPreferences pref;
    private void getDetails(){
        pref = getActivity().getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {

            Call<PaymentListModel> call = api.getPaymentsOfAPatient(pref.getString("token","-"),pref.getString("doctorId","-"));
            call.enqueue(new Callback<PaymentListModel>() {
                @Override
                public void onResponse(Call<PaymentListModel> call, Response<PaymentListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<PaymentModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    paymentFragmentAdapter=new PaymentFragmentAdapter(getActivity(),list_appointments);
                                    recyclerView.setAdapter(paymentFragmentAdapter);
                                    //Toast.makeText(getActivity(), list_appointments.get(0).getDate(), Toast.LENGTH_SHORT).show();
                                    //timeAvailabilityAdapter=new TimeAvailabilityAdapter(TimeAvailabilityActivity.this,list_appointments.getAppointments(),getIntent().getStringExtra("sel_date"));  //attach adapter class with therecyclerview
                                    //recyclerView.setAdapter(timeAvailabilityAdapter);
                                }else{

                                        tv_nodata.setVisibility(View.VISIBLE);
                                        recyclerView.setVisibility(View.GONE);

                                    //Toast.makeText(getActivity(),"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(),"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PaymentListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}