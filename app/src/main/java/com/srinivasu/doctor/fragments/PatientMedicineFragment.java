package com.srinivasu.doctor.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.MoreAppointmentsActivity;
import com.srinivasu.doctor.activities.PatAppointmentDetailsActivity;
import com.srinivasu.doctor.adapters.PatientAppointmentAdapter;
import com.srinivasu.doctor.adapters.PatientAppointmentsAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.apis.models.RequestGetAppointments;
import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.PatientHomeAppointmenListtModel;
import com.srinivasu.doctor.model.PatientHomeAppointmentModel;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.utils.DateMethods;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientMedicineFragment extends MyVDocBaseFragment {
    View view;
    RecyclerView recyclerView;

    TextView textNoData;

    PatientAppointmentsAdapter patientAppointmentsAdapter;
    List<AppointmentDetails> appointmentDetailsList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_medicine, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        textNoData=(TextView)view.findViewById(R.id.tv_nodata);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false));
        patientAppointmentsAdapter = new PatientAppointmentsAdapter(getActivity(), new ArrayList<>(),
                new com.srinivasu.doctor.adapters.interfaces.ActionCallback() {
                    @Override
                    public void onItemClick(int position) {
                        Intent intent = new Intent(getActivity(), PatAppointmentDetailsActivity.class);
                        intent.putExtra("appointment_details", Parcels.wrap(appointmentDetailsList.get(position)));
                        startActivity(intent);
                    }
                });
        recyclerView.setAdapter(patientAppointmentsAdapter);

        getAppointmentDetails();
    }

    private void getAppointmentDetails() {
        getLoadingDialog().show("Loading...");
        retrofitInterface.getPatientAppointmentHistory(getLocalStorageData().getID(),1, 10 ).enqueue(new Callback<ResponseObject<List<AppointmentDetails>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<AppointmentDetails>>> call, Response<ResponseObject<List<AppointmentDetails>>> response) {
                closeLoadingDialog();
                if(response.isSuccessful() && response.body()!= null){
                    if(response.body().getData()!=null && response.body().getData().size()>0){
                        for(int i=0; i<response.body().getData().size(); i++){
                            AppointmentDetails appointmentDetails = response.body().getData().get(i);
                            appointmentDetailsList.add(appointmentDetails);
                        }
                        patientAppointmentsAdapter.setItems(appointmentDetailsList);
                    }else{
                        recyclerView.setVisibility(View.GONE);
                        textNoData.setVisibility(View.VISIBLE);
                    }
                }else{
                    showToastMessage("Unable to get appointments for today");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<AppointmentDetails>>> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                showToastMessage(t.getMessage());
            }
        });

    }

   /* private void getAppointmentDetails() {
        getLoadingDialog().show("Loading...");
        try {
            paramObject.put("date", DateMethods.getCurrentDate());
            retrofitInterface.getPatientAppointments(getLocalStorageData().getID(), new RequestGetAppointments(DateMethods.getCurrentDate())).enqueue(new Callback<ResponseObject<List<AppointmentDetails>>>() {
                @Override
                public void onResponse(Call<ResponseObject<List<AppointmentDetails>>> call, Response<ResponseObject<List<AppointmentDetails>>> response) {
                    closeLoadingDialog();
                    if(response.isSuccessful() && response.body()!= null){
                        if(response.body().getData()!=null && response.body().getData().size()>0){
                            appointmentDetailsList.addAll(response.body().getData());
                            patientAppointmentsAdapter.setItems(appointmentDetailsList);
                        }else{
                            recyclerView.setVisibility(View.GONE);
                            textNoData.setVisibility(View.VISIBLE);
                        }
                    }else{
                        showToastMessage("Unable to get appointments for today");
                    }
                }

                @Override
                public void onFailure(Call<ResponseObject<List<AppointmentDetails>>> call, Throwable t) {
                    closeLoadingDialog();
                    call.cancel();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/
}