package com.srinivasu.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.DoctorAvailabilityActivity;
import com.srinivasu.doctor.activities.TimeAvailabilityActivity;
import com.srinivasu.doctor.db.LocalStorageData;

import java.util.Calendar;

public class AppointmentsFragment extends MyVDocBaseFragment {
    View view;
    CalendarView calender;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_appointments, container, false);

        LocalStorageData localStorageData = new LocalStorageData(getActivity());

        Calendar cal = Calendar.getInstance();
        calender=(CalendarView)view.findViewById(R.id.calender);
        calender.setMinDate(cal.getTimeInMillis());
        calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                if(getLocalStorageData().getRoleId().contentEquals("1")){

                    Intent intentg= new Intent(getActivity(), DoctorAvailabilityActivity.class);
                    intentg.putExtra("sel_date",year+"-"+(month+1)+"-"+dayOfMonth);
                    startActivity(intentg);

                }else{
                    Intent intentg= new Intent(getActivity(), TimeAvailabilityActivity.class);
                    intentg.putExtra("sel_date",year+"-"+(month+1)+"-"+dayOfMonth);
                    startActivity(intentg);
                }

            }
        });
        return view;
    }

}