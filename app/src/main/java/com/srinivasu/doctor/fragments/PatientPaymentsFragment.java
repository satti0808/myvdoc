package com.srinivasu.doctor.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.PatientPaymentFragmentAdapter;
import com.srinivasu.doctor.adapters.PaymentFragmentAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.PatientPaymentListModel;
import com.srinivasu.doctor.model.PatientPaymentModel;
import com.srinivasu.doctor.model.PaymentPojo;
import com.srinivasu.doctor.model.ResponseObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientPaymentsFragment extends MyVDocBaseFragment {
    RecyclerView recyclerView;
    List<PaymentPojo> a1;
    PatientPaymentFragmentAdapter paymentFragmentAdapter;
    View view;
    TextView tvNoData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_payments, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=(RecyclerView)view.findViewById(R.id.payments_holder);
        tvNoData=(TextView)view.findViewById(R.id.tv_nodata);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false));

        getDetails();
    }

    private void getDetails(){
        getLoadingDialog().show("Loading....");
        retrofitInterface.getPatientPayments(getLocalStorageData().getID()).enqueue(new Callback<ResponseObject<List<PatientPaymentModel>>>() {
            @Override
            public void onResponse(Call<ResponseObject<List<PatientPaymentModel>>> call, Response<ResponseObject<List<PatientPaymentModel>>> response) {
                closeLoadingDialog();
                if(response.isSuccessful() && response.body()!= null){
                    if(response.body().getData()!= null && response.body().getData().size()>0){
                        paymentFragmentAdapter = new PatientPaymentFragmentAdapter(getActivity(), response.body().getData());
                        recyclerView.setAdapter(paymentFragmentAdapter);
                    }else{
                        recyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                    }

                }else{
                    showToastMessage("Unable to get past payments");
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<List<PatientPaymentModel>>> call, Throwable t) {
                call.cancel();
                t.printStackTrace();
                closeLoadingDialog();
            }
        });
    }
}