package com.srinivasu.doctor.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.MessageAdapter;
import com.srinivasu.doctor.adapters.PaymentFragmentAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.MessageListModel;
import com.srinivasu.doctor.model.MessageModel;
import com.srinivasu.doctor.model.PaymentPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagesFragment extends Fragment {
    RecyclerView recyclerView;
    List<PaymentPojo> a1;
    PaymentFragmentAdapter paymentFragmentAdapter;
    View view;
    TextView tv_nodata;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_messages1, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        tv_nodata=(TextView)view.findViewById(R.id.tv_nodata);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);recyclerView.setLayoutManager(linearLayoutManager);

        getDetails();
        return view;
    }
    ProgressDialog pd;
    SharedPreferences pref;
    private void getDetails(){

        pref = getActivity().getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            paramObject.put("date", mYear+"-"+(mMonth+1)+"-"+mDay);
            //paramObject.put("date", "2020-07-03");
            //paramObject.put("password", "1234");
            Call<MessageListModel> call = api.getMessages(pref.getString("token","-"),pref.getString("doctorId","-"));
            call.enqueue(new Callback<MessageListModel>() {
                @Override
                public void onResponse(Call<MessageListModel> call, Response<MessageListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<MessageModel> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{

                            if(list_appointments.size()>0) {
                                MessageAdapter homeFragmentAdapter=new MessageAdapter(getActivity(),list_appointments);  //attach adapter class with therecyclerview
                                recyclerView.setAdapter(homeFragmentAdapter);
                            }
                            else {
                                tv_nodata.setVisibility(View.VISIBLE);
                                tv_nodata.setText("No Urgent Messages");
                                recyclerView.setVisibility(View.GONE);
                            }
                            }

                    }else{
                        Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MessageListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
}