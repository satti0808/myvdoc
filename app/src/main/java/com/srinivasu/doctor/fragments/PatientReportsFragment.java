package com.srinivasu.doctor.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.BPListModel;
import com.srinivasu.doctor.model.BPModel;
import com.srinivasu.doctor.model.DiabetesListModel;
import com.srinivasu.doctor.model.DiabetesModel;
import com.srinivasu.doctor.model.ResultData;
import com.srinivasu.doctor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PatientReportsFragment extends MyVDocBaseFragment {
    View view;
    TableLayout tb_diabetes,tb_bp;
    TextView tv_add_bp,tv_add_diabetes,tv_diabetes_no_details,tv_bp_no_details;

    int mYear,mMonth,mDay,mHour,mMinute;
    String sel_date,sel_time;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_patient_reports, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pref = getActivity().getSharedPreferences("mydoc", 0);
        tb_diabetes=(TableLayout)view.findViewById(R.id.tb_diabetes);
        tb_bp=(TableLayout)view.findViewById(R.id.tb_bp);
        //Toast.makeText(getActivity(),"Hello",Toast.LENGTH_SHORT).show();
        tv_add_bp=(TextView)view.findViewById(R.id.tv_add_bp);
        tv_diabetes_no_details=(TextView)view.findViewById(R.id.tv_diabetes_no_details);
        tv_bp_no_details=(TextView)view.findViewById(R.id.tv_bp_no_details);

        tv_add_diabetes=(TextView)view.findViewById(R.id.tv_add_diabetes);
        tv_add_bp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBP();
            }
        });
        tv_add_diabetes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDiabetes();
            }
        });

        getBPDetails();

    }

    private void getStartDate(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        sel_date = year + "-"+(monthOfYear+1)+"-"+dayOfMonth;
                        getStartTime();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    private void getStartTime(){
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        //tvStartTime.setText(hourOfDay + ":" + minute);
                        //Toast.makeText(context,postpone_duration+" : "+postpone_date,Toast.LENGTH_SHORT).show();
                        sel_time =hourOfDay + ":" + minute+":00";
                        tv_select_date_val.setText(sel_date + "  "+sel_time);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }
    TextView tv_select_date_val;
    private void addBP() {

        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialogbox_bp_details, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.ProgressDialogTheme).create();
        alertDialog.setTitle("");
        alertDialog.setCancelable(false);
        final EditText etSystolic = (EditText) view.findViewById(R.id.etSystolic);
        final EditText etDastolic = (EditText) view.findViewById(R.id.etDastolic);
        tv_select_date_val = (TextView) view.findViewById(R.id.tv_select_date_val);
        TextView tv_select_date = (TextView) view.findViewById(R.id.tv_select_date);

        tv_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStartDate();
            }
        });

        TextView btn_save = (TextView) view.findViewById(R.id.btn_save);
        TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Systolic=etSystolic.getText().toString();
                Dastolic=etDastolic.getText().toString();
                submitBPDetails();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }
    private void editBP(final String id) {
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialogbox_bp_details, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.ProgressDialogTheme).create();
        alertDialog.setTitle("");
        alertDialog.setCancelable(false);
        final EditText etSystolic = (EditText) view.findViewById(R.id.etSystolic);
        final EditText etDastolic = (EditText) view.findViewById(R.id.etDastolic);
        tv_select_date_val = (TextView) view.findViewById(R.id.tv_select_date_val);
        TextView tv_select_date = (TextView) view.findViewById(R.id.tv_select_date);

        tv_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStartDate();
            }
        });

        TextView btn_save = (TextView) view.findViewById(R.id.btn_save);
        TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Systolic=etSystolic.getText().toString();
                Dastolic=etDastolic.getText().toString();
                submitEditBPDetails(id);
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }
    String PerLunch,PostLunch,Random;
    private void addDiabetes() {
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialogbox_diabetes_details, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.ProgressDialogTheme).create();
        alertDialog.setTitle("");
        alertDialog.setCancelable(false);
        final EditText etPerLunch = (EditText) view.findViewById(R.id.etPerLunch);
        final EditText etPostLunch = (EditText) view.findViewById(R.id.etPostLunch);
        final EditText etRandom = (EditText) view.findViewById(R.id.etRandom);
        tv_select_date_val = (TextView) view.findViewById(R.id.tv_select_date_val);
        TextView tv_select_date = (TextView) view.findViewById(R.id.tv_select_date);

        tv_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStartDate();
            }
        });

        TextView btn_save = (TextView) view.findViewById(R.id.btn_save);
        TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PerLunch=etPerLunch.getText().toString();
                PostLunch=etPostLunch.getText().toString();
                Random=etRandom.getText().toString();
                submitDiabetesDetails();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }
    private void editDiabetes(final String id,String pre,String post) {
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialogbox_diabetes_details, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.ProgressDialogTheme).create();
        alertDialog.setTitle("");
        alertDialog.setCancelable(false);
        final EditText etPerLunch = (EditText) view.findViewById(R.id.etPerLunch);
        final EditText etPostLunch = (EditText) view.findViewById(R.id.etPostLunch);
        final EditText etRandom = (EditText) view.findViewById(R.id.etRandom);
        tv_select_date_val = (TextView) view.findViewById(R.id.tv_select_date_val);
        TextView tv_select_date = (TextView) view.findViewById(R.id.tv_select_date);
        etPerLunch.setText(pre);
        etPostLunch.setText(post);

        tv_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStartDate();
            }
        });

        TextView btn_save = (TextView) view.findViewById(R.id.btn_save);
        TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PerLunch=etPerLunch.getText().toString();
                PostLunch=etPostLunch.getText().toString();
                Random=etRandom.getText().toString();
                submitEditDiabetesDetails(id);
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }
    ProgressDialog pd;
    SharedPreferences pref;
    List<DiabetesModel> list_appointments;
    private void getDiabetesDetails(){
        getLoadingDialog().show("Loading ... ");
        try {
            retrofitInterface.getDiabetesOfAPatient(getLocalStorageData().getID()).enqueue(new Callback<DiabetesListModel>() {
                @Override
                public void onResponse(Call<DiabetesListModel> call, Response<DiabetesListModel> response) {
                    closeLoadingDialog();
                    if (response.isSuccessful()) {
                        list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    //tb_diabetes.getChildCount()
                                    tv_diabetes_no_details.setVisibility(View.GONE);
                                    tb_diabetes.removeAllViews();
                                    LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService
                                            (Context.LAYOUT_INFLATER_SERVICE);
                                    TableRow row0 = (TableRow) inflater.inflate(R.layout.row_table_row1,null);
                                    TextView tv_date0=(TextView)row0.findViewById(R.id.tv_date);
                                    tv_date0.setText("Date");

                                    TextView tv01=(TextView)row0.findViewById(R.id.tv_pre);
                                    tv01.setText("Pre");

                                    TextView tv02=(TextView)row0.findViewById(R.id.tv_post);
                                    tv02.setText("Post");
                                    ImageView iv_edit0=(ImageView)row0.findViewById(R.id.iv_edit);
                                    iv_edit0.setVisibility(View.GONE);

                                    ImageView iv_delete0=(ImageView)row0.findViewById(R.id.iv_delete);
                                    iv_delete0.setVisibility(View.GONE);
                                    tb_diabetes.addView(row0,0);
                                    for(int i = 0; i < list_appointments.size(); i ++){
                                        final int pos= i;
                                        TableRow row = (TableRow) inflater.inflate(R.layout.row_table_row,null);

                                        TextView tv_date=(TextView)row.findViewById(R.id.tv_date);
                                        tv_date.setText(Utils.displayDateFormat(list_appointments.get(i).getDate()));

                                        TextView tv1=(TextView)row.findViewById(R.id.tv_pre);
                                        tv1.setText(""+list_appointments.get(i).getPerLunch());

                                        TextView tv2=(TextView)row.findViewById(R.id.tv_post);
                                        tv2.setText(""+list_appointments.get(i).getPostLunch());
                                        tb_diabetes.addView(row,(i+1));


                                        ImageView iv_delete=(ImageView)row.findViewById(R.id.iv_delete);
                                        iv_delete.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                deleteDiabetes(list_appointments.get(pos).getId());
                                                //editDiabetes(list_appointments.get(pos).getId(),list_appointments.get(pos).getPerLunch(),list_appointments.get(pos).getPostLunch());
                                                //Toast.makeText(getActivity(),""+list_appointments.get(pos).getId(),Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        ImageView iv_edit=(ImageView)row.findViewById(R.id.iv_edit);
                                        iv_edit.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                editDiabetes(list_appointments.get(pos).getId(),list_appointments.get(pos).getPerLunch(),list_appointments.get(pos).getPostLunch());
                                                //Toast.makeText(getActivity(),""+list_appointments.get(pos).getId(),Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                    }

                                }else{
                                    Toast.makeText(getActivity(),"No diabetes details for this date.",Toast.LENGTH_SHORT).show();
                                    tv_diabetes_no_details.setVisibility(View.VISIBLE);
                                }
                            }else{
                                Toast.makeText(getActivity(),"No diabetes details for this date.",Toast.LENGTH_SHORT).show();
                                tv_diabetes_no_details.setVisibility(View.VISIBLE);
                            }

                        }

                    }else{
                        Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DiabetesListModel> call, Throwable t) {
                    closeLoadingDialog();
                    Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    List<BPModel> list_bp;
    private void getBPDetails(){

        getLoadingDialog().show("Loading ... ");
        try {
            retrofitInterface.getBPOfAPatient(getLocalStorageData().getID()).enqueue(new Callback<BPListModel>() {
                @Override
                public void onResponse(Call<BPListModel> call, Response<BPListModel> response) {
                    closeLoadingDialog();
                    getDiabetesDetails();
                    if (response.isSuccessful()) {
                        list_bp = response.body().getData1();
                        if(list_bp==null){
                            Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_bp!=null){
                                if(list_bp.size()>0) {
                                    tv_bp_no_details.setVisibility(View.GONE);
                                    LayoutInflater inflater = (LayoutInflater)getActivity().getApplicationContext().getSystemService
                                            (Context.LAYOUT_INFLATER_SERVICE);
                                    TableRow row0 = (TableRow) inflater.inflate(R.layout.row_table_row1,null);
                                    TextView tv_date0=(TextView)row0.findViewById(R.id.tv_date);
                                    tv_date0.setText("Date");

                                    TextView tv01=(TextView)row0.findViewById(R.id.tv_pre);
                                    tv01.setText("Systolic");

                                    TextView tv02=(TextView)row0.findViewById(R.id.tv_post);
                                    tv02.setText("Dastolic");

                                    ImageView iv_edit0=(ImageView)row0.findViewById(R.id.iv_edit);
                                    iv_edit0.setVisibility(View.GONE);

                                    ImageView iv_delete0=(ImageView)row0.findViewById(R.id.iv_delete);
                                    iv_delete0.setVisibility(View.GONE);


                                    tb_bp.addView(row0,0);


                                    for(int i = 0; i < list_bp.size(); i ++){
                                        final int pos=i;
                                        TableRow row = (TableRow) inflater.inflate(R.layout.row_table_row,null);

                                        TextView tv_date=(TextView)row.findViewById(R.id.tv_date);
                                        tv_date.setText(Utils.displayDateFormat(list_bp.get(i).getDate()));

                                        TextView tv1=(TextView)row.findViewById(R.id.tv_pre);
                                        tv1.setText(""+list_bp.get(i).getSystolic());

                                        TextView tv2=(TextView)row.findViewById(R.id.tv_post);
                                        tv2.setText(""+list_bp.get(i).getDiastolic());
                                        ImageView iv_delete=(ImageView)row.findViewById(R.id.iv_delete);
                                        iv_delete.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                deleteBP(list_bp.get(pos).getId());
                                                //editDiabetes(list_appointments.get(pos).getId(),list_appointments.get(pos).getPerLunch(),list_appointments.get(pos).getPostLunch());
                                                //Toast.makeText(getActivity(),"ID -> "+list_bp.get(pos).getId(),Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ImageView iv_edit=(ImageView)row.findViewById(R.id.iv_edit);
                                        iv_edit.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                editBP(list_bp.get(pos).getId());
                                                //Toast.makeText(getActivity(),""+list_bp.get(pos).getId(),Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        tb_bp.addView(row,(i+1));
                                    }

                                }else{
                                    tv_bp_no_details.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(),"No BP details for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                tv_bp_no_details.setVisibility(View.VISIBLE);
                                Toast.makeText(getActivity(),"No BP details for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BPListModel> call, Throwable t) {
                    closeLoadingDialog();
                    Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
String Dastolic,Systolic;
    private void submitBPDetails(){
        getLoadingDialog().show("Loading...");
        Calendar c = Calendar.getInstance();
        int mY = c.get(Calendar.YEAR);
        int mM = c.get(Calendar.MONTH);
        int mD = c.get(Calendar.DAY_OF_MONTH);

        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("PatientID", getLocalStorageData().getID());
            paramObject.put("Date",   mY+"-"+(mM+1)+"-"+mD);
            paramObject.put("Time",   currentTime);
            paramObject.put("Systolic",   Systolic);
            paramObject.put("Diastolic",   Dastolic);
            Call<ResultData> call = api.addBPDetails(pref.getString("token","-"),paramObject.toString());
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    closeLoadingDialog();
                    tb_diabetes.removeAllViews();
                    tb_bp.removeAllViews();
                    getDiabetesDetails();
                    getBPDetails();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                   // pd.dismiss();
                    closeLoadingDialog();
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteBP(String id){
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("PatientID", "");
            Call<ResultData> call = api.deleteBP(pref.getString("token","-"),id);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    tb_diabetes.removeAllViews();
                    tb_bp.removeAllViews();
                    getDiabetesDetails();
                    getBPDetails();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void deleteDiabetes(String id){
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("PatientID", "");
            Call<ResultData> call = api.deleteDiabetes(pref.getString("token","-"),id);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    tb_diabetes.removeAllViews();
                    tb_bp.removeAllViews();
                    getDiabetesDetails();
                    getBPDetails();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void submitDiabetesDetails(){
        Calendar c = Calendar.getInstance();
        int mY = c.get(Calendar.YEAR);
        int mM = c.get(Calendar.MONTH);
        int mD = c.get(Calendar.DAY_OF_MONTH);
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being submitted.");
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("PatientID", pref.getString("pid","-"));
            paramObject.put("Date",   mY+"-"+(mM+1)+"-"+mD);
            paramObject.put("Time",   currentTime);
            paramObject.put("PerLunch",   PerLunch);
            paramObject.put("PostLunch",   PostLunch);
            paramObject.put("Random",   Random);
            Call<ResultData> call = api.addDiabetesDetails(pref.getString("token","-"),paramObject.toString());
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    tb_diabetes.removeAllViews();
                    tb_bp.removeAllViews();
                    getDiabetesDetails();
                    getBPDetails();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void submitEditDiabetesDetails(String id){
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being submitted.");

        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("PatientID", pref.getString("pid","-"));
            paramObject.put("PerLunch",   PerLunch);
            paramObject.put("PostLunch",   PostLunch);
            paramObject.put("Random",   Random);
            Call<ResultData> call = api.editDiabetesDetails(pref.getString("token","-"),paramObject.toString(),id);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    tb_diabetes.removeAllViews();
                    tb_bp.removeAllViews();
                    getDiabetesDetails();
                    getBPDetails();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void submitEditBPDetails(String id){
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("PatientID", pref.getString("pid","-"));
            paramObject.put("Systolic",   Systolic);
            paramObject.put("Diastolic",   Dastolic);
            Call<ResultData> call = api.editBPDetails(pref.getString("token","-"),paramObject.toString(),id);
            call.enqueue(new Callback<ResultData>() {
                @Override
                public void onResponse(Call<ResultData> call, Response<ResultData> response) {
                    tb_diabetes.removeAllViews();
                    tb_bp.removeAllViews();
                    getDiabetesDetails();
                    getBPDetails();
                }

                @Override
                public void onFailure(Call<ResultData> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "Invalid Email/Password", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
}