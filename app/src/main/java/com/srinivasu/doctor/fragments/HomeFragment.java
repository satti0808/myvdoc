package com.srinivasu.doctor.fragments;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.quickblox.chat.QBChatService;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.CallActivity;
import com.srinivasu.doctor.adapters.HomeFragmentAdapter;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.db.LocalStorageData;
import com.srinivasu.doctor.model.HomeAppointmentListModel;
import com.srinivasu.doctor.model.HomeAppointmentModel;
import com.srinivasu.doctor.services.CallService;
import com.srinivasu.doctor.services.LoginService;
import com.srinivasu.doctor.utils.Consts;
import com.srinivasu.doctor.utils.PushNotificationSender;
import com.srinivasu.doctor.utils.SharedPrefsHelper;
import com.srinivasu.doctor.utils.WebRtcSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.srinivasu.doctor.utils.Consts.COMMAND_LOGIN;
import static com.srinivasu.doctor.utils.Consts.EXTRA_COMMAND_TO_SERVICE;
import static com.srinivasu.doctor.utils.Consts.EXTRA_QB_USER;

public class HomeFragment extends Fragment {
    private static final String TAG = HomeFragment.class.getSimpleName();

    RecyclerView recyclerView;
    ProgressDialog progressDialog;

    List<HomeAppointmentModel> list_appointments;

    TextView tvNoData;
    HomeFragmentAdapter homeFragmentAdapter;
    View view;

    protected SharedPrefsHelper sharedPrefsHelper;
    private LocalStorageData localStorageData;
    private QBUser currentUser;
    private QBRTCClient rtcClient;
    private QBChatService chatService;

    boolean isVideoCall;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_home, container, false);

        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        localStorageData = new LocalStorageData(getActivity());
        currentUser = localStorageData.getQBUser();

        isVideoCall = false;

        tvNoData = (TextView) view.findViewById(R.id.tv_nodata);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        getDetails();
        checkIsLoggedInChat();
        return view;
    }

    ProgressDialog pd;
    SharedPreferences pref;
    int mYear, mMonth, mDay;

    private void getDetails() {
        pref = getActivity().getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being submitted.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {
            JSONObject paramObject = new JSONObject();
            final Calendar c = Calendar.getInstance();
            Calendar now = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String currentTime = sdf.format(new Date());
            paramObject.put("date", currentTime);
            Call<HomeAppointmentListModel> call = api.getHomeApointmentsOfADoctor(localStorageData.getToken(), localStorageData.getID(), paramObject.toString());
            call.enqueue(new Callback<HomeAppointmentListModel>() {
                @Override
                public void onResponse(Call<HomeAppointmentListModel> call, Response<HomeAppointmentListModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        list_appointments = new ArrayList<>();
                        list_appointments = response.body().getData1();
                        if (list_appointments == null) {
                            tvNoData.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
//                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                        } else {

                            if (list_appointments.size() > 0) {
                                homeFragmentAdapter = new HomeFragmentAdapter(getActivity(), list_appointments, new HomeFragmentAdapter.HomeCallBackInterface() {
                                    @Override
                                    public void onStartCall(int position) {

                                        if (list_appointments.get(position).getAppointment_type().contentEquals("Video Call")) {
                                            isVideoCall = true;
                                        }
//                                        startCall(position);
                                        if (checkIsLoggedInChat()) {
                                            Toast.makeText(getActivity(),"Checking", Toast.LENGTH_SHORT).show();
                                            startCall(position);
                                        }
                                    }
                                });
                                recyclerView.setAdapter(homeFragmentAdapter);
                            } else {
                                tvNoData.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);

                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<HomeAppointmentListModel> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isIncomingCall = SharedPrefsHelper.getInstance().get(Consts.EXTRA_IS_INCOMING_CALL, false);
        if (isCallServiceRunning(CallService.class)) {
            Log.d(this.getClass().getSimpleName(), "CallService is running now");
            CallActivity.start(getActivity(), isIncomingCall);
        }
        clearAppNotifications();
    }

    private boolean isCallServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void clearAppNotifications() {
        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }

    private boolean checkIsLoggedInChat() {
        if (!QBChatService.getInstance().isLoggedIn()) {
            Toast.makeText(getActivity(), "Logging into chat", Toast.LENGTH_SHORT).show();
            startLoginService();
            return false;
        }
        return true;
    }

    private void startLoginService() {
        if (localStorageData.getQBUser() != null) {
            QBUser qbUser = localStorageData.getQBUser();
            Intent intent = new Intent(getActivity(), LoginService.class);
            intent.putExtra(EXTRA_COMMAND_TO_SERVICE, COMMAND_LOGIN);
            intent.putExtra(EXTRA_QB_USER, qbUser);
            Objects.requireNonNull(getActivity()).startService(intent);
//            getActivity().LoginService.start(getActivity(), qbUser);
        }
    }


    private void startCall(int position) {

        Log.d(TAG, "Starting Call");

        ArrayList<Integer> patientsList = new ArrayList<>();
        patientsList.add(Integer.parseInt(list_appointments.get(position).getVedioid()));

        QBRTCTypes.QBConferenceType conferenceType = isVideoCall
                ? QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
                : QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO;
        Log.d(TAG, "conferenceType = " + conferenceType);

        QBRTCClient qbrtcClient = QBRTCClient.getInstance(getActivity());
        QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(patientsList, conferenceType);
        WebRtcSessionManager.getInstance(getActivity()).setCurrentSession(newQbRtcSession);

        // Make Users FullName Strings and ID's list for iOS VOIP push
        String newSessionID = newQbRtcSession.getSessionID();

        Log.d(TAG, "New Session with ID: " + newSessionID + "\n Users in Call: " + "\n" + list_appointments.get(position).getPatient_ID()
                + "\n" + list_appointments.get(position).getPatient_name());
        PushNotificationSender.sendPushMessage(patientsList, currentUser.getFullName(), newSessionID, list_appointments.get(position).getPatient_ID(),
                list_appointments.get(position).getPatient_name(), isVideoCall);
        CallActivity.start(getActivity(), false);
    }
}