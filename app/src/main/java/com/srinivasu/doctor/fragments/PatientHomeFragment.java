package com.srinivasu.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.activities.DocAppointmentDetailsActivity;
import com.srinivasu.doctor.activities.MoreAppointmentsActivity;
import com.srinivasu.doctor.activities.PatAppointmentDetailsActivity;
import com.srinivasu.doctor.adapters.PatientAppointmentsAdapter;
import com.srinivasu.doctor.apis.models.RequestGetAppointments;
import com.srinivasu.doctor.model.AppointmentDetails;
import com.srinivasu.doctor.model.ResponseObject;
import com.srinivasu.doctor.utils.DateMethods;

import org.json.JSONException;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientHomeFragment extends MyVDocBaseFragment {
    RecyclerView appointmentsHolder;
    View view;
    TextView textNoData,textMore;
    ImageView imgAddAppointment;

    PatientAppointmentsAdapter patientAppointmentsAdapter;
    List<AppointmentDetails> appointmentDetailsList = new ArrayList<>();

    ActionCallback actionCallback;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_patient_home, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        appointmentsHolder=(RecyclerView)view.findViewById(R.id.rv_appointment);
        textNoData=(TextView)view.findViewById(R.id.tv_no_data);
        textMore=(TextView)view.findViewById(R.id.tvMore);
        textMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), MoreAppointmentsActivity.class);
                startActivity(intent);
            }
        });

        appointmentsHolder.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false));
        patientAppointmentsAdapter = new PatientAppointmentsAdapter(getActivity(), new ArrayList<>(),
                new com.srinivasu.doctor.adapters.interfaces.ActionCallback() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), PatAppointmentDetailsActivity.class);
                intent.putExtra("appointment_details", Parcels.wrap(appointmentDetailsList.get(position)));
                startActivity(intent);
            }
        });
        appointmentsHolder.setAdapter(patientAppointmentsAdapter);

        imgAddAppointment=(ImageView) view.findViewById(R.id.iv_add_appointment);
        imgAddAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               actionCallback.onAddAppointmentClicked();
            }
        });
        getAppointmentDetails();
    }

    private void getAppointmentDetails() {
        getLoadingDialog().show("Loading...");
        try {
            paramObject.put("date", DateMethods.getCurrentDate());
            retrofitInterface.getPatientAppointments(getLocalStorageData().getID(), new RequestGetAppointments(DateMethods.getCurrentDate())).enqueue(new Callback<ResponseObject<List<AppointmentDetails>>>() {
                @Override
                public void onResponse(Call<ResponseObject<List<AppointmentDetails>>> call, Response<ResponseObject<List<AppointmentDetails>>> response) {
                    closeLoadingDialog();
                    if(response.isSuccessful() && response.body()!= null){
                        if(response.body().getData()!=null && response.body().getData().size()>0){
                            appointmentDetailsList.addAll(response.body().getData());
                            patientAppointmentsAdapter.setItems(appointmentDetailsList);
                        }else{
                            appointmentsHolder.setVisibility(View.GONE);
                            textNoData.setVisibility(View.VISIBLE);
                        }
                    }else{
                        showToastMessage("Unable to get appointments for today");
                    }
                }

                @Override
                public void onFailure(Call<ResponseObject<List<AppointmentDetails>>> call, Throwable t) {
                    closeLoadingDialog();
                    call.cancel();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setActionCallBackListener(ActionCallback callback) {
        this.actionCallback = callback;
    }

    public interface ActionCallback {
        void onAddAppointmentClicked();
    }
}
