package com.srinivasu.doctor.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srinivasu.doctor.R;
import com.srinivasu.doctor.adapters.PatientsFragmentAdapter1;
import com.srinivasu.doctor.apis.ApiInterface;
import com.srinivasu.doctor.apis.RetroClient;
import com.srinivasu.doctor.model.PatientListModel1;
import com.srinivasu.doctor.model.PatientModel1;
import com.srinivasu.doctor.model.PatientPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientsFragment extends Fragment {
    RecyclerView recyclerView;
    List<PatientPojo> a1;
    PatientsFragmentAdapter1 patientsFragmentAdapter;
    View view;
    TextView tv_nodata;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_patients, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        tv_nodata=(TextView)view.findViewById(R.id.tv_nodata);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);recyclerView.setLayoutManager(linearLayoutManager);

        getDetails();
        return view;
    }
    ProgressDialog pd;
    SharedPreferences pref;
    private void getDetails(){
        pref = getActivity().getSharedPreferences("mydoc", 0);
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Please wait,Data is being retrived.");
        pd.show();
        ApiInterface api = RetroClient.getApiService();
        try {

            Call<PatientListModel1> call = api.getPaymentsOfAPatient2(pref.getString("token","-"),pref.getString("doctorId","-"));
            call.enqueue(new Callback<PatientListModel1>() {
                @Override
                public void onResponse(Call<PatientListModel1> call, Response<PatientListModel1> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        List<PatientModel1> list_appointments = response.body().getData1();
                        if(list_appointments==null){
                            Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                        }else{
                            if(list_appointments!=null){
                                if(list_appointments.size()>0) {
                                    patientsFragmentAdapter=new PatientsFragmentAdapter1(getActivity(),list_appointments);
                                    recyclerView.setAdapter(patientsFragmentAdapter);

                                }else{
                                    tv_nodata.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                    //Toast.makeText(getActivity(),"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(),"No Appointments for this date.",Toast.LENGTH_SHORT).show();
                            }

                        }

                    }else{
                        Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PatientListModel1> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}