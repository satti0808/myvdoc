package com.srinivasu.doctor.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.srinivasu.doctor.R;
import com.srinivasu.doctor.apis.BackendAPIs;
import com.srinivasu.doctor.apis.RetrofitInterface;
import com.srinivasu.doctor.apis.interceptors.OfflineException;
import com.srinivasu.doctor.db.LocalStorageData;
import com.srinivasu.doctor.dialogs.CustomDialog;
import com.srinivasu.doctor.dialogs.LoadingDialog;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class MyVDocBaseFragment extends Fragment {
    private View layoutView;

    private ViewSwitcher viewSwitcher;
    private ProgressBar loading_indicator;
    private FloatingActionButton reload_indicator;
    private TextView loading_message;
    private TextView no_data_message;

    private LoadingDialog loadingDialog;
    private CustomDialog customDialog;

    public RetrofitInterface retrofitInterface;
    private LocalStorageData localStorageData;
    private BackendAPIs backendApi;


    public MyHandler myHandler;
    JSONObject paramObject;

    public MyVDocBaseFragment(){}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;
        myHandler = new MyHandler(getActivity());
        retrofitInterface = getBackendApi().baseAdapter().create(RetrofitInterface.class);

        paramObject = new JSONObject();

    }

    //===== TOAST METHODS
    public void showToastMessage(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){
            localStorageData = new LocalStorageData(getActivity().getApplicationContext());
        }
        return localStorageData;
    }

    public BackendAPIs getBackendApi() {
        if ( backendApi == null ){
            backendApi = new BackendAPIs(getActivity());
        }
        return backendApi;
    }

    //======= Dialogs

    public LoadingDialog getLoadingDialog() {
        closeLoadingDialog();
        if (loadingDialog == null){
            loadingDialog = new LoadingDialog(getActivity());
        }
        return loadingDialog;
    }

    public void closeLoadingDialog(){
        if ( loadingDialog != null ){
            loadingDialog.closeDialog();
        }
    }

    public CustomDialog getCustomDialog() {
        closeCustomDialog();
        if (customDialog == null){
            customDialog = new CustomDialog(getActivity());
        }
        return customDialog;
    }

    public void closeCustomDialog(){
        if ( customDialog != null ){
            customDialog.closeDialog();
        }
    }

    public void hideSoftKeyboard(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showSoftKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    /**
     * Instances of static inner classes do not hold an implicit
     * reference to their outer class.
     */
    public static class MyHandler extends Handler {
        private final WeakReference<Activity> mActivity;
        MyHandler(Activity activity) {
            mActivity = new WeakReference<Activity>(activity);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeLoadingDialog();
        closeCustomDialog();
    }

}
